package lumenghz.com.pullrefresh.refresh_view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import lumenghz.com.pullrefresh.PullToRefreshView;
import lumenghz.com.pullrefresh.R;
import lumenghz.com.pullrefresh.util.Utils;

/**
 * Created by denisshovhenia on 16.11.16.
 */
public class CloudRefreshView extends BaseRefreshView {

    private static final float HEIGHT_RATIO = 1.0f;

    private PullToRefreshView mParent;

    private Matrix mMatrix;

    private Context mContext;

    private Animation mCloudAnimation;

    private Bitmap mCloud;
    private Bitmap mSky;

    private Paint mBackgroundPaint;

    private boolean isRefreshing = false;
    private float mCloudAlpha = 1;

    private float mPercent;

    private int mCloudWidth;

    /**
     * height of landscape
     */
    private int mSenceHeight;
    /**
     * width of landscape
     */
    private int mScreenWidth;
    /**
     * distance between bottom of landscape and top of landscape
     */
    private int mTop;
    /**
     * max distance between bottom of landscape and top of landscape
     */
    private int totalDistance;

    public CloudRefreshView(Context context, final PullToRefreshView layout) {
        super(context, layout);

        mParent = layout;
        mMatrix = new Matrix();
        mContext = getContext();
        setupAnimations();
        setupPaint();
        layout.post(new Runnable() {
            @Override
            public void run() {
                initialDimens(layout.getWidth());
            }
        });
    }

    @Override
    protected void initialDimens(int viewWidth) {
        if (viewWidth <= 0 || viewWidth == mScreenWidth) return;
        mScreenWidth = viewWidth;

        createBitmaps();

        mSenceHeight = (int) (HEIGHT_RATIO * mScreenWidth);

        mTop = -mParent.getTotalDragDistance();
        totalDistance = -mTop;

        mCloudWidth = mCloud.getWidth();
    }

    private void createBitmaps() {
        mCloud = CreateBitmapFactory.getBitmapFromImage(R.drawable.cloud_primary, mContext);
        mCloud = Bitmap.createScaledBitmap(mCloud, Utils.convertDpToPixel(mContext, 55), Utils.convertDpToPixel(mContext, 40), true);
        mSky = CreateBitmapFactory.getBitmapFromImage(R.drawable.clouds_back, mContext);
        final int maxSize = mScreenWidth - (mScreenWidth / 5);
        int outWidth;
        int outHeight;
        int inWidth = mSky.getWidth();
        int inHeight = mSky.getHeight();
        if (inWidth > inHeight) {
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }
        mSky = Bitmap.createScaledBitmap(mSky, outWidth, outHeight, false);
    }

    @Override
    public void draw(Canvas canvas) {
        if (mScreenWidth <= 0) return;

        final int saveCount = canvas.save();

        canvas.translate(0, mTop);
        canvas.clipRect(0, -mTop, mScreenWidth, mParent.getTotalDragDistance());
        canvas.drawRect(0, -mTop, mScreenWidth, mParent.getTotalDragDistance(), mBackgroundPaint);

        drawSky(canvas);
        drawCloud(canvas);

        canvas.restoreToCount(saveCount);
    }

    private void drawCloud(Canvas canvas) {
        final Matrix matrix = mMatrix;
        matrix.reset();

        float dragPercent = Math.min(1f, Math.abs(mPercent));

        final float offsetX = isRefreshing ?
                mScreenWidth / 2 * (2 - dragPercent) - mCloudWidth / 2 + (mCloudWidth * (1.0f - dragPercent)) :
                (mScreenWidth * dragPercent - mCloudWidth) / 2;
        float offsetY = isRefreshing ? Utils.convertDpToPixel(mContext, 25) :
                (1.0f - dragPercent) * mParent.getTotalDragDistance() + Utils.convertDpToPixel(mContext, 25);

        Paint paint = new Paint();
        float alpha = (Math.max(0.5f, mCloudAlpha)) * 255;
        paint.setAlpha((int) alpha);

        matrix.postTranslate(offsetX, offsetY);
        canvas.drawBitmap(mCloud, matrix, paint);
    }

    private void drawSky(Canvas canvas) {
        Matrix matrix = mMatrix;
        matrix.reset();

        float dragPercent = Math.min(1f, Math.abs(mPercent));
        float offsetY = isRefreshing ? Utils.convertDpToPixel(mContext, 25) :
                (1.0f - dragPercent) * mParent.getTotalDragDistance() + Utils.convertDpToPixel(mContext, 25);

        float offsetX = (mScreenWidth / 2) - (mSky.getWidth() / 2);

        Paint paint = new Paint();
        float alpha = (Math.max(0.1f, isRefreshing ? 1 : dragPercent)) * 255;
        paint.setAlpha((int) alpha);

        matrix.postTranslate(offsetX, offsetY);
        canvas.drawBitmap(mSky, matrix, paint);
    }

    @Override
    protected void setupAnimations() {
        AnimationFactory factory = new AnimationFactory();
        mCloudAnimation = factory.getCloudAnimation(new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                mCloudAlpha = 1.0f - setVariable(interpolatedTime);
            }
        });
        mCloudAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                resetOrigins();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void setupPaint() {
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(Color.rgb(232, 52, 1));
        mBackgroundPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, mSenceHeight + top);
    }

    @Override
    public void setPercent(float percent, boolean invalidate) {
        setPercent(percent);
    }

    private void setPercent(float percent) {
        this.mPercent = percent;
    }

    @Override
    public void offsetTopAndBottom(int offset) {
        mTop += offset;
        invalidateSelf();
    }

    @Override
    public void start() {
        isRefreshing = true;
        mCloudAnimation.reset();

        mParent.startAnimation(mCloudAnimation);
    }

    @Override
    public void stop() {
        mParent.clearAnimation();
        isRefreshing = false;
    }

    @Override
    public boolean isRunning() {
        return false;
    }

    private float setVariable(float value) {
        invalidateSelf();
        return value;
    }

    private void resetOrigins() {
        setPercent(0);
        mCloudAlpha = 1;
    }

}
