package com.electricsoul.android.ui.activity;


import android.os.SystemClock;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.electricsoul.android.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest() {
//        ViewInteraction appCompatButton = onView(
//                allOf(withId(R.id.btn_location_selection), withText("location selection"), isDisplayed()));
//        appCompatButton.perform(click());
//        SystemClock.sleep(1000);
//        ViewInteraction recyclerView = onView(
//                allOf(withId(R.id.rv_locations),
//                        withParent(withId(R.id.select_location_conteiner)),
//                        isDisplayed()));
//        recyclerView.perform(actionOnItemAtPosition(1, click()));
        SystemClock.sleep(1000);
        ViewInteraction relativeLayout = onView(
                allOf(withId(R.id.profile_wrapper), isDisplayed()));
        relativeLayout.perform(click());
        SystemClock.sleep(1000);
        ViewInteraction relativeLayout2 = onView(
                allOf(withId(R.id.events_wrapper), isDisplayed()));
        relativeLayout2.perform(click());
        SystemClock.sleep(1000);
        ViewInteraction relativeLayout3 = onView(
                allOf(withId(R.id.search_wrapper), isDisplayed()));
        relativeLayout3.perform(click());
        SystemClock.sleep(1000);
        ViewInteraction searchAutoComplete = onView(
                allOf(withId(R.id.search_src_text),
                        withParent(allOf(withId(R.id.search_plate),
                                withParent(withId(R.id.search_edit_frame)))),
                        isDisplayed()));
        searchAutoComplete.perform(replaceText("xh"), closeSoftKeyboard());
        SystemClock.sleep(1000);
        ViewInteraction searchAutoComplete2 = onView(
                allOf(withId(R.id.search_src_text), withText("xh"),
                        withParent(allOf(withId(R.id.search_plate),
                                withParent(withId(R.id.search_edit_frame)))),
                        isDisplayed()));
        searchAutoComplete2.perform(pressImeActionButton());
        SystemClock.sleep(1000);
        ViewInteraction relativeLayout4 = onView(
                allOf(withId(R.id.festival_tab),
                        withParent(withId(R.id.tabs)),
                        isDisplayed()));
        relativeLayout4.perform(click());
        SystemClock.sleep(1000);
        ViewInteraction relativeLayout5 = onView(
                allOf(withId(R.id.event_tab),
                        withParent(withId(R.id.tabs)),
                        isDisplayed()));
        relativeLayout5.perform(click());
        SystemClock.sleep(1000);
        ViewInteraction relativeLayout6 = onView(
                allOf(withId(R.id.user_tab),
                        withParent(withId(R.id.tabs)),
                        isDisplayed()));
        relativeLayout6.perform(click());
        SystemClock.sleep(1000);
    }

}
