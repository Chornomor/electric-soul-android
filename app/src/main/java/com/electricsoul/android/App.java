package com.electricsoul.android;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;

/**
 * Created by slava on 14.11.16.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        initInstaBug();
    }

    private void initInstaBug() {
        new Instabug.Builder(this, getResources().getString(R.string.instabug_token))
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .build();
    }
}
