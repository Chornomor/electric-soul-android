package com.electricsoul.android.ui.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.electricsoul.android.R;

import com.electricsoul.android.model.LocationModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.electricsoul.android.ui.base.BaseFragment.isClickUnlock;

/**
 * Created by eugenetroyanskii on 10.11.16.
 */

public class SelectLocationAdapter extends RecyclerView.Adapter<SelectLocationAdapter.LocationViewHolder> {

    private ItemClickListener itemClick;
    private ArrayList<LocationModel> items;


    public SelectLocationAdapter(LinkedHashMap<String, ArrayList<LocationModel>> mapItems) {
        this.items = new ArrayList<>();
        fillListItemsFromMap(mapItems);
    }


    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_location, parent, false);
        return new LocationViewHolder(v);
    }


    /*if item don't contains name
     we setup it as country holder*/
    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        if (items.get(position).getName() == null) {
            holder.itemView.setPadding(100, 15, 0, 0);
            holder.content.setText(items.get(position).getCountry());
            holder.content.setTypeface(Typeface.DEFAULT_BOLD);
        } else {
            holder.itemView.setPadding(150, 0, 0, 0);
            holder.content.setText(items.get(position).getName());
            holder.content.setTypeface(Typeface.DEFAULT);
            holder.location = items.get(position);
            holder.itemView.setTag(holder);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isClickUnlock()) {
                        LocationViewHolder viewHolder = (LocationViewHolder) view.getTag();
                        itemClick.onItemClick(view, viewHolder.getAdapterPosition(), viewHolder.location);
                    }
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public class LocationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item_select_location)
        TextView content;

        LocationModel location;


        public LocationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    private void fillListItemsFromMap(LinkedHashMap<String, ArrayList<LocationModel>> mapItems) {
        for (Map.Entry entry : mapItems.entrySet()) {
            items.add(new LocationModel(entry.getKey().toString()));
            ArrayList<LocationModel> modelsList = (ArrayList)entry.getValue();
            for (LocationModel model : modelsList) {
                items.add(model);
            }
        }
    }


    /***
     * listener for get item by click
     ***/
    public void setItemClickListener(ItemClickListener listener) {
        itemClick = listener;
    }


    public interface ItemClickListener {
        void onItemClick(View view, int position, LocationModel locationModel);
    }
}
