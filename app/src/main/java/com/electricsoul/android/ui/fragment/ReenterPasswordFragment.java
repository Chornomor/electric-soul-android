package com.electricsoul.android.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.KeyboardUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eugenetroyanskii on 14.12.16.
 */

public class ReenterPasswordFragment extends BaseFragment {

    private View root;

    @BindView(R.id.sign_up_et_pswd_confirmed)
    EditText confirmedPassword;

    @BindView(R.id.btn_sign_up_done)
    Button btnDone;

    @BindView(R.id.check_pswd_fl)
    LinearLayout content;

    public OnNextClickListener onNextClick;
    private Context context;
    private String initialPassword;


    public static ReenterPasswordFragment getInstance(String password){
        ReenterPasswordFragment fragment = new ReenterPasswordFragment();
        fragment.initialPassword = password;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_reenter_password, container, false);
        ButterKnife.bind(this, root);
        context = getActivity();

        KeyboardUtil.initFocusAndShowKeyboard(confirmedPassword, context);
        setupOnClickListeners();
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        initKeyboardListener(root);
    }


    @Override
    public void onPause() {
        super.onPause();
        removeGlobalLayoutListener();
    }


    private void signUpUser() {
        if(confirmedPassword.getText().toString().equals(initialPassword)) {
            onNextClick.onPasswordMatched();
            getActivity().onBackPressed();
        } else {
            Toast.makeText(context, R.string.passwords_do_not_match, Toast.LENGTH_LONG).show();
            KeyboardUtil.initFocusAndShowKeyboard(confirmedPassword, context);
        }
    }


    private TextView.OnEditorActionListener keyboardDoneListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                signUpUser();
            }
            return false;
        }
    };


    private void setupOnClickListeners() {
        content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUpUser();
            }
        });

        confirmedPassword.setOnEditorActionListener(keyboardDoneListener);
    }


    public void setOnNextClick(OnNextClickListener listener) {
        onNextClick = listener;
    }


    public interface OnNextClickListener {
        void onPasswordMatched();
    }
}
