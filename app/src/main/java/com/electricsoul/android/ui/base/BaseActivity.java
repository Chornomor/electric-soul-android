package com.electricsoul.android.ui.base;

import android.support.v7.app.AppCompatActivity;

import com.electricsoul.android.ui.dialog.LoadingDialog;
import com.electricsoul.android.utils.KeyboardUtil;

/**
 * Created by denisshovhenia on 09.11.16.
 */

public class BaseActivity extends AppCompatActivity {

    private LoadingDialog loadingDialog;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        KeyboardUtil.hideKeyboard(this);
        getFragmentManager().popBackStack();
    }

    @Override
    protected void onResume() {
        super.onResume();
        KeyboardUtil.hideKeyboard(this);
    }


    public void showProgressDialog() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(this);
        }
        loadingDialog.showProgressDialog();
    }


    public void cancelProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.cancelProgressDialog();
        }
    }
}
