package com.electricsoul.android.ui.views;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by alexchern on 06.12.16.
 */

public class SquareLayout extends LinearLayout {


    public SquareLayout(Context context) {
        super(context);
    }


    public SquareLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public SquareLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SquareLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, w, oldw, oldh);
    }


    @Override
    protected void onMeasure(int width, int height) {
        super.onMeasure(width, width);
    }
}
