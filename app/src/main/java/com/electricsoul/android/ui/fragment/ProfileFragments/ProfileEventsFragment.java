package com.electricsoul.android.ui.fragment.ProfileFragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.electricsoul.android.R;
import com.electricsoul.android.model.EventModel;
import com.electricsoul.android.model.EventResultModel;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.EventsTabAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.fragment.EventCommentsFragment;
import com.electricsoul.android.ui.fragment.EventDetailsFragment;
import com.electricsoul.android.ui.fragment.ProfileFragment;
import com.google.firebase.analytics.FirebaseAnalytics;

public class ProfileEventsFragment extends BaseFragment implements EventsTabAdapter.OnEventsClickListener
        , EventsTabAdapter.OnRSVPClickListener, EventsTabAdapter.OnCommentClickListener {

    private RecyclerView events;
    private EventsTabAdapter eventsAdapter;
    private boolean forProfileScreen;
    private boolean withBackButton;
    private ProfileFragment.OnScrollChangeProfileListener scrollChangeProfileListener;


    public static ProfileEventsFragment newInstance(boolean forProfileScreen, boolean withBackButton
            , ProfileFragment.OnScrollChangeProfileListener scrollChangeProfileListener) {
        ProfileEventsFragment fragment = new ProfileEventsFragment();
        fragment.forProfileScreen = forProfileScreen;
        fragment.withBackButton = withBackButton;
        fragment.scrollChangeProfileListener = scrollChangeProfileListener;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile_events, container, false);
        events = (RecyclerView) root.findViewById(R.id.scroll);
        events.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (recyclerView.getScrollState()) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        scrollChangeProfileListener.onScrollChangeProfile();
                        break;
                }

            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        events.setLayoutManager(new LinearLayoutManager(getContext()));
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        trackOpenMyEventFirebaseEvent();
    }


    private void trackOpenMyEventFirebaseEvent() {
        FirebaseAnalytics.getInstance(getContext()).logEvent(ConstantsFireBase.EVENT_CHECKIN_EVENT, new Bundle());
    }


    @Nullable
    @Override
    public void onEventsItemClick(View view, int position, EventModel event) {
        switch (view.getId()) {
            case R.id.event_root:
                if(((MainActivity)getActivity()).isNetworkAvailable()) {
                    ((MainActivity) getActivity()).setFragmentWithBackStack(EventDetailsFragment.newInstance(event.getId()), null);
                }
                break;
        }
    }


    public void updateEventsList(EventResultModel eventModel, String userId) {
        eventsAdapter = new EventsTabAdapter(getContext(), eventModel, true, userId);
        eventsAdapter.setEventsClickListener(this);
        eventsAdapter.setForProfileScreen(forProfileScreen);
        eventsAdapter.setWithBackButton(withBackButton);
        eventsAdapter.setRSVPClickListener(this);
        eventsAdapter.setCommentClickListener(this);
        events.setAdapter(eventsAdapter);
    }


    @Override
    public void onCommentClickListener(String eventId, String eventName) {
        ((MainActivity) getActivity()).setFragmentWithBackStack(EventCommentsFragment.getInstance(eventId, eventName), null);
    }


    @Override
    public void onRSVPClickListener(int itemsCount) {

    }
}
