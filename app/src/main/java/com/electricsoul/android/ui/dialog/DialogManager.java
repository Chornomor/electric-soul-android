package com.electricsoul.android.ui.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.electricsoul.android.R;
import com.electricsoul.android.utils.LogUtil;
import com.stripe.android.model.Card;

/**
 * Created by admin on 07.01.2017.
 */

public class DialogManager {

    public static void showConfirmDialog(Context context, int title, int message
            , DialogInterface.OnClickListener positiveClick
            , DialogInterface.OnClickListener negativeClick) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton(R.string.yes_button, positiveClick);
        dialog.setNegativeButton(R.string.no_button, negativeClick);
        AlertDialog alert = dialog.create();
        alert.show();
    }


    public static void showPayConfirmDialog(Context context
            , DialogInterface.OnClickListener positiveClick) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.confirm_string);
        dialog.setMessage(R.string.please_confirm);
        dialog.setPositiveButton(R.string.confirm_string, positiveClick);
        dialog.setNegativeButton(R.string.dialog_cancel, null);
        AlertDialog alert = dialog.create();
        alert.show();
    }


    public static void showSrtipePayConfirmDialog(Context context, Card card
            , DialogInterface.OnClickListener positiveClick) {

        StringBuffer message = new StringBuffer();
        message.append(context.getString(R.string.card_info));
        message.append("\n\n");
        if (card.getName() != null) {
            message.append(context.getString(R.string.card_info_name, card.getName()));
            message.append("\n");
        }
        if (card.getBrand() != null) {
            message.append(context.getString(R.string.card_info_type, card.getBrand()));
            message.append("\n");
        }
        if (card.getLast4() != null) {
            message.append(context.getString(R.string.card_info_number, getNumber(card.getNumber())));
            message.append("\n\n");
        }
        message.append(context.getString(R.string.please_confirm));

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.confirm_string);
        dialog.setMessage(message);
        dialog.setPositiveButton(R.string.confirm_string, positiveClick);
        dialog.setNegativeButton(R.string.dialog_cancel, null);
        AlertDialog alert = dialog.create();
        alert.show();
    }


    private static String getNumber(String text){
        StringBuffer number = new StringBuffer();
        int index = 0;
        while (index < text.length()) {
            number.append(text.substring(index, Math.min(index + 4, text.length())) + " ");
            index += 4;
        }
        return number.toString();
    }


    public static void showDisableAndroidPayDialog(Context context
            , DialogInterface.OnClickListener positiveClick
            , DialogInterface.OnClickListener negativeClick) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.dialog_info_title);
        dialog.setMessage(R.string.go_to_gp);
        dialog.setPositiveButton(R.string.button_go, positiveClick);
        dialog.setNegativeButton(R.string.button_another, negativeClick);
        AlertDialog alert = dialog.create();
        alert.show();
    }


    public static void showSimpleDialog(Context context, int title, int message
            , DialogInterface.OnClickListener positiveClick) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton(R.string.signup_btn_ok, positiveClick);
        AlertDialog alert = dialog.create();
        alert.show();
    }


    public static void showStripeErrorDialog(Context context, String message) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.validation_errors);
        dialog.setMessage(message);
        dialog.setPositiveButton(R.string.signup_btn_ok, null);
        AlertDialog alert = dialog.create();
        alert.show();
    }
}
