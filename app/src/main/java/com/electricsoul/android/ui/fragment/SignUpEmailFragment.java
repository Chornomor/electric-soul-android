package com.electricsoul.android.ui.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.FacebookManager;
import com.electricsoul.android.manager.MixpanelManager;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.Regex;
import com.facebook.AccessToken;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yuliasokolova on 14.11.16.
 */

public class SignUpEmailFragment extends BaseFragment {

    public static final String TAG_SIGN_UP = "sign up";

    private View rootView;

    @BindView(R.id.login_et_email)
    EditText etEmail;

    @BindView(R.id.tv_auth_subtitle)
    TextView tvSubtitle;

    @BindView(R.id.auth_title)
    TextView tvTitle;

    @BindView(R.id.ll_font_root)
    LinearLayout layoutFontRoot;

    @BindView(R.id.tv_bottom_panel)
    TextView tvBottom;

    private boolean isLoggingIn;
    private String newUserEmail = "";
    private String userFbToken = "";
    private String fireBaseToken;
    private FacebookManager facebookManager;


    public static SignUpEmailFragment getInstance(String newUserEmail, String fbToken) {
        SignUpEmailFragment fragment = new SignUpEmailFragment();
        fragment.newUserEmail = newUserEmail;
        fragment.userFbToken = fbToken;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_signup_email, container, false);
        ButterKnife.bind(this, rootView);

        setupViews();
        KeyboardUtil.initFocusAndShowKeyboard(etEmail, getContext());
        isLoggingIn = false;
        facebookManager = FacebookManager.getInstance(getContext());
        fireBaseToken = FirebaseInstanceId.getInstance().getToken();

        initFonts();
        return rootView;
    }


    private void initFonts() {
        Typeface typeface_black = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_BLACK);
        Typeface typeface_medium = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        FontUtil.impl(layoutFontRoot, typeface_medium);
        tvBottom.setTypeface(typeface_black);
        tvTitle.setTypeface(typeface_black);
    }


    private void setupViews() {
        etEmail.setText(newUserEmail);
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                              @Override
                                              public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                                                  getSignUpPhoto();
                                                  return false;
                                              }
                                          }

        );
    }


    @OnClick(R.id.btn_sign_in_email)
    void onLogInClick() {
        ((MainActivity) getActivity()).setFragmentWithoutBackStack(AuthFragment.newInstance(), AuthFragment.AUTH_TAG);
    }


    @OnClick(R.id.btn_log_in_email)
    void onNextClick() {
        getSignUpPhoto();
    }


    @OnClick(R.id.btn_fb)
    void onFBClick() {
        if (!isLoggingIn) {
            isLoggingIn = true;
            showProgressDialog();
            facebookManager.login(getActivity(), new FacebookManager.LoginListener() {

                @Override
                public void onSuccess() {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        SharedPrefManager.getInstance(getActivity()).storeFbToken(AccessToken.getCurrentAccessToken().getToken());
                        RESTClient.getInstance().requestFacebookLogin(fireBaseToken, fbLoginCallback);
                    } else {
                        fbLoginCallback.onFailure(null, null);
                    }
                    isLoggingIn = false;
                    cancelProgressDialog();
                }


                @Override
                public void onError(FacebookManager.LoginError error) {
                    switch (error) {
                        case NOT_ALL_PERMISSIONS_GRANTED:
                            Toast.makeText(getContext(), R.string.facebook_login_fail_permissions, Toast.LENGTH_SHORT)
                                    .show();
                            break;
                        case CANCELED_BY_USER:
                            Toast.makeText(getContext(), R.string.facebook_login_cancelled, Toast.LENGTH_SHORT).show();
                            break;
                        case FACEBOOK_ERROR:
                            facebookManager.logout();
                            Toast.makeText(getContext(), R.string.social_login_failed, Toast.LENGTH_LONG).show();
                            break;
                    }
                    isLoggingIn = false;
                    cancelProgressDialog();
                }
            });
        }
    }


    Callback<UserModel> fbLoginCallback = new Callback<UserModel>() {
        @Override
        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
            cancelProgressDialog();
            switch (response.code()) {
                case ConstantsAPI.CODE_200_SUCCESS:
                    if (response.body().getAuthToken() == null) {
                        String fbToken = SharedPrefManager.getInstance(getActivity()).getStoredFbToken();
                        SharedPrefManager.getInstance(getActivity()).storeFbToken("");
                        if (response.body().getEmail() == null) {
                            Toast.makeText(getActivity(), R.string.signup_info_enter_email, Toast.LENGTH_SHORT).show();
                        } else {
                            ((MainActivity) getActivity()).setFragmentWithBackStack(
                                    SignUpUserPhoto.getInstance(response.body().getEmail(), fbToken), SignUpUserPhoto.TAG_SIGN_UP_PHOTO);
                        }
                    } else {
                        storeFbLoginUserData(response.body());
                        trackLoginFbFirebaseEvent(response.body().getUserName());
                        startActivity(new Intent(getActivity(), MainActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    }
                    break;
                case ConstantsAPI.CODE_400_BAD_REQUEST:
                    Toast.makeText(getActivity(), R.string.error_wrong_email_or_password, Toast.LENGTH_LONG).show();
                    break;
                case ConstantsAPI.CODE_403_VEREFIY_ACCOUNT:
                    Toast.makeText(getActivity(), R.string.error_confirm_email, Toast.LENGTH_SHORT).show();
                    break;
            }

        }


        @Override
        public void onFailure(Call<UserModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private void storeFbLoginUserData(UserModel userModel) {
        SharedPrefManager.getInstance(getActivity()).storeEmail(userModel.getEmail());
        SharedPrefManager.getInstance(getActivity()).storeUserId(userModel.getId());
        SharedPrefManager.getInstance(getActivity()).storeAuthToken(userModel.getAuthToken());
        SharedPrefManager.getInstance(getActivity()).storeUserName(userModel.getUserName());
        MixpanelManager.registerMixpanel(getContext(), fireBaseToken, userModel);
    }


    private void trackLoginFbFirebaseEvent(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsFireBase.USER_NAME_PROPERTY, name);
        FirebaseAnalytics.getInstance(getContext()).logEvent(ConstantsFireBase.LOGIN_EVENT, bundle);
    }


    private void getSignUpPhoto() {
        if (Regex.isValidEmail(etEmail.getText().toString())) {
            ((MainActivity) getActivity()).setFragmentWithBackStack(SignUpUserPhoto.getInstance(etEmail.getText().toString(), userFbToken), null);
        } else {
            if (etEmail.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), R.string.signup_info_enter_email, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), R.string.error_invalid_email, Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        KeyboardUtil.hideKeyboard(getActivity());
        initKeyboardListener(rootView);
    }


    @Override
    public void onPause() {
        super.onPause();
        removeGlobalLayoutListener();
    }


}
