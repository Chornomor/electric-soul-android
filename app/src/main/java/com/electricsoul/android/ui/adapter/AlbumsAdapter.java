package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.EventModel;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.utils.PixelUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.electricsoul.android.ui.base.BaseFragment.isClickUnlock;

/**
 * Created by alexchern on 16.11.16.
 */

public class AlbumsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private ArrayList<EventModel> eventPhoto = new ArrayList<>();
    private Context context;
    private PhotosAdapter.onPhotoItemClickListenen photoItemClickListenen;
    private boolean forProfileScreen = false;
    private boolean isEmptyEventsList = false;


    public AlbumsAdapter(Context context, ArrayList<EventModel> eventPhoto) {
        this.context = context;
        this.eventPhoto = eventPhoto;
    }


    public boolean isHeader(int position) {
        return position == 0;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_albums, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isEmptyEventsList) {
            bindEmptyItemViewHolder(holder, position);
        } else {
            bindItemViewHolder(holder, position);
        }
    }


    private void bindItemViewHolder(final RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).itemView.setTag(position);

        fillFanImages(holder, position);

        ((ViewHolder) holder).eventName.setText(eventPhoto.get(position).getName());
        ((ViewHolder) holder).imageCount.setText(eventPhoto.get(position).getFanImages().size() + " " + context.getString(R.string.profile_photos_tab));
        ((ViewHolder) holder).emptyItem.setVisibility(View.GONE);
        ((ViewHolder) holder).imagesLayout.setVisibility(View.VISIBLE);
        ((ViewHolder) holder).imagesLayout.setTag(position);
        ((ViewHolder) holder).imagesLayout.setOnClickListener(this);
    }


    private void fillFanImages(final RecyclerView.ViewHolder holder, int position) {
        FanImageModel fanImageModel1 = null;
        FanImageModel fanImageModel2 = null;
        FanImageModel fanImageModel3 = null;

        ((ViewHolder) holder).image2.setVisibility(View.INVISIBLE);
        ((ViewHolder) holder).image3.setVisibility(View.INVISIBLE);
        try {
            fanImageModel1 = eventPhoto.get(position).getFanImages().get(0);
            fanImageModel2 = eventPhoto.get(position).getFanImages().get(1);
            fanImageModel3 = eventPhoto.get(position).getFanImages().get(3);
        } catch (IndexOutOfBoundsException e) {
        }
        if (fanImageModel1 != null && fanImageModel1.hasFullImage()) {
            Picasso.with(context).load(fanImageModel1.getImage().getFullSize()).placeholder(R.drawable.ic_event_empty).fit().centerCrop()
                    .into(((ViewHolder) holder).image1);
        }
        if (fanImageModel2 != null && fanImageModel2.hasFullImage()) {
            ((ViewHolder) holder).image2.setVisibility(View.VISIBLE);
            Picasso.with(context).load(fanImageModel2.getImage().getFullSize()).placeholder(R.drawable.ic_event_empty).fit().centerCrop()
                    .into(((ViewHolder) holder).image2);
        }
        if (fanImageModel3 != null && fanImageModel3.hasFullImage()) {
            ((ViewHolder) holder).image3.setVisibility(View.VISIBLE);
            Picasso.with(context).load(fanImageModel3.getImage().getFullSize()).placeholder(R.drawable.ic_event_empty).fit().centerCrop()
                    .into(((ViewHolder) holder).image3);
        }
        if (fanImageModel2 == null && fanImageModel3 == null) {
            ((ViewHolder) holder).layout.setVisibility(View.GONE);
        }
    }


    private void bindEmptyItemViewHolder(final RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).emptyItem.setVisibility(View.VISIBLE);
        ((ViewHolder) holder).imagesLayout.setVisibility(View.GONE);
        if (isHeader(position)) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = new StaggeredGridLayoutManager
                    .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setFullSpan(true);
            ((ViewHolder) holder).itemView.setLayoutParams(layoutParams);
        }
    }


    @Override
    public int getItemCount() {
        if (forProfileScreen) {
            if (eventPhoto.size() == 0) {
                isEmptyEventsList = true;
                return eventPhoto.size() + 1;
            } else {
                return eventPhoto.size();
            }
        } else {
            return eventPhoto.size();
        }
    }


    public void setForProfileScreen(boolean forProfileScreen) {
        this.forProfileScreen = forProfileScreen;
    }


    @Override
    public void onClick(View view) {
        int position;
        if (isClickUnlock()) {
            switch (view.getId()) {
                case R.id.images_layout:
                    position = Integer.parseInt(String.valueOf(view.getTag()));
                    photoItemClickListenen.onItemClick(view, position, eventPhoto.get(position).getId());
                    break;
            }
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.empty_layout)
        FrameLayout emptyItem;

        @BindView(R.id.images_layout)
        LinearLayout imagesLayout;

        @BindView(R.id.layout)
        LinearLayout layout;

        @BindView(R.id.tv_event_name)
        TextView eventName;

        @BindView(R.id.tv_image_count)
        TextView imageCount;

        @BindView(R.id.image1)
        ImageView image1;

        @BindView(R.id.image2)
        ImageView image2;

        @BindView(R.id.image3)
        ImageView image3;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void setClickListener(PhotosAdapter.onPhotoItemClickListenen listener) {
        photoItemClickListenen = listener;
    }

}
