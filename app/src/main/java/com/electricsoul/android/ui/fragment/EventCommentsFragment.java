package com.electricsoul.android.ui.fragment;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.Comment;
import com.electricsoul.android.model.CommentResult;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.model.DetailEventModel;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.CommentsAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.Regex;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yuliasokolova on 08.12.16.
 */

public class EventCommentsFragment extends BaseFragment {

    private static final int FIRST_ADAPTER_POSITION = 0;
    private static final int ANIMATE_TIME_DURATION = 300;

    @BindView(R.id.comments_recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.et_message)
    EditText et_message;

    @BindView(R.id.iv_send_message)
    ImageView iv_send_message;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView tvToolbarTitle;

    @BindView(R.id.ll_scroll_to_first)
    LinearLayout llScrollToTopButton;

    @BindView(R.id.btn_scroll_to_first)
    ImageButton btnScrollToFirst;

    private MainActivity mainActivity;
    private LinearLayoutManager layoutManager;
    private CommentsAdapter commentsAdapter;
    private String currentEventId;
    private String currentEventName = "";
    private String userAuthToken = "";
    private String currentUserName = "";
    private ArrayList<Comment> commentList = new ArrayList<>();
//    private Map<String, String> mapMentionedUsers = new HashMap<>();


    public static EventCommentsFragment getInstance(String eventId, String eventName) {
        EventCommentsFragment fragment = new EventCommentsFragment();
        fragment.currentEventId = eventId;
        fragment.currentEventName = eventName;
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_comments, container, false);
        ButterKnife.bind(this, rootView);
        mainActivity = (MainActivity) getActivity();
        userAuthToken = SharedPrefManager.getInstance(mainActivity).getStoredAuthToken();
        currentUserName = SharedPrefManager.getInstance(mainActivity).getStoredUserName();
        if (currentEventName == null) {
            RESTClient.getInstance().getEventById(currentEventId, requestEventByIdCallback);
        }
        initSendButton();
        addToolbarBackArrow(toolbar);
        setFonts();
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        KeyboardUtil.hideKeyboard(getActivity());
        mainActivity.hideBottomToolbar();
        showProgressDialog();
        RESTClient.getInstance().getEventComments(userAuthToken, currentEventId, getCommentsCallback);
    }


    @Override
    public void onPause() {
        super.onPause();
        KeyboardUtil.hideKeyboard(getActivity());
        mainActivity.showBottomToolbar();
        commentsAdapter = null;
    }


    private void setFonts() {
        Typeface typeface_extrabold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_EXTRA_BOLD);
        Typeface typeface_medium = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        tvToolbarTitle.setTypeface(typeface_extrabold);
        et_message.setTypeface(typeface_medium);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            et_message.setLetterSpacing(0.06f);
            tvToolbarTitle.setLetterSpacing(0.06f);
        }
    }


    private void initSendButton() {
        iv_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((MainActivity) getActivity()).isNetworkAvailable()) {
                    String message = Regex.removeMultiSpace(et_message.getText().toString());
                    if (!message.isEmpty()) {
                        showProgressDialog();
                        String mentionedUsers = prepareMentionedUsers(message);
                        RESTClient.getInstance().postEventComment(userAuthToken, currentEventId, message, mentionedUsers, sendCommentCallback);
                        KeyboardUtil.hideKeyboard(mainActivity);
                        trackSendCommentFirebaseEvent(currentEventName, message);
                        et_message.setText("");
                        if (commentsAdapter != null) {
                            commentsAdapter.clearCheckedItem();
                            recycler_view.smoothScrollToPosition(commentsAdapter.getItemCount());
                        }
                    } else {
                        Toast.makeText(mainActivity, R.string.error_empty_message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    private void initRecyclerView() {
        layoutManager = new LinearLayoutManager(mainActivity);
        recycler_view.setLayoutManager(layoutManager);
        setAdapter();
        commentsAdapter.setClickListener(new CommentsAdapter.ClicksListener() {
            @Override
            public void onItemClick(String name, String userId) {
//                mapMentionedUsers.put(name, userId);
                String msg = et_message.getText().toString();
                String userName = msg.isEmpty() ? "@" + name + " " : " @" + name + " ";
                String directMessage = msg + userName;
                et_message.setText(directMessage);
                et_message.setSelection(et_message.getText().length());
                KeyboardUtil.initFocusAndShowKeyboard(et_message, mainActivity);
            }


            @Override
            public void onLikesClick(String commentId) {
                likeCommentRequest(commentId);
            }


            @Override
            public void onPhotoClick(String userId) {
                ((MainActivity) getActivity()).setFragmentWithBackStack(ProfileFragment.newInstance(userId, true), null);
            }
        });
        recycler_view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtil.hideKeyboard(mainActivity);
                return false;
            }
        });

        btnScrollToFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recycler_view.smoothScrollToPosition(FIRST_ADAPTER_POSITION);
            }
        });

        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisiblePosition = 0;
                if (layoutManager != null) {
                    firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                }
                if (firstVisiblePosition < 1) {
                    animateScrollToTopButtonVisibility(0f);
                } else {
                    animateScrollToTopButtonVisibility(1f);
                }
            }
        });
    }


    private void setAdapter() {
        if (commentsAdapter == null) {
            commentsAdapter = new CommentsAdapter(mainActivity);
        }
        recycler_view.setAdapter(commentsAdapter);
        commentsAdapter.updateComments(commentList);
        recycler_view.scrollToPosition(commentList.size() - 1);
    }


    private String prepareMentionedUsers(String msg) {
        StringBuffer users = new StringBuffer();
        String userName = "";
        int fromIndex = 0;
        int indexOfName = msg.indexOf("@");
        while (indexOfName > -1) {
            indexOfName++;
            fromIndex = indexOfName;
            int space = msg.indexOf(" ", fromIndex);
            if (space < 0){
                space = msg.length();
            }
            userName = msg.substring(indexOfName, space);
            if (!userName.equals(currentUserName)) {
                users.append(userName + ",");
            }
            indexOfName = msg.indexOf("@", fromIndex);
        }

        String result = users.toString();
        if (!result.isEmpty()) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }


    private void animateScrollToTopButtonVisibility(float alpha) {
        llScrollToTopButton.animate()
                .alpha(alpha)
                .setDuration(ANIMATE_TIME_DURATION)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }


    private void likeCommentRequest(String commentId) {
        if (((MainActivity) getActivity()).isNetworkAvailable()) {
            RESTClient.getInstance().likeEventComment(userAuthToken, commentId, new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {

                }


                @Override
                public void onFailure(Call<Object> call, Throwable t) {

                }
            });
        }
    }


    private Callback<JSONObject> sendCommentCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
            RESTClient.getInstance().getEventComments(userAuthToken, currentEventId, getCommentsCallback);
        }


        @Override
        public void onFailure(Call<JSONObject> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private Callback<CommentResult> getCommentsCallback = new Callback<CommentResult>() {
        @Override
        public void onResponse(Call<CommentResult> call, Response<CommentResult> response) {
            if (response.body() != null) {
                commentList = response.body().getResults();
                Collections.reverse(commentList);
                cancelProgressDialog();
                if (commentsAdapter == null) {
                    initRecyclerView();
                } else {
                    setAdapter();
                }
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<CommentResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };

    Callback<DetailEventModel> requestEventByIdCallback = new Callback<DetailEventModel>() {

        @Override
        public void onResponse(Call<DetailEventModel> call, Response<DetailEventModel> response) {
            if (response.body() != null) {
                currentEventName = response.body().getName();
            }
        }


        @Override
        public void onFailure(Call<DetailEventModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private void trackSendCommentFirebaseEvent(String name, String comment) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsFireBase.EVENT_NAME_PROPERTY, name);
        bundle.putString(ConstantsFireBase.COMMENT_TEXT_PROPERTY, comment);
        FirebaseAnalytics.getInstance(mainActivity).logEvent(ConstantsFireBase.EVENT_COMMENT_EVENT, bundle);
    }
}
