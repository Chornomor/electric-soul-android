package com.electricsoul.android.ui.fragment;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.model.DetailEventModel;
import com.electricsoul.android.model.SearchModel;
import com.electricsoul.android.model.SearchResultModel;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.model.elasticModel.EventModelElastic;
import com.electricsoul.android.model.elasticModel.EventResultModelElastic;
import com.electricsoul.android.model.elasticModel.EventSearchRequestModel;
import com.electricsoul.android.model.elasticModel.UserModelElastic;
import com.electricsoul.android.model.elasticModel.UserResultModelElastic;
import com.electricsoul.android.model.elasticModel.UserSearchRequestModel;
import com.electricsoul.android.network.clients.ElasticRESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.EventsAdapter;
import com.electricsoul.android.ui.adapter.elastic.EventsAdapterElastic;
import com.electricsoul.android.ui.adapter.elastic.UsersAdapterElastic;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.LogUtil;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denisshovhenia on 09.11.16.
 */

public class SearchFragment extends BaseFragment implements EventsAdapterElastic.ClicksListener {

    public static final long DELAY_BEFORE_START_SEARCH = 500;

    public static final int USERS_TAB = 0;
    public static final int EVENTS_TAB = 1;
    public static final int FESTIVALS_TAB = 2;

    public static final int FIRST_PAGE_REQUEST_NUMBER = 1;
    public static final String FESTIVAL_REQUEST_TAG = "y";
    public static final String EVENT_REQUEST_TAG = "n";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.users_tab_selected)
    View usersTabSelected;

    @BindView(R.id.events_tab_selected)
    View eventsTabSelected;

    @BindView(R.id.festival_tab_selected)
    View festivalTabSelected;

    @BindView(R.id.festival_tab)
    TextView festivalTabTitle;

    @BindView(R.id.user_tab)
    TextView userTabTitle;

    @BindView(R.id.event_tab)
    TextView eventTabTitle;

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.iv_search_cancel_icon)
    ImageView ivCancel;

    private EventsAdapterElastic eventsAdapter;
    private ArrayList<EventModelElastic> eventElasticListForTab = new ArrayList<>();

    private EventsAdapter festivalsAdapter;
    private ArrayList<DetailEventModel> festivalListForTab = new ArrayList<>();

    private UsersAdapterElastic usersAdapter;
    private ArrayList<UserModelElastic> userElasticListForTab = new ArrayList<>();

    private MainActivity mMainActivity;
    private SearchResultModel searchResultModel;
    private Timer timer;
    private String lastSearchWord;


    public static SearchFragment newInstance() {
        return new SearchFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, root);
        mMainActivity = (MainActivity) getActivity();

        selectTab(mMainActivity.getLastTabOfSearchScreen());

        initSearch();

        initTabs();

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtil.hideKeyboard(mMainActivity);
                return false;
            }
        });

        return root;
    }


    private void initSearch() {
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
                clearEventList();
                clearUserList();
                KeyboardUtil.initFocusAndShowKeyboard(etSearch, mMainActivity);
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            String searchFilter = "";


            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            @Override
            public void afterTextChanged(Editable editable) {
                searchFilter = etSearch.getText().toString();
                if (searchFilter.equals("")) {
                    ivCancel.setVisibility(View.GONE);
                } else {
                    ivCancel.setVisibility(View.VISIBLE);
                }
                mMainActivity.setmLastSearchQuery(searchFilter);
                lastSearchWord = searchFilter;
                startSearchingTimerTask(searchFilter);
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                KeyboardUtil.hideKeyboard(getActivity());
                return true;
            }
        });

        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) KeyboardUtil.hideKeyboard(getActivity());
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        KeyboardUtil.hideKeyboard(getActivity());
    }


    private void initTabs() {
        userTabTitle.setOnClickListener(onTabChangeListener);
        eventTabTitle.setOnClickListener(onTabChangeListener);
        festivalTabTitle.setOnClickListener(onTabChangeListener);

        initFonts();
    }


    private void initFonts() {
        Typeface typeface_bold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_KHULA_SEMI_BOLD);
        Typeface typeface_search = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_EXTRA_BOLD);
        festivalTabTitle.setTypeface(typeface_bold);
        eventTabTitle.setTypeface(typeface_bold);
        userTabTitle.setTypeface(typeface_bold);
        etSearch.setTypeface(typeface_search);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            festivalTabTitle.setLetterSpacing(0.1f);
            eventTabTitle.setLetterSpacing(0.1f);
            userTabTitle.setLetterSpacing(0.1f);
            etSearch.setLetterSpacing(0.1f);
        }
    }


    private View.OnClickListener onTabChangeListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.festival_tab:
                    selectTab(FESTIVALS_TAB);
                    break;
                case R.id.event_tab:
                    selectTab(EVENTS_TAB);
                    break;
                case R.id.user_tab:
                    selectTab(USERS_TAB);
                    break;
            }
        }
    };


    private void selectTab(int tab) {
        KeyboardUtil.hideKeyboard(mMainActivity);

        switch (tab) {
            case USERS_TAB:
                mMainActivity.setLastTabOfSearchScreen(USERS_TAB);
                updateTabsVisibility(View.VISIBLE, View.INVISIBLE, View.GONE);
                setupUsersAdapter();
                clearUserList();
                break;
            case EVENTS_TAB:
                mMainActivity.setLastTabOfSearchScreen(EVENTS_TAB);
                updateTabsVisibility(View.INVISIBLE, View.VISIBLE, View.GONE);
                setupEventAdapter();
                clearEventList();
                break;
            case FESTIVALS_TAB:
                mMainActivity.setLastTabOfSearchScreen(FESTIVALS_TAB);
                updateTabsVisibility(View.INVISIBLE, View.INVISIBLE, View.VISIBLE);
                setupFestivalAdapter();
                break;
        }
        getSearchResult(etSearch.getText().toString());
    }


    private void setupFestivalAdapter() {
        if (festivalsAdapter == null) {
            festivalsAdapter = new EventsAdapter(getActivity(), festivalListForTab, true);
        }
        setAdapter(festivalsAdapter);
    }


    private void setupEventAdapter() {
        if (eventsAdapter == null) {
            eventsAdapter = new EventsAdapterElastic(getActivity(), eventElasticListForTab, false);
        }
        eventsAdapter.setClickListener(this);
        setAdapter(eventsAdapter);
    }


    private void setupUsersAdapter() {
        if (usersAdapter == null) {
            usersAdapter = new UsersAdapterElastic(getActivity(), userElasticListForTab);
        } else {
            usersAdapter.initFollowingsList();
        }
        usersAdapter.setClickListener(new UsersAdapterElastic.ClicksListener() {
            @Override
            public void onItemClick(View view, int position, UserModelElastic user) {
                KeyboardUtil.hideKeyboard(mMainActivity);
                ((MainActivity) getActivity()).setFragmentWithBackStack(ProfileFragment.newInstance(user.getId(), true), null);
            }
        });
        setAdapter(usersAdapter);
    }


    private void updateTabsVisibility(int userTab, int eventTab, int festivalTab) {
        usersTabSelected.setVisibility(userTab);
        eventsTabSelected.setVisibility(eventTab);
        festivalTabSelected.setVisibility(festivalTab);
    }


    private void setAdapter(RecyclerView.Adapter recyclerAdapter) {
        recyclerView.setAdapter(recyclerAdapter);
    }


    private void getSearchResult(String searchSequence) {
        /*minimal string length for start searching is 3*/
        if (searchSequence.length() > 0 && mMainActivity.isNetworkAvailable()) {
            if (!((MainActivity) getActivity()).isUserAuthorized()) {
                openSignUpScreen();
                return;
            }
            switch (mMainActivity.getLastTabOfSearchScreen()) {
                case USERS_TAB:
                    showProgressDialog();
                    ElasticRESTClient.getInstance().searchUsersByWord(new UserSearchRequestModel(FIRST_PAGE_REQUEST_NUMBER - 1, searchSequence), searchUserElasticCallback);
                    break;
                case EVENTS_TAB:
                    showProgressDialog();
                    ElasticRESTClient.getInstance().searchEventsByWord(new EventSearchRequestModel(FIRST_PAGE_REQUEST_NUMBER - 1, searchSequence, false), searchEventElasticCallback);
                    break;
                case FESTIVALS_TAB:
                    break;
            }
        } else {
            clearAdapter(mMainActivity.getLastTabOfSearchScreen());
        }
    }


    @Override
    public void showProgressDialog() {
        progressBar.setVisibility(View.VISIBLE);
    }


    @Override
    public void cancelProgressDialog() {
        progressBar.setVisibility(View.GONE);
    }


    private void openSignUpScreen() {
        KeyboardUtil.hideKeyboard(mMainActivity);
        ((MainActivity) getActivity()).enableTab(MainActivity.PROFILE_TAB);
        etSearch.setText("");
    }


    private void clearAdapter(int tabType) {
        switch (tabType) {
            case USERS_TAB:
                clearUserList();
                if (usersAdapter != null) {
                    usersAdapter.updateItems(userElasticListForTab);
                }
                break;
            case EVENTS_TAB:
                clearEventList();
                if (eventsAdapter != null) {
                    eventsAdapter.updateItems(eventElasticListForTab);
                }
                break;
            case FESTIVALS_TAB:
                clearFestivalList();
                if (festivalsAdapter != null) {
                    festivalsAdapter.updateItems(festivalListForTab);
                }
                break;
        }
    }


    private void clearEventList() {
        eventElasticListForTab.clear();
        if (eventsAdapter != null) {
            eventsAdapter.updateItems(eventElasticListForTab);
        }
    }


    private void clearUserList() {
        userElasticListForTab.clear();
        if (eventsAdapter != null) {
            usersAdapter.updateItems(userElasticListForTab);
        }
    }


    private void clearFestivalList() {
        festivalListForTab.clear();
    }


    /**************************
     * Events callbacks
     **********************************/
    private Callback<EventResultModelElastic> searchEventElasticCallback = new Callback<EventResultModelElastic>() {
        @Override
        public void onResponse(Call<EventResultModelElastic> call, Response<EventResultModelElastic> response) {
            cancelProgressDialog();
            if (response.body() != null && !response.body().getEventsModelElastic().isEmpty()) {
                clearEventList();
                eventElasticListForTab = response.body().getEventsModelElastic();
                if (eventsAdapter == null) {
                    eventsAdapter = new EventsAdapterElastic(getActivity(), eventElasticListForTab, false);
                } else {
                    eventsAdapter.updateItems(eventElasticListForTab);
                }
                setAdapter(eventsAdapter);
            } else {
                if (getActivity() != null) {
                    clearEventList();
                    Toast.makeText(getActivity(), R.string.no_search_result, Toast.LENGTH_LONG).show();
                }
            }
        }


        @Override
        public void onFailure(Call<EventResultModelElastic> call, Throwable t) {
            LogUtil.logE("onFailure " + t.getMessage());
            cancelProgressDialog();
        }
    };


    /*********************************
     * Users callbacks
     ****************************************/
    private Callback<UserResultModelElastic> searchUserElasticCallback = new Callback<UserResultModelElastic>() {
        @Override
        public void onResponse(Call<UserResultModelElastic> call, Response<UserResultModelElastic> response) {
            cancelProgressDialog();
            if (response.body() != null && !response.body().getUsersModelElastic().isEmpty()) {
                clearUserList();
                userElasticListForTab = response.body().getUsersModelElastic();
                if (usersAdapter == null) {
                    usersAdapter = new UsersAdapterElastic(getActivity(), userElasticListForTab);
                } else {
                    usersAdapter.updateItems(userElasticListForTab);
                }
                setAdapter(usersAdapter);
            } else {
                if (getActivity() != null) {
                    clearUserList();
                    Toast.makeText(getActivity(), R.string.no_search_result, Toast.LENGTH_LONG).show();
                }
            }
        }


        @Override
        public void onFailure(Call<UserResultModelElastic> call, Throwable t) {
            LogUtil.logE("onFailure " + t.getMessage());
            cancelProgressDialog();
        }
    };


    private void startSearchingTimerTask(String searchWord) {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        timer = new Timer();

        SearchTimerTask timerTask = new SearchTimerTask(searchWord);
        timer.schedule(timerTask, DELAY_BEFORE_START_SEARCH);
    }


    @Override
    public void onItemClick(View view, int position, EventModelElastic model) {
        KeyboardUtil.hideKeyboard(getActivity());
        if (((MainActivity) getActivity()).isNetworkAvailable()) {
            mMainActivity.setFragmentWithBackStack(EventDetailsFragment.newInstance(model.getId()), null);
        }
    }


    public class SearchTimerTask extends TimerTask {

        String searchWord = "";


        public SearchTimerTask(String searchWord) {
            this.searchWord = searchWord;
        }


        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (searchWord.equals(lastSearchWord)) {
                            getSearchResult(searchWord);
                        }

                        if (timer != null) {
                            timer.cancel();
                            timer = null;
                        }
                    }
                });
            }
        }
    }
}