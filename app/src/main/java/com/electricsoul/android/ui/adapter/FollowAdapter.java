package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.FollowerResult;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.dialog.FollowDialog;
import com.electricsoul.android.utils.FontUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.electricsoul.android.ui.base.BaseFragment.isClickUnlock;


public class FollowAdapter extends RecyclerView.Adapter<FollowAdapter.ViewHolder> {

    private static final String FOLLOW_KEY = "1";
    private static final String NOT_FOLLOW_KEY = "0";
    private static final String MUTUAL_FOLLOW_TAG = "mutual";
    private static final String NOT_MUTUAL_FOLLOW_TAG = "not_mutual";

    private FollowAdapter.OnItemClickListener mClickListener;
    private FollowAdapter.OnItemUpdateListener updateListListener;
    private List<UserModel> mItemList = new ArrayList<>();
    private List<String> followerList = new ArrayList<>();
    private List<String> followingList = new ArrayList<>();

    private Context mContext;
    private MainActivity mMainActivity;

    private Typeface medium;

    private String userId;
    private String followKey;
    private String currentUserId;

    private int totalEventsCount;

    private boolean isOnSearchMode = false;
    private boolean isMyFollowersAdapter;
    private boolean isFollowingScreen;


    public FollowAdapter(Context context, String userId) {
        mContext = context;
        this.userId = userId;
        mMainActivity = (MainActivity) mContext;
        currentUserId = SharedPrefManager.getInstance(context).getStoredUserId();
        fillFollowLists();
        medium = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        notifyDataSetChanged();
    }


    public void fillFollowLists() {
        followerList = prepareFollowsIdList(mMainActivity.followers.getResults());
        followingList = prepareFollowsIdList(mMainActivity.followings.getResults());
    }


    public void setTotalCount(int totalEventsCount) {
        this.totalEventsCount = totalEventsCount;
    }


    public void setOnSearchMode(boolean onSearchMode) {
        isOnSearchMode = onSearchMode;
    }


    @Override
    public FollowAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_user, parent, false);

        return new FollowAdapter.ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final FollowAdapter.ViewHolder holder, int position) {
        final UserModel item = mItemList.get(position);
        holder.userName.setText(item.getUserName());
        holder.userName.setTypeface(medium);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.userName.setLetterSpacing(0.06f);
        }

        if (item.hasImageThumbnail()) {
            Picasso.with(mContext)
                    .load(item.getImage().getThumbnail()).placeholder(mContext.getResources().getDrawable(R.drawable.avatar)).fit().centerCrop().fit().centerCrop()
                    .into(holder.userPhoto);
        } else {
            holder.userPhoto.setImageResource(R.drawable.avatar);
        }

        if (isFollowing(item)) {
            setDrawable(holder.followingStatus, R.drawable.ic_follow_minus_red_icon);
        } else {
            setDrawable(holder.followingStatus, R.drawable.ic_plus_btn);
        }
        if (currentUserId.equals(item.getId())) {
            holder.friendshitIcon.setVisibility(View.INVISIBLE);
            holder.followingStatus.setVisibility(View.GONE);
        } else {
            holder.friendshitIcon.setVisibility(View.VISIBLE);
            holder.followingStatus.setVisibility(View.VISIBLE);
            setupIsFollowing(holder.followingStatus, item);
        }
        holder.followingStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickUnlock()) {
                    final String currentUserId = SharedPrefManager.getInstance(mContext).getStoredUserId();
                    if (!currentUserId.isEmpty()) {
                        if (!isFollowing(item)) {
                            new FollowDialog().showDialog(mContext, item, true, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    followKey = FOLLOW_KEY;
                                    setDrawable(holder.followingStatus, R.drawable.ic_follow_minus_red_icon);
                                    followingList.add(item.getId());
                                    mMainActivity.addToFollowings(item);
                                    RESTClient.getInstance().postFollowUser(SharedPrefManager.getInstance(mContext).getStoredAuthToken(), followKey, item.getId(), currentUserId, followCallback);
                                    isMutualFollow(holder, item);
                                }
                            });
                        } else {
                            new FollowDialog().showDialog(mContext, item, false, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    followKey = NOT_FOLLOW_KEY;
                                    isMyFollowersAdapter = currentUserId.equals(userId);
                                    setDrawable(holder.followingStatus, R.drawable.ic_plus_btn);
                                    followingList.remove(item.getId());
                                    mMainActivity.removeFromFollowings(item);
                                    totalEventsCount--;
                                    if (isFollowingScreen) {
                                        removeFromMyFollowers(item);
                                    }
                                    RESTClient.getInstance().postFollowUser(SharedPrefManager.getInstance(mContext).getStoredAuthToken(), followKey, item.getId(), currentUserId, followCallback);
                                    isMutualFollow(holder, item);
                                }
                            });
                        }
                    } else {
                        Toast.makeText(mContext, R.string.error_login_to_follow, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        isMutualFollow(holder, item);
        holder.userModel = item;
        holder.itemView.setTag(holder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FollowAdapter.ViewHolder viewHolder = (FollowAdapter.ViewHolder) v.getTag();
                mClickListener.onItemClick(v, viewHolder.getAdapterPosition(), viewHolder.userModel);
            }
        });

        if (!isOnSearchMode && mItemList.size() == position + 1 && position + 1 < totalEventsCount) {
            updateListListener.onItemListUpdate();
        }
    }


    private void setupIsFollowing(ImageView view, UserModel item) {
        if (isFollowing(item)) {
            setDrawable(view, R.drawable.ic_follow_minus_red_icon);
        } else {
            setDrawable(view, R.drawable.ic_plus_btn);
        }
    }


    public void setFollowingScreen(boolean followingScreen) {
        isFollowingScreen = followingScreen;
    }


    private void removeFromMyFollowers(UserModel user) {
        if (isMyFollowersAdapter) {
            mItemList.remove(user);
            updateListListener.onUnFollowed(user);
            notifyDataSetChanged();
        }
    }


    private boolean isFollowing(UserModel item) {
        return followingList.contains(item.getId());
    }


    private void isMutualFollow(FollowAdapter.ViewHolder holder, UserModel item) {
        if (followerList.contains(item.getId()) && followingList.contains(item.getId())) {
            setDrawable(holder.friendshitIcon, R.drawable.ic_follow_hand_selected_icon);
            holder.friendshitIcon.setTag(MUTUAL_FOLLOW_TAG);
        } else {
            setDrawable(holder.friendshitIcon, R.drawable.ic_follow_hand_unselected_icon);
            holder.friendshitIcon.setTag(NOT_MUTUAL_FOLLOW_TAG);
        }
    }


    private void setDrawable(ImageView imgView, int resourceId) {
        imgView.setImageDrawable(ContextCompat.getDrawable(mContext, resourceId));
    }


    @Override
    public int getItemCount() {
        return mItemList.size();
    }


    public void updateItems(ArrayList<UserModel> usersList) {
        mItemList = usersList;
        notifyDataSetChanged();
    }


    private ArrayList<String> prepareFollowsIdList(ArrayList<FollowerResult.Follower> followerList) {
        ArrayList<String> followsIds = new ArrayList<>();
        for (int i = 0; i < followerList.size(); ++i) {
            followsIds.add(followerList.get(i).getId());
        }
        return followsIds;
    }


    private Callback<String> followCallback = new Callback<String>() {
        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            if (followKey == FOLLOW_KEY) {
                trackFollowFirebaseEvent();
            } else {
                trackUnFollowFirebaseEvent();
            }
        }


        @Override
        public void onFailure(Call<String> call, Throwable t) {

        }
    };


    private void trackFollowFirebaseEvent() {
        FirebaseAnalytics.getInstance(mContext).logEvent(ConstantsFireBase.FOLLOW_USER_EVENT, new Bundle());
    }


    private void trackUnFollowFirebaseEvent() {
        FirebaseAnalytics.getInstance(mContext).logEvent(ConstantsFireBase.UNFOLLOW_USER_EVENT, new Bundle());
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_photo_image)
        ImageView userPhoto;

        @BindView(R.id.plus_minus_icon)
        ImageView followingStatus;

        @BindView(R.id.friendship_icon)
        ImageView friendshitIcon;

        @BindView(R.id.user_name_text)
        TextView userName;

        UserModel userModel;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public void setOnItemClickListener(FollowAdapter.OnItemClickListener listener) {
        mClickListener = listener;
    }


    public void setOnItemUpdateListener(FollowAdapter.OnItemUpdateListener listener) {
        updateListListener = listener;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, UserModel user);
    }


    public interface OnItemUpdateListener {
        void onItemListUpdate();


        void onUnFollowed(UserModel user);
    }
}