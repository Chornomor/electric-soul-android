package com.electricsoul.android.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.FacebookManager;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.dialog.DialogManager;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.GetPhotoUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.LogUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denisshovhenia on 24.11.16.
 */
public class ProfileSettingsFragment extends BaseFragment implements View.OnClickListener {

    public static final String USER_ID_EXTRA = "com.electricsoul.android.ui.fragment.USER_ID_EXTRA";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.header_wrapper)
    RelativeLayout focusAnchor;

    @BindView(R.id.toolbar_option)
    TextView toolbarOptionSave;

    @BindView(R.id.user_background_image)
    ImageView userBackgroundImage;

    @BindView(R.id.change_background_image)
    ImageView changeBackgroundImage;

    @BindView(R.id.change_profile_photo_image)
    ImageView changeProfilePhotoImage;

    @BindView(R.id.profile_photo_image)
    CircleImageView profilePhotoImage;

    @BindView(R.id.nick_name_edit_text)
    EditText nickNameEditText;

    @BindView(R.id.change_password_button)
    RelativeLayout changePasswordButton;

    @BindView(R.id.terms_and_conditions_button)
    RelativeLayout termsAndConditionsButton;

    @BindView(R.id.log_out_button)
    RelativeLayout logOutButton;

    @BindView(R.id.delete_profile_button)
    RelativeLayout deleteProfileButton;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.root_layout)
    LinearLayout rootLayout;

    private View root;

    private String mUserId;
    private String authToken;

    private GetPhotoUtil mGetPhotoUtil;

    private static final int REQUEST_PHOTO_PROFILE = 0;
    private static final int REQUEST_PHOTO_BACKGROUND_PROFILE = 1;
    private static final int ERROR_TEXT_POSITION = 3;

    MultipartBody.Part userPhoto;
    MultipartBody.Part userCoverImage;

    private String mLastUserName;

    private int mRequestedPhoto = -1;

    FacebookManager facebookManager;

    private MainActivity mMainActivity;


    public static ProfileSettingsFragment newInstance(String user_id) {
        ProfileSettingsFragment fragment = new ProfileSettingsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(USER_ID_EXTRA, user_id);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_profile_settings, container, false);
        ButterKnife.bind(this, root);
        mUserId = getArguments().getString(USER_ID_EXTRA, "");
        authToken = SharedPrefManager.getInstance(getActivity()).getStoredAuthToken();
        showProgressDialog();
        RESTClient.getInstance().getMe(authToken, getMeCallback);
        addToolbarBackArrow(toolbar);
        facebookManager = FacebookManager.getInstance(getContext());
        mMainActivity = (MainActivity) getActivity();

        initPhotoPicker(savedInstanceState);
        setOnPhotoClickListeners();

        setOnButtonClickListeners();
        initFonts();

        return root;
    }


    private void initFonts() {
        Typeface typeface_extrabold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_EXTRA_BOLD);
        toolbarTitle.setTypeface(typeface_extrabold);
        toolbarOptionSave.setTypeface(typeface_extrabold);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbarTitle.setLetterSpacing(0.06f);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        focusAnchor.requestFocus();
        initKeyboardListener(root);
    }


    @Override
    public void onPause() {
        super.onPause();
        removeGlobalLayoutListener();
    }


    private void setOnButtonClickListeners() {
        toolbarOptionSave.setOnClickListener(this);
        changePasswordButton.setOnClickListener(this);
        termsAndConditionsButton.setOnClickListener(this);
        logOutButton.setOnClickListener(this);
        deleteProfileButton.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickUnlock()) {
                    if (isDataChanged()) {
                        createConfirmSaveDataDialog();
                    } else {
                        KeyboardUtil.hideKeyboard(getActivity());
                        getActivity().onBackPressed();
                    }
                }
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (isClickUnlock()) {
            switch (view.getId()) {
                case R.id.log_out_button:
                    RESTClient.getInstance().requestLogout(authToken, logoutCallback);
                    facebookManager.logout();
                    ((MainActivity) getActivity()).clearDataInLogout();
                    SharedPrefManager.getInstance(getActivity()).clearStoredData();
                    mMainActivity.openProfileTab();
                    KeyboardUtil.hideKeyboard(getActivity());
                    break;
                case R.id.toolbar_option:
                    if (isDataChanged() && ((MainActivity) getActivity()).isNetworkAvailable()) {
                        showProgressDialog();
                        RESTClient.getInstance().postUpdateMe(authToken,
                                nickNameEditText.getText().toString().trim(),
                                userPhoto,
                                userCoverImage,
                                updateMeCallback);
                        KeyboardUtil.hideKeyboard(getActivity());
                    }
                    break;
                case R.id.change_password_button:
                    if (((MainActivity) getActivity()).isNetworkAvailable()) {
                        KeyboardUtil.hideKeyboard(getActivity());
                        ((MainActivity) getActivity()).setFragmentWithBackStack(
                                ChangePasswordFragment.newInstance(),
                                ChangePasswordFragment.class.getSimpleName());
//                    ((MainActivity) getActivity()).setFragmentWithAnimation(
//                            ChangePasswordFragment.newInstance(mUserId),
//                            ChangePasswordFragment.class.getSimpleName(),
//                            true,
//                            R.anim.enter_from_right, R.anim.exit_to_left,
//                            R.anim.enter_from_left, R.anim.exit_to_right);
                    }
                    break;
                case R.id.terms_and_conditions_button:
                    if (((MainActivity) getActivity()).isNetworkAvailable()) {
                        KeyboardUtil.hideKeyboard(getActivity());
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.terms_of_service_link)));
                        startActivity(browserIntent);
                    }
                    break;
                case R.id.delete_profile_button:
                    if (((MainActivity) getActivity()).isNetworkAvailable()) {
                        KeyboardUtil.hideKeyboard(getActivity());
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", getString(R.string.electric_soul_support_email), null));
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.delete_account_subject));
                        String body = getString(R.string.delete_account_content_msg1) + " " + mLastUserName + "\"" +
                                getString(R.string.delete_account_content_msg2);
                        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
                        startActivity(Intent.createChooser(emailIntent, getString(R.string.delete_account_subject)));
                    }
                    break;
            }
        }
    }


    private void createConfirmSaveDataDialog() {
        DialogManager.showConfirmDialog(getContext(), R.string.confirm_edit_profile_title
                , R.string.confirm_edit_profile_message
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showProgressDialog();
                        RESTClient.getInstance().postUpdateMe(authToken,
                                nickNameEditText.getText().toString().trim(),
                                userPhoto,
                                userCoverImage,
                                updateMeCallback);
                        KeyboardUtil.hideKeyboard(getActivity());
                    }
                }
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        KeyboardUtil.hideKeyboard(getActivity());
                        getActivity().onBackPressed();
                    }
                });
    }


    private boolean isDataChanged() {
        return !(mLastUserName != null && mLastUserName.trim().contentEquals(nickNameEditText.getText().toString().trim()) &&
                userPhoto == null && userCoverImage == null);
    }


    private void initPhotoPicker(Bundle savedInstanceState) {
        mGetPhotoUtil = new GetPhotoUtil(this, getActivity(), new GetPhotoUtil.PhotoUtilListener() {

            @Override
            public void onImageDecoding() {
            }


            @Override
            public void onPicObtained(GetPhotoUtil.LoadedPhoto loadedPhoto) {
                LogUtil.logE("loadedPhoto = " + loadedPhoto);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/png"), loadedPhoto.file);

                switch (mRequestedPhoto) {
                    case REQUEST_PHOTO_PROFILE:
                        profilePhotoImage.setImageBitmap(loadedPhoto.bitmap);
                        userPhoto = MultipartBody.Part.createFormData("image", loadedPhoto.file.getName(), requestFile);
                        break;
                    case REQUEST_PHOTO_BACKGROUND_PROFILE:
                        userBackgroundImage.setImageBitmap(loadedPhoto.bitmap);
                        userCoverImage = MultipartBody.Part.createFormData("cover_image", loadedPhoto.file.getName(), requestFile);
                        break;
                }

                mRequestedPhoto = -1;
            }
        });

        if (savedInstanceState != null) {
            mGetPhotoUtil.loadInstance(savedInstanceState);
        }
    }


    private void setOnPhotoClickListeners() {
        changeBackgroundImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRequestedPhoto = REQUEST_PHOTO_BACKGROUND_PROFILE;
                mGetPhotoUtil.takeFromGallery();
            }
        });

        changeProfilePhotoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRequestedPhoto = REQUEST_PHOTO_PROFILE;
                mGetPhotoUtil.takeFromGallery();
            }
        });
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mGetPhotoUtil != null) {
            mGetPhotoUtil.saveInstance(outState);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mGetPhotoUtil.resolveResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GetPhotoUtil.REQ_GALLERY:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mGetPhotoUtil.sendGallery();
                } else {
                    LogUtil.logE("Permission Denied");
                }
                break;
        }
    }


    private void storeUserData(UserModel userModel) {
        SharedPrefManager.getInstance(getActivity()).storeUserName(userModel.getUserName());
    }


    private Callback<JSONObject> logoutCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
            cancelProgressDialog();
        }


        @Override
        public void onFailure(Call<JSONObject> call, Throwable t) {
            cancelProgressDialog();
            LogUtil.logE("Failed try to logout");
        }
    };

    private Callback<JSONObject> deactivateUserCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {

        }


        @Override
        public void onFailure(Call<JSONObject> call, Throwable t) {

        }
    };


    private Callback<JsonObject> updateMeCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            cancelProgressDialog();
            if (response.body() != null) {
                if (response.code() == ConstantsAPI.CODE_200_SUCCESS) {
                    String data = response.body().toString();
                    UserModel user = new Gson().fromJson(data, UserModel.class);
                    if (user != null) {
                        storeUserData(user);
                    }
                    getActivity().onBackPressed();
                }
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_400_BAD_REQUEST:
                        String error = getString(R.string.error_change_name);
                        try {
                            //we use this hack because server don't give us a normal error message
                            String[] errorArray = response.errorBody().string().split("\"");
                            error = errorArray[ERROR_TEXT_POSITION];
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                        KeyboardUtil.initFocusAndShowKeyboard(nickNameEditText, getActivity());
                        break;
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            cancelProgressDialog();
        }
    };

    private Callback<UserModel> getMeCallback = new Callback<UserModel>() {
        @Override
        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
            if (response.body() != null) {
                UserModel currentUser = response.body();
                if (currentUser != null && getActivity() != null) {
                    if (currentUser.hasImageThumbnail()) {
                        Picasso.with(getActivity()).load(currentUser.getImage().getThumbnail())
                                .placeholder(R.drawable.avatar).fit().centerCrop().into(profilePhotoImage);
                    }
                    if (currentUser.hasCoverImageThumbnail()) {
                        Picasso.with(getActivity()).load(currentUser.getCoverImage().getFullSize())
                                .placeholder(R.drawable.ic_event_empty).fit().centerCrop().into(userBackgroundImage);
                    }
                    mLastUserName = currentUser.getUserName();
                    nickNameEditText.setText(mLastUserName);
                }
                cancelProgressDialog();
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<UserModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };
}

