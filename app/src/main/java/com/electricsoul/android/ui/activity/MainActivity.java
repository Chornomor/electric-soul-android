package com.electricsoul.android.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.constants.ConstantsNotification;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.EventModel;
import com.electricsoul.android.model.EventResultModel;
import com.electricsoul.android.model.FollowerResult;
import com.electricsoul.android.model.LocationsResultModel;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.model.elasticModel.UserModelElastic;
import com.electricsoul.android.network.NetworkManager;
import com.electricsoul.android.network.NetworkReceiver;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.base.BaseActivity;
import com.electricsoul.android.ui.fragment.AuthFragment;
import com.electricsoul.android.ui.fragment.EventCommentsFragment;
import com.electricsoul.android.ui.fragment.EventsFragment;
import com.electricsoul.android.ui.fragment.ProfileFragment;
import com.electricsoul.android.ui.fragment.SearchFragment;
import com.electricsoul.android.ui.fragment.TicketCheckoutFragment;
import com.electricsoul.android.utils.AnimateView;
import com.electricsoul.android.utils.KeyboardUtil;
import com.google.android.gms.wallet.FullWallet;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.WalletConstants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.electricsoul.android.constants.ConstantsAndroidPay.REQUEST_CODE_MASKED_WALLET;
import static com.electricsoul.android.constants.ConstantsAndroidPay.REQUEST_CODE_RESOLVE_LOAD_FULL_WALLET;
import static com.electricsoul.android.ui.base.BaseFragment.isClickUnlock;


public class MainActivity extends BaseActivity implements NetworkReceiver.NetworkStateReceiverListener {

    private static boolean activityVisible = false;

    public static final String LAST_TAB_OF_SEARCH_SCREEN = "com.electricsoul.android.ui.activity.LAST_TAB_OF_SEARCH_SCREEN";

    public static final String LAST_SEARCH_QUERY = "com.electricsoul.android.ui.activity.LAST_SEARCH_QUERY";

    private int mLastTabOfSearchScreen;
    private int mLastTabOfMainActivity;

    private String mLastSearchQuery;

    public String currentUserId = "";
    public String currentUserToken = "";

    public static final int SEARCH_TAB = 0;

    public static final int EVENTS_TAB = 1;

    public static final int PROFILE_TAB = 2;

    private static final int DEFAULT_TAB_POSITION_IN_EVENT_FRAGMENT = SEARCH_TAB;

    @BindView(R.id.error_view)
    LinearLayout errorView;

    @BindView(R.id.tv_error_title)
    TextView errorTitle;

    @BindView(R.id.tv_error_message)
    TextView errorMessage;

    @BindView(R.id.search_wrapper)
    View searchWrapper;

    @BindView(R.id.events_wrapper)
    View eventsWrapper;

    @BindView(R.id.profile_wrapper)
    View profileWrapper;

    @BindView(R.id.search_choose_view)
    View searchChooseView;

    @BindView(R.id.events_choose_view)
    View eventsChooseView;

    @BindView(R.id.profile_choose_view)
    View profileChooseView;

    @BindView(R.id.bottom_toolbar)
    RelativeLayout bottomToolbar;

    @BindView(R.id.space_for_bottom_toolbar)
    View spaceForBotToolbar;

    private NetworkReceiver networkStateReceiver;

    /*event tab data*/
    public LocationsResultModel locationsResult;
    public EventResultModel currentEventResultModel;
    public EventResultModel myFollowsEvent;
    public EventResultModel festivals;
    public ArrayList<String> rsvpListIds = new ArrayList<>();
    public int selectedTabInEventFragment = DEFAULT_TAB_POSITION_IN_EVENT_FRAGMENT;
    public boolean needShowCityList = true;

    /*followers data*/
    public FollowerResult followers;
    public FollowerResult followings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initTabs();
        currentUserId = SharedPrefManager.getInstance(this).getStoredUserId();
        currentUserToken = SharedPrefManager.getInstance(this).getStoredAuthToken();
        requestDataForFollows();
        enableTab(EVENTS_TAB);
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerNetworkListener();
        handleNotification();
        activityVisible = true;
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterNetworkListener();
    }


    public static boolean isActivityVisible() {
        return activityVisible;
    }


    private void handleNotification() {
        if (getIntent() != null) {
            Intent intent = getIntent();
            String eventId = intent.getStringExtra(ConstantsNotification.NOTIFICATION_EVENT_ID);
            if (eventId != null) {
                setIntent(new Intent());
                setFragmentWithBackStack(EventCommentsFragment.getInstance(eventId, null), null);
            }
        }
    }


    public void setFragmentWithBackStack(Fragment fragment, String tag) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction =
                    fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content, fragment, tag).addToBackStack(null);
            fragmentTransaction.commit();
            checkConnection();
        } catch (IllegalStateException e) {
        }
    }


    public void setFragmentWithoutBackStack(Fragment fragment, String tag) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentTransaction fragmentTransaction =
                    fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content, fragment, tag);
            fragmentTransaction.commit();
            checkConnection();
        } catch (IllegalStateException e) {
        }
    }


    public void setFragmentWithAnimation(Fragment fragment, String tag, boolean withBackStack, int enterAnim, int exitAnim, int popEnter, int popExit) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if ((enterAnim != 0 && exitAnim != 0) && (popEnter != 0 && popExit != 0)) {
                fragmentTransaction.setCustomAnimations(enterAnim, exitAnim, popEnter, popExit);
            } else if (enterAnim != 0 && exitAnim != 0) {
                fragmentTransaction.setCustomAnimations(enterAnim, exitAnim);
            }
            fragmentTransaction.replace(R.id.content, fragment, tag);
            if (withBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
            checkConnection();
        } catch (IllegalStateException e) {
        }
    }


    private void initTabs() {
        searchWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableTab(SEARCH_TAB);
            }
        });

        eventsWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableTab(EVENTS_TAB);
            }
        });

        profileWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableTab(PROFILE_TAB);
            }
        });
    }


    public void enableTab(int tab) {
        if (!(tab == mLastTabOfMainActivity)) {
            mLastTabOfMainActivity = tab;
            switch (tab) {
                case SEARCH_TAB:
                    updateTabsVisibility(View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
                    setFragmentWithoutBackStack(SearchFragment.newInstance(), null);
                    break;
                case EVENTS_TAB:
                    updateTabsVisibility(View.INVISIBLE, View.VISIBLE, View.INVISIBLE);
                    setFragmentWithoutBackStack(EventsFragment.newInstance(), null);
                    break;
                case PROFILE_TAB:
                    updateTabsVisibility(View.INVISIBLE, View.INVISIBLE, View.VISIBLE);
                    openProfileTab();
                    break;
            }
        }
    }


    public void openProfileTab() {
        if (isUserAuthorized()) {
            setFragmentWithoutBackStack(ProfileFragment.newInstance(SharedPrefManager.getInstance(this).getStoredUserId(), false), null);
        } else {
            setFragmentWithoutBackStack(AuthFragment.newInstance(), AuthFragment.AUTH_TAG);
        }
    }


    public void addToFollowings(UserModel user) {
        followings.addFollowings(user);
    }


    public void addToFollowings(UserModelElastic user) {
        followings.addFollowings(user);
    }


    public void addFollowingsList(List<FollowerResult.Follower> followerList) {
        followings.cleanFollowList();
        followings.addFollowingsList(followerList);
    }


    public void addFollowersList(List<FollowerResult.Follower> followerList) {
        followers.cleanFollowList();
        followers.addFollowingsList(followerList);
    }


    public void removeFromFollowings(UserModel user) {
        followings.removeFollowings(user);
    }


    public void removeFromFollowings(UserModelElastic user) {
        followings.removeFollowings(user);
    }


    private void updateTabsVisibility(int searchTab, int eventTab, int profileTab) {
        searchChooseView.setVisibility(searchTab);
        eventsChooseView.setVisibility(eventTab);
        profileChooseView.setVisibility(profileTab);
    }


    public void removeFromMyFollowsEvent(EventModel event) {
        if (myFollowsEvent.getEventModelList() == null) {
            return;
        }
        for (int i = 0; i < myFollowsEvent.getEventModelList().size(); i++) {
            EventModel eventModel = myFollowsEvent.getEventModelList().get(i);
            if (eventModel.getId().equals(event.getId())) {
                myFollowsEvent.getEventModelList().remove(eventModel);
            }
        }
    }


    public void addToMyFollowsEvent(EventModel event) {
        if (!rsvpListIds.contains(event.getId())) {
            rsvpListIds.add(event.getId());
            myFollowsEvent.getEventModelList().add(event);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isClickUnlock()) {
            getFragmentManager().popBackStack();
        }
    }


    public void hideBottomToolbar() {
        if (isShownBottomToolbar()) {
            bottomToolbar.setVisibility(View.GONE);
            spaceForBotToolbar.setVisibility(View.GONE);
        }
    }


    public void showBottomToolbar() {
        if (!isShownBottomToolbar()) {
            bottomToolbar.setVisibility(View.VISIBLE);
            spaceForBotToolbar.setVisibility(View.VISIBLE);
        }
    }


    private boolean isShownBottomToolbar() {
        return bottomToolbar.getVisibility() == View.VISIBLE;
    }


    public int getLastTabOfSearchScreen() {
        return mLastTabOfSearchScreen;
    }


    public void setLastTabOfSearchScreen(int mLastTabOfSearchScreen) {
        this.mLastTabOfSearchScreen = mLastTabOfSearchScreen;
    }


    public void setmLastSearchQuery(String mLastSearchQuery) {
        this.mLastSearchQuery = mLastSearchQuery;
    }


    public void registerNetworkListener() {
        if (networkStateReceiver == null) {
            networkStateReceiver = new NetworkReceiver(this);
            registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }


    public void unregisterNetworkListener() {
        if (networkStateReceiver != null) {
            unregisterReceiver(networkStateReceiver);
            networkStateReceiver = null;
        }
    }


    public boolean isNetworkAvailable() {
        if (!NetworkManager.isNetworkAvailable(this)) {
            showErrorView(getString(R.string.error_connection_title), getString(R.string.error_connection_message));
            return false;
        } else {
            return true;
        }
    }


    public void checkConnection() {
        if (!NetworkManager.isNetworkAvailable(this)) {
            errorTitle.setText(getString(R.string.error_connection_title));
            errorMessage.setText(getString(R.string.error_connection_message));
            AnimateView animation = new AnimateView(this, errorView);
            animation.showNoConnection();
        }
    }


    public void showErrorView(String title, String message) {
        errorTitle.setText(title);
        errorMessage.setText(message);
        AnimateView animation = new AnimateView(this, errorView);
        animation.showNoConnection();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(LAST_TAB_OF_SEARCH_SCREEN, mLastTabOfSearchScreen);
        outState.putString(LAST_SEARCH_QUERY, mLastSearchQuery);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mLastTabOfSearchScreen = savedInstanceState.getInt(LAST_TAB_OF_SEARCH_SCREEN);
        mLastSearchQuery = savedInstanceState.getString(LAST_SEARCH_QUERY);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (getSupportFragmentManager().findFragmentByTag(AuthFragment.AUTH_TAG) != null) {
            getSupportFragmentManager().findFragmentByTag(AuthFragment.AUTH_TAG).onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode) {
            case REQUEST_CODE_MASKED_WALLET:
                if (getSupportFragmentManager().findFragmentByTag(TicketCheckoutFragment.TAG) != null) {
                    getSupportFragmentManager().findFragmentByTag(TicketCheckoutFragment.TAG).onActivityResult(requestCode, resultCode, data);
                }
                break;
            case REQUEST_CODE_RESOLVE_LOAD_FULL_WALLET:
                if (getSupportFragmentManager().findFragmentByTag(TicketCheckoutFragment.TAG) != null) {
                    getSupportFragmentManager().findFragmentByTag(TicketCheckoutFragment.TAG).onActivityResult(requestCode, resultCode, data);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                KeyboardUtil.hideKeyboard(this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public boolean isUserAuthorized() {
        return !currentUserId.isEmpty();
    }


    public void clearDataInLogout() {
        myFollowsEvent.getEventModelList().clear();
        rsvpListIds.clear();
        currentUserToken = "";
        currentUserId = "";
        selectedTabInEventFragment = 0;
    }


    private void requestDataForFollows() {
        if (isUserAuthorized()) {
            RESTClient.getInstance().getFollowerUsersByUserId(currentUserId, 1, getFollowersCallback);
            RESTClient.getInstance().getFollowingUsersByUserId(currentUserId, 1, getFollowingsCallback);
        }
    }


    @Override
    public void networkAvailable() {

    }


    @Override
    public void networkUnavailable() {
        AnimateView animation = new AnimateView(this, errorView);
        animation.showNoConnection();
    }


    private Callback<FollowerResult> getFollowersCallback = new Callback<FollowerResult>() {
        @Override
        public void onResponse(Call<FollowerResult> call, Response<FollowerResult> response) {
            if (response.body() != null) {
                followers = response.body();
            } else {
                followers = new FollowerResult();
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        break;
                    default:
                        showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                }
            }
        }


        @Override
        public void onFailure(Call<FollowerResult> call, Throwable t) {

        }
    };


    private Callback<FollowerResult> getFollowingsCallback = new Callback<FollowerResult>() {
        @Override
        public void onResponse(Call<FollowerResult> call, Response<FollowerResult> response) {
            if (response.body() != null) {
                followings = response.body();
            } else {
                followings = new FollowerResult();
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        break;
                    default:
                        showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                }
            }
        }


        @Override
        public void onFailure(Call<FollowerResult> call, Throwable t) {

        }
    };
}
