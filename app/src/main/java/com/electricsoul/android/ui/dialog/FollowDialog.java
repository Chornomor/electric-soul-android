package com.electricsoul.android.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.utils.FontUtil;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuliasokolova on 18.11.16.
 */

public class FollowDialog {

    @BindView(R.id.dialog_user_image)
    CircleImageView ivUserPhoto;

    @BindView(R.id.dialog_user_name)
    TextView tvUserName;

    @BindView(R.id.dialog_cancel)
    TextView tvCancel;

    @BindView(R.id.dialog_follow)
    TextView tvFollow;

    @BindView(R.id.title_dialog_follow)
    TextView title;

    @BindView(R.id.dialog_image_follow)
    ImageView ivFollow;


    public void showDialog(Context context, UserModel userModel, boolean startFollowing, final View.OnClickListener onFollowClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog dialog = builder.create();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.show();

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_follow, null);
        ButterKnife.bind(this, view);
        dialog.setContentView(view);
        setupTypeface(context);

        if(startFollowing) {
            ivFollow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_plus_btn));
            title.setText(R.string.dialog_follow);
            tvFollow.setText(R.string.dialog_follow);
        } else {
            ivFollow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_follow_minus_red_icon));
            title.setText(R.string.dialog_unfollow);
            tvFollow.setText(R.string.dialog_unfollow);
        }

        if (userModel.hasImageThumbnail()) {
            Picasso.with(context).load(userModel.getImage().getThumbnail()).fit().centerCrop().into(ivUserPhoto);
        }

        tvUserName.setText(userModel.getUserName());
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvFollow.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onFollowClick.onClick(view);
                        dialog.dismiss();
                    }
                });
    }


    private void setupTypeface(Context context) {
        Typeface extrabold = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_RALEWAY_EXTRA_BOLD);
        Typeface medium = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        title.setTypeface(extrabold);
        tvFollow.setTypeface(medium);
        tvCancel.setTypeface(medium);
        tvUserName.setTypeface(medium);
    }
}
