package com.electricsoul.android.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.Space;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.BuildConfig;
import com.electricsoul.android.R;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsAndroidPay;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.manager.StripeManager;
import com.electricsoul.android.model.DetailEventModel;
import com.electricsoul.android.model.FullWalletModel;
import com.electricsoul.android.model.ItemInfo;
import com.electricsoul.android.model.TestTicketingRequest;
import com.electricsoul.android.model.TicketingRequest;
import com.electricsoul.android.model.TicketsModel;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.dialog.DialogManager;
import com.electricsoul.android.utils.DateUtil;
import com.electricsoul.android.utils.DigitUtil;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.LogUtil;
import com.electricsoul.android.utils.WalletUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wallet.FullWallet;
import com.google.android.gms.wallet.FullWalletRequest;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.PaymentMethodToken;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.gms.wallet.fragment.SupportWalletFragment;
import com.google.android.gms.wallet.fragment.WalletFragmentInitParams;
import com.google.android.gms.wallet.fragment.WalletFragmentMode;
import com.google.android.gms.wallet.fragment.WalletFragmentOptions;
import com.google.android.gms.wallet.fragment.WalletFragmentStyle;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.electricsoul.android.constants.ConstantsAndroidPay.REQUEST_CODE_MASKED_WALLET;
import static com.electricsoul.android.constants.ConstantsAndroidPay.REQUEST_CODE_RESOLVE_LOAD_FULL_WALLET;
import static com.electricsoul.android.model.TestTicketingRequest.TEST_TYPE;

/**
 * Created by eugenetroyanskii on 27.12.16.
 */

public class TicketCheckoutFragment extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener {

    private static final int PRICE_POSITION = 1;

    public static final String TAG = "TicketCheckoutFragment";

    @BindView(R.id.checkout_ticket_back_event_image)
    ImageView backgroundImage;

    @BindView(R.id.checkout_ticket_event_name)
    TextView eventName;

    @BindView(R.id.checkout_ticket_event_place)
    TextView eventPlace;

    @BindView(R.id.checkout_promoter_name)
    TextView promoterName;

    @BindView(R.id.checkout_ticket_event_date)
    TextView eventDate;

    @BindView(R.id.checkout_ticket_ticket_type)
    TextView ticketType;

    @BindView(R.id.checkout_ticket_price)
    TextView price;

    @BindView(R.id.checkout_ticket_qty)
    TextView qty;

    @BindView(R.id.checkout_ticket_amount)
    TextView amount;

    @BindView(R.id.ticket_checkout_early)
    TextView tvEarlyAmount;

    @BindView(R.id.ticket_checkout_standard)
    TextView tvStandardAmount;

    @BindView(R.id.ticket_checkout_vip)
    TextView tvVipAmount;

    @BindView(R.id.early_total_price)
    TextView earlyTotalPrice;

    @BindView(R.id.standard_total_price)
    TextView standardTotalPrice;

    @BindView(R.id.vip_total_price)
    TextView vipTotalPrice;

    @BindView(R.id.early_price)
    TextView earlyPrice;

    @BindView(R.id.standard_price)
    TextView standardPrice;

    @BindView(R.id.vip_price)
    TextView vipPrice;

    @BindView(R.id.ticket_layout1)
    LinearLayout ticketLayout1;

    @BindView(R.id.ticket_layout2)
    LinearLayout ticketLayout2;

    @BindView(R.id.ticket_layout3)
    LinearLayout ticketLayout3;

    @BindView(R.id.ticket_name1)
    TextView ticketName1;

    @BindView(R.id.ticket_name2)
    TextView ticketName2;

    @BindView(R.id.ticket_name3)
    TextView ticketName3;

    @BindView(R.id.total_tickets_price)
    TextView totalTicketsPrice;

    @BindView(R.id.checkout_ticket_currency)
    TextView totalTicketsCurrency;

    @BindView(R.id.tickets_check_email)
    TextView ticketSentEmail;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.android_pay_btn)
    FrameLayout androidPayBtn;

    @BindView(R.id.stripe_pay_btn)
    LinearLayout stripePayBtn;

    @BindView(R.id.card_input_widget)
    CardInputWidget stripeCardInput;

    @BindView(R.id.input_widget_space)
    Space stripeCardInputSpace;

    private DetailEventModel currentEventModel;
    private ArrayList<TicketsModel> tickets = new ArrayList<>();

    private String earlyTicketAmount, standardTicketAmount, vipTicketAmount, authToken, email;

    private GoogleApiClient mGoogleApiClient;
    private ItemInfo mItemInfo;
    private MaskedWallet mMaskedWallet;

    private StripeManager stripeManager;

    private SupportWalletFragment mWalletFragment;


    public static TicketCheckoutFragment newInstance(DetailEventModel eventModel, ArrayList<TicketsModel> tickets
            , String earlyBirdAmount, String standardAmount, String vipAmount, String email) {
        TicketCheckoutFragment instance = new TicketCheckoutFragment();
        instance.currentEventModel = eventModel;
        instance.tickets = tickets;
        instance.earlyTicketAmount = earlyBirdAmount;
        instance.standardTicketAmount = standardAmount;
        instance.vipTicketAmount = vipAmount;
        instance.email = email;
        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ticket_checkout, container, false);
        ButterKnife.bind(this, root);
        addToolbarBackArrow(toolbar);
        initFonts(root);
        fillTicketingUI();
        setupViews();

        stripeManager = new StripeManager((BaseActivity) getActivity(), stripeCardInput);
        authToken = SharedPrefManager.getInstance(getContext()).getStoredAuthToken();
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        initGoogleApiClient();
        isReadyToPay();
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && getActivity() != null) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }


    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Wallet.API, new Wallet.WalletOptions.Builder()
                        .setEnvironment(ConstantsAndroidPay.WALLET_ENVIRONMENT)
                        .build())
                .enableAutoManage(getActivity(), this)
                .build();
    }


    private void setupViews() {
        if (getContext() != null) {
            Picasso.with(getContext()).load(currentEventModel.getCoverImage().getFullSize())
                    .fit().centerCrop().into(backgroundImage);
        }
        promoterName.setText(currentEventModel.getPromoter().getName());
        tvEarlyAmount.setText(earlyTicketAmount);
        tvStandardAmount.setText(standardTicketAmount);
        tvVipAmount.setText(vipTicketAmount);
        String[] ePrice = (earlyPrice.getText().toString()).split(" ");
        String[] sPrice = (standardPrice.getText().toString()).split(" ");
        String[] vPrice = (vipPrice.getText().toString()).split(" ");
        float eTotalPrice = ePrice.length > 1 ? Integer.parseInt(earlyTicketAmount) * Float.parseFloat(ePrice[PRICE_POSITION]) : 0;
        float sTotalPrice = sPrice.length > 1 ? Integer.parseInt(standardTicketAmount) * Float.parseFloat(sPrice[PRICE_POSITION]) : 0;
        float vTotalPrice = vPrice.length > 1 ? Integer.parseInt(vipTicketAmount) * Float.parseFloat(vPrice[PRICE_POSITION]) : 0;
        earlyTotalPrice.setText(DigitUtil.floatFormat(eTotalPrice));
        standardTotalPrice.setText(DigitUtil.floatFormat(sTotalPrice));
        vipTotalPrice.setText(DigitUtil.floatFormat(vTotalPrice));
        totalTicketsCurrency.setText(tickets.get(0).getCurrencyCode());
        totalTicketsPrice.setText(DigitUtil.floatFormat(eTotalPrice + sTotalPrice + vTotalPrice));
        eventName.setText(currentEventModel.getName());
        eventPlace.setText(currentEventModel.getAddress().getAddress1());
        eventDate.setText(DateUtil.convertEventCompletelyFullDate(currentEventModel.getStart()) + " " + DateUtil.convertTime(currentEventModel.getStart())
                + " - " + DateUtil.convertEventCompletelyFullDate(currentEventModel.getEnd()) + " " + DateUtil.convertTime(currentEventModel.getEnd()));
        ticketSentEmail.setText(email);
    }


    private void fillTicketingUI() {
        for (int i = 0; i < tickets.size(); i++) {
            TicketsModel ticket = tickets.get(i);
            if (ticket != null) {
                switch (i) {
                    case 0:
                        ticketLayout1.setVisibility(View.VISIBLE);
                        ticketName1.setText(ticket.getTicketCategory());
                        earlyPrice.setText(ticket.getCurrencyCode() + " " + DigitUtil.floatFormat(ticket.getPrice()));
                        break;
                    case 1:
                        ticketLayout2.setVisibility(View.VISIBLE);
                        ticketName2.setText(ticket.getTicketCategory());
                        standardPrice.setText(ticket.getCurrencyCode() + " " + DigitUtil.floatFormat(ticket.getPrice()));
                        break;
                    case 2:
                        ticketLayout3.setVisibility(View.VISIBLE);
                        ticketName3.setText(ticket.getTicketCategory());
                        vipPrice.setText(ticket.getCurrencyCode() + " " + DigitUtil.floatFormat(ticket.getPrice()));
                        break;
                }
            }
        }
    }


    private void initFonts(View rootView) {
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        Typeface semibold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_SEMI_BOLD);
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_KHULA_BOLD);

        FontUtil.impl((ViewGroup) rootView, medium);
        ticketType.setTypeface(semibold);
        price.setTypeface(semibold);
        qty.setTypeface(semibold);
        amount.setTypeface(semibold);
        eventName.setTypeface(bold);
        ticketSentEmail.setTypeface(bold);
    }


    @OnClick(R.id.checkout_ticket_edit_order)
    public void onEditOrderClick() {
        (getActivity()).onBackPressed();
    }


    private void isReadyToPay() {
        Wallet.Payments.isReadyToPay(mGoogleApiClient).setResultCallback(
                new ResultCallback<BooleanResult>() {
                    @Override
                    public void onResult(@NonNull BooleanResult booleanResult) {
                        cancelProgressDialog();
                        if (booleanResult.getStatus().isSuccess()) {
                            if (booleanResult.getValue()) {
                                androidPayBtn.setVisibility(View.VISIBLE);
                                stripePayBtn.setVisibility(View.GONE);
                                stripeCardInput.setVisibility(View.GONE);
                                stripeCardInputSpace.setVisibility(View.GONE);
                                createAndAddWalletFragment();
                            } else {
                                androidPayBtn.setVisibility(View.GONE);
                                stripePayBtn.setVisibility(View.VISIBLE);
                                stripeCardInput.setVisibility(View.VISIBLE);
                                stripeCardInputSpace.setVisibility(View.VISIBLE);
                                stripeManager.attachButton(stripePayBtn, new StripeManager.StripeCallback() {
                                    @Override
                                    public void onSuccess(String token) {
                                        buyTestTicketsRequest(token);
                                    }
                                });
                            }
                        } else {
                            LogUtil.logE("isReadyToPay:" + booleanResult.getStatus());
                        }
                    }
                });
    }


    private void createAndAddWalletFragment() {
        WalletFragmentStyle walletFragmentStyle = new WalletFragmentStyle()
                .setBuyButtonText(WalletFragmentStyle.BuyButtonText.BUY_WITH)
                .setBuyButtonAppearance(WalletFragmentStyle.BuyButtonAppearance.ANDROID_PAY_DARK)
                .setBuyButtonWidth(WalletFragmentStyle.Dimension.MATCH_PARENT);

        WalletFragmentOptions walletFragmentOptions = WalletFragmentOptions.newBuilder()
                .setEnvironment(ConstantsAndroidPay.WALLET_ENVIRONMENT)
                .setFragmentStyle(walletFragmentStyle)
                .setTheme(WalletConstants.THEME_LIGHT)
                .setMode(WalletFragmentMode.BUY_BUTTON)
                .build();
        mWalletFragment = SupportWalletFragment.newInstance(walletFragmentOptions);

        String accountName = SharedPrefManager.getInstance(getContext()).getStoredLogin();

        mItemInfo = new ItemInfo(currentEventModel.getName(), Float.parseFloat(totalTicketsPrice.getText().toString()), tickets.get(0).getCurrencyCode());

        MaskedWalletRequest maskedWalletRequest = WalletUtil.createStripeMaskedWalletRequest(mItemInfo
                , getString(R.string.stripe_public_key), BuildConfig.VERSION_NAME);

        WalletFragmentInitParams.Builder startParamsBuilder = WalletFragmentInitParams.newBuilder()
                .setMaskedWalletRequest(maskedWalletRequest)
                .setMaskedWalletRequestCode(REQUEST_CODE_MASKED_WALLET)
                .setAccountName(accountName);
        mWalletFragment.initialize(startParamsBuilder.build());

        getChildFragmentManager().beginTransaction()
                .replace(R.id.android_pay_btn, mWalletFragment)
                .commit();
    }


    private void goToGooglePlay() {
        final String appPackageName = "com.google.android.apps.walletnfcrel";
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }


    private void createAndAddWalletFragmentWithCustomBtn() {
        mItemInfo = new ItemInfo(currentEventModel.getName(), Float.parseFloat(totalTicketsPrice.getText().toString()), tickets.get(0).getCurrencyCode());
        MaskedWalletRequest maskedWalletRequest = WalletUtil.createStripeMaskedWalletRequest(mItemInfo
                , getString(R.string.stripe_public_key), BuildConfig.VERSION_NAME);

        Wallet.Payments.loadMaskedWallet(mGoogleApiClient, maskedWalletRequest, REQUEST_CODE_MASKED_WALLET);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // retrieve the error code, if available
        int errorCode = -1;
        if (data != null) {
            errorCode = data.getIntExtra(WalletConstants.EXTRA_ERROR_CODE, -1);
        }
        switch (requestCode) {
            case REQUEST_CODE_MASKED_WALLET:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if (data != null) {
                            MaskedWallet maskedWallet =
                                    data.getParcelableExtra(WalletConstants.EXTRA_MASKED_WALLET);
                            launchConfirmationDialog(maskedWallet);
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                    default:
                        handleError(errorCode);
                        break;
                }
                break;
            case REQUEST_CODE_RESOLVE_LOAD_FULL_WALLET:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if (data != null && data.hasExtra(WalletConstants.EXTRA_FULL_WALLET)) {
                            FullWallet fullWallet = data.getParcelableExtra(WalletConstants.EXTRA_FULL_WALLET);
                            fetchTransactionStatus(fullWallet);
                        } else if (data != null && data.hasExtra(WalletConstants.EXTRA_MASKED_WALLET)) {
                            // re-launch the activity with new masked wallet information
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        // nothing to do here
                        break;
                    default:
                        handleError(errorCode);
                        break;
                }
                break;
            case WalletConstants.RESULT_ERROR:
                handleError(errorCode);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }


    private void launchConfirmationDialog(MaskedWallet maskedWallet) {
        mMaskedWallet = maskedWallet;
        DialogManager.showPayConfirmDialog(getContext(), positiveBtnClick);
    }


    private Dialog.OnClickListener positiveBtnClick = new Dialog.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            confirmPurchase();
        }
    };


    private void confirmPurchase() {
        showProgressDialog();
        getFullWallet();
    }


    private void getFullWallet() {
        FullWalletRequest fullWalletRequest = WalletUtil.createFullWalletRequest(mItemInfo,
                mMaskedWallet.getGoogleTransactionId());
        Wallet.Payments.loadFullWallet(mGoogleApiClient, fullWalletRequest,
                REQUEST_CODE_RESOLVE_LOAD_FULL_WALLET);
    }


    private void fetchTransactionStatus(FullWallet fullWallet) {
        cancelProgressDialog();

        PaymentMethodToken token = fullWallet.getPaymentMethodToken();
        if (token != null) {
            String mToken = token.getToken().replace('\n', ' ');
            FullWalletModel tokenModel = null;
            try {
                tokenModel = new Gson().fromJson(mToken, FullWalletModel.class);
                LogUtil.logE("PaymentMethodToken:" + tokenModel.getToken());
            } catch (Exception e) {

            }
            if (tokenModel != null) {
                buyTicketsRequest(tokenModel.getToken());
            } else {
                handleError(ConstantsAndroidPay.ERROR_CODE_INVALID_TOKEN);
            }
        }
    }


    private void showEnjoyDialog() {
        DialogManager.showSimpleDialog(getContext(), R.string.dialog_info_title, R.string.but_ticket_enjoy, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent result = new Intent();
                result.putExtra(ConstantsAndroidPay.PAY_RESULT, ConstantsAndroidPay.PAY_RESULT_SUCCESS);
                getActivity().setIntent(result);
                getActivity().onBackPressed();
            }
        });
    }


    private void buyTicketsRequest(String token) {
        RESTClient.getInstance().buyTicket(authToken, new TicketingRequest(token, email, getTickets()), buyTicketCallback);
    }


    private void buyTestTicketsRequest(String token) {
        RESTClient.getInstance().buyTestTicket(authToken, new TestTicketingRequest(token, email, TEST_TYPE, getTickets()), buyTicketCallback);
    }


    private ArrayList<TicketsModel> getTickets() {
        ArrayList<TicketsModel> ticketsModels = new ArrayList<>();

        for (int i = 0; i < tickets.size(); i++) {
            TicketsModel ticket = tickets.get(i);
            if (ticket != null) {
                switch (i) {
                    case 0:
                        if (Integer.parseInt(earlyTicketAmount) > 0) {
                            ticket.setCount(Integer.parseInt(earlyTicketAmount));
                            ticketsModels.add(ticket);
                        }
                        break;
                    case 1:
                        if (Integer.parseInt(standardTicketAmount) > 0) {
                            ticket.setCount(Integer.parseInt(standardTicketAmount));
                            ticketsModels.add(ticket);
                        }
                        break;
                    case 2:
                        if (Integer.parseInt(vipTicketAmount) > 0) {
                            ticket.setCount(Integer.parseInt(vipTicketAmount));
                            ticketsModels.add(ticket);
                        }
                        break;
                }
            }
        }
        return ticketsModels;
    }


    protected void handleError(int errorCode) {
        switch (errorCode) {
            case ConstantsAndroidPay.ERROR_CODE_INVALID_TOKEN:
                ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_invalid_token));
                break;
            case WalletConstants.ERROR_CODE_SPENDING_LIMIT_EXCEEDED:
                Toast.makeText(getContext(), getContext().getString(R.string.spending_limit_exceeded, errorCode),
                        Toast.LENGTH_LONG).show();
                break;
            case WalletConstants.ERROR_CODE_INVALID_PARAMETERS:
            case WalletConstants.ERROR_CODE_AUTHENTICATION_FAILURE:
            case WalletConstants.ERROR_CODE_BUYER_ACCOUNT_ERROR:
            case WalletConstants.ERROR_CODE_MERCHANT_ACCOUNT_ERROR:
            case WalletConstants.ERROR_CODE_SERVICE_UNAVAILABLE:
            case WalletConstants.ERROR_CODE_UNSUPPORTED_API_VERSION:
            case WalletConstants.ERROR_CODE_UNKNOWN:
            default:
                // unrecoverable error
                String errorMessage = getString(R.string.google_wallet_unavailable) + "\n" +
                        getString(R.string.error_code, errorCode);
                Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();
                break;
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        LogUtil.logE("onConnectionFailed:" + connectionResult.getErrorMessage());
        Toast.makeText(getContext(), "Google Play Services error", Toast.LENGTH_SHORT).show();
    }


    private Callback<JsonObject> buyTicketCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            switch (response.code()) {
                case ConstantsAPI.CODE_200_SUCCESS:
                    cancelProgressDialog();
                    showEnjoyDialog();
                    break;
                case ConstantsAPI.CODE_404_NO_MATCHES:
                case ConstantsAPI.CODE_400_BAD_REQUEST:
                case ConstantsAPI.CODE_401_NOT_AUTHENTICATE:
                case ConstantsAPI.CODE_403_VEREFIY_ACCOUNT:
                case ConstantsAPI.CODE_429_SERVER_UPDATING:

                    String message = "";
                    if (response.body() != null) {
                        try {
                            JSONObject result = new JSONObject(response.body().toString()).optJSONObject("error");
                            message = result.optString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (response.errorBody() != null) {
                        try {
                            JSONObject result = null;
                            try {
                                result = new JSONObject(response.errorBody().string()).optJSONObject("error");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            message = result.optString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (getActivity() != null) {
                        if (message == null || message.isEmpty()) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        } else {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), message);
                        }
                    }
                    break;
            }
        }


        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            cancelProgressDialog();
        }
    };
}
