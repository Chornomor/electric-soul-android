package com.electricsoul.android.ui.views;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.utils.ClipboardUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnLongClick;

/**
 * Created by alexchern on 24.11.16.
 */

public class EventDetailsField extends FrameLayout {

    View root;

    @BindView(R.id.tv_field_title)
    TextView fieldTitle;

    @BindView(R.id.tv_field_text)
    TextView fieldText;

    @BindView(R.id.btn_more_details)
    ImageView btnMoreDetails;

    boolean isInMoreDetailsMode = false;


    public EventDetailsField(Context context) {
        super(context);
    }


    public EventDetailsField(Context context, AttributeSet attrs) {
        super(context, attrs);
        root = LayoutInflater.from(getContext()).inflate(R.layout.view_event_details_common_field, null);
        ButterKnife.bind(this, root);
        initViews(context, attrs);
        addView(root);
    }


    private void initViews(Context context, AttributeSet attrs) {
        if (null == attrs) {
            throw new IllegalArgumentException("Attributes should be provided to this view");
        }
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.EventDetailsField);
        fieldTitle.setText(typedArray.getString(R.styleable.EventDetailsField_field_title));
        fieldText.setText(typedArray.getString(R.styleable.EventDetailsField_field_text));
        btnMoreDetails.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isInMoreDetailsMode()) {
                            setInMoreDetailsMode(!isInMoreDetailsMode());
                            btnMoreDetails.setImageResource(R.drawable.ic_plus_btn);
                        } else {
                            setInMoreDetailsMode(!isInMoreDetailsMode());
                            btnMoreDetails.setImageResource(R.drawable.ic_follow_minus_red_icon);
                        }
                        adjestFieldText();
                    }
                }
        );
    }


    @OnLongClick(R.id.tv_field_text)
    public boolean onPlaceLongClick() {
        ClipboardUtil.saveToClipboard(getContext(), fieldText.getText().toString());
        return false;
    }


    public void setFieldText(String text) {
        fieldText.setText(text);
        checkTextLines(text);
    }


    private void checkTextLines(String text) {
        int lineCount = text.split("\\r?\\n").length;
        if (lineCount > 2) {
            btnMoreDetails.setVisibility(VISIBLE);
        } else {
            btnMoreDetails.setVisibility(GONE);
        }
    }


    private void adjestFieldText() {
        fieldText.setMaxLines(isInMoreDetailsMode() ? Integer.MAX_VALUE : 2);
    }


    private boolean isInMoreDetailsMode() {
        return isInMoreDetailsMode;
    }


    public void setInMoreDetailsMode(boolean inMoreDetailsMode) {
        isInMoreDetailsMode = inMoreDetailsMode;
    }


    public void setTitleFont(Typeface font) {
        fieldTitle.setTypeface(font);
    }


    public void setTextFont(Typeface font) {
        fieldText.setTypeface(font);
    }
}
