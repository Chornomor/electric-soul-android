package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.utils.PixelUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.electricsoul.android.ui.base.BaseFragment.isClickUnlock;

/**
 * Created by alexchern on 16.11.16.
 */

public class PhotosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private ArrayList<FanImageModel> fanImages = new ArrayList<>();
    private Context context;
    private PhotosAdapter.onPhotoItemClickListenen photoItemClickListenen;
    private boolean forProfileScreen = false;
    private boolean isEmptyEventsList = false;


    public PhotosAdapter(Context context, ArrayList<FanImageModel> fanImages) {
        this.context = context;
        this.fanImages = fanImages;
    }


    public boolean isHeader(int position) {
        return position == 0;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_photos, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isEmptyEventsList) {
            bindEmptyItemViewHolder(holder, position);
        } else {
            bindItemViewHolder(holder, position);
        }
    }


    private void bindItemViewHolder(final RecyclerView.ViewHolder holder, int position) {
        FanImageModel fanImageModel = fanImages.get(position);
        ((ViewHolder) holder).itemView.setTag(position);

        if (fanImageModel.hasFullImage()) {
            Picasso.with(context).load(fanImageModel.getImage().getFullSize()).placeholder(R.drawable.ic_event_empty).fit().centerCrop()
                    .into(((ViewHolder) holder).image);
        }

        ((ViewHolder) holder).emptyItem.setVisibility(View.GONE);
        ((ViewHolder) holder).image.setVisibility(View.VISIBLE);
        ((ViewHolder) holder).image.setTag(position);
        ((ViewHolder) holder).image.setOnClickListener(this);

        if (isHeader(position)) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = new StaggeredGridLayoutManager
                    .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setFullSpan(true);
            ((ViewHolder) holder).itemView.setLayoutParams(layoutParams);
            ((ViewHolder) holder).itemView.getLayoutParams().height = (int) PixelUtil.dpToPx(300, context);
            return;
        }
        ((ViewHolder) holder).itemView.getLayoutParams().height = (int) PixelUtil.dpToPx(200, context);
    }


    private void bindEmptyItemViewHolder(final RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).emptyItem.setVisibility(View.VISIBLE);
        ((ViewHolder) holder).image.setVisibility(View.GONE);
        if (isHeader(position)) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = new StaggeredGridLayoutManager
                    .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setFullSpan(true);
            ((ViewHolder) holder).itemView.setLayoutParams(layoutParams);
        }
    }


    @Override
    public int getItemCount() {
        if (forProfileScreen) {
            if (fanImages.size() == 0) {
                isEmptyEventsList = true;
                return fanImages.size() + 1;
            } else {
                return fanImages.size();
            }
        } else {
            return fanImages.size();
        }
    }


    public void setForProfileScreen(boolean forProfileScreen) {
        this.forProfileScreen = forProfileScreen;
    }


    @Override
    public void onClick(View view) {
        int position;
        if (isClickUnlock()) {
            switch (view.getId()) {
                case R.id.image:
                    position = Integer.parseInt(String.valueOf(view.getTag()));
                    photoItemClickListenen.onItemClick(view, position, fanImages.get(position).getEventId());
                    break;
            }
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.empty_layout)
        FrameLayout emptyItem;

        @BindView(R.id.image)
        ImageView image;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void setClickListener(PhotosAdapter.onPhotoItemClickListenen listener) {
        photoItemClickListenen = listener;
    }


    public interface onPhotoItemClickListenen {
        void onItemClick(View view, int position, String eventId);
    }

}
