package com.electricsoul.android.ui.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.constants.ConstantsAndroidPay;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.DetailEventModel;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.model.FanImageResult;
import com.electricsoul.android.model.PromoterResult;
import com.electricsoul.android.model.TicketsModel;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.model.UserResult;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.EventDetailsFanImagesAdapter;
import com.electricsoul.android.ui.adapter.EventDetailsHorizontalImagesAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.dialog.DialogManager;
import com.electricsoul.android.utils.DateUtil;
import com.electricsoul.android.utils.DigitUtil;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.GetPhotoUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.LogUtil;
import com.electricsoul.android.ui.views.EventDetailsField;
import com.electricsoul.android.utils.Regex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wallet.Wallet;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventDetailsFragment extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener
        , EventDetailsHorizontalImagesAdapter.OnItemClickListener {

    private final static int DELAY_FOR_HIDE_BOTTOM_BAR = 200;
    private final static int MIN_DESCRIPTION_LINES = 4;
    private final static String DEFAULT_AMOUNT = "0";

    private View root;

    @BindView(R.id.iv_event_details_photo)
    ImageView eventImageView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_de_date)
    TextView eventDate;

    @BindView(R.id.btn_next)
    ImageView btnNext;

    @BindView(R.id.field_event_details_who)
    EventDetailsField detailsWho;

    @BindView(R.id.field_event_details_where)
    EventDetailsField detailsWhere;

    @BindView(R.id.tv_title_bold)
    TextView tvTitle;

    @BindView(R.id.tv_event_details_time)
    TextView eventTime;

    @BindView(R.id.tv_detailed_event_place)
    TextView eventPlace;

    @BindView(R.id.tv_event_details_name)
    TextView name;

    @BindView(R.id.empty_users)
    TextView emptyUsers;

    @BindView(R.id.empty_fan_image_layout)
    LinearLayout emptyFanImages;

    @BindView(R.id.rv_promoter_images)
    RecyclerView rvPromoterImages;

    @BindView(R.id.rv_fan_gallery_images)
    RecyclerView rvFanGalleryImages;

    @BindView(R.id.scroll)
    NestedScrollView scroll;

    @BindView(R.id.layout)
    LinearLayout layout;

    @BindView(R.id.tv_event_details_description)
    TextView description;

    @BindView(R.id.detailed_event_more_btn)
    Button btnMore;

    @BindView(R.id.rv_whos_going_images)
    RecyclerView rvWhosGoing;

    @BindView(R.id.root_layout)
    LinearLayout rootLayout;

    @BindView(R.id.fan_title)
    TextView tvFanTitle;

    @BindView(R.id.fan_description)
    TextView tvFanDescribtion;

    @BindView(R.id.tv_detailed_event_friends_counter)
    TextView friendsCounter;

    @BindView(R.id.tv_detailed_event_friends_going)
    TextView friendText;

    @BindView(R.id.inactive_add_filter)
    ImageView inactiveFilter;

    /**************
     * Bottom sheet views
     **************/
    @BindView(R.id.bottom_sheet)
    View bottomSheet;

    @BindView(R.id.bsheet_title)
    TextView bsTitle;

    @BindView(R.id.checkout_promoter_name)
    TextView bsPromoterName;

    @BindView(R.id.bsheet_place)
    TextView bsPlace;

    @BindView(R.id.bsheet_date)
    TextView bsDate;

    @BindView(R.id.bs_ticket_type)
    TextView bsTicketType;

    @BindView(R.id.bs_price)
    TextView bsPrice;

    @BindView(R.id.bs_qty)
    TextView bsQty;

    @BindView(R.id.ticket_layout1)
    LinearLayout ticketLayout1;

    @BindView(R.id.ticket_layout2)
    LinearLayout ticketLayout2;

    @BindView(R.id.ticket_layout3)
    LinearLayout ticketLayout3;

    @BindView(R.id.ticket_amount1)
    EditText ticketAmount1;

    @BindView(R.id.ticket_amount2)
    EditText ticketAmount2;

    @BindView(R.id.ticket_amount3)
    EditText ticketAmount3;

    @BindView(R.id.ticket_name1)
    TextView ticketName1;

    @BindView(R.id.ticket_name2)
    TextView ticketName2;

    @BindView(R.id.ticket_name3)
    TextView ticketName3;

    @BindView(R.id.ticket_price1)
    TextView ticketPrice1;

    @BindView(R.id.ticket_price2)
    TextView ticketPrice2;

    @BindView(R.id.ticket_price3)
    TextView ticketPrice3;

    @BindView(R.id.ticket_description1)
    TextView ticketDescription1;

    @BindView(R.id.ticket_description2)
    TextView ticketDescription2;

    @BindView(R.id.ticket_description3)
    TextView ticketDescription3;

    @BindView(R.id.ticket_end1)
    TextView ticketEnd1;

    @BindView(R.id.ticket_end2)
    TextView ticketEnd2;

    @BindView(R.id.ticket_end3)
    TextView ticketEnd3;

    @BindView(R.id.ticketing_email)
    EditText emailForTickets;

    @BindView(R.id.ticketing_please_input_email)
    TextView pleaseInputEmail;

    @BindView(R.id.ticket_will_be_sent)
    TextView willBeSent;

    @BindView(R.id.hide_bottom_sheet_layout)
    CoordinatorLayout viewForHideBottomSheet;

    @Nullable
    EventDetailsHorizontalImagesAdapter promotersImageAdapter;

    @Nullable
    EventDetailsHorizontalImagesAdapter usersImagesAdapter;

    @Nullable
    EventDetailsFanImagesAdapter fanImagesAdapter;

    private BottomSheetBehavior bottomSheetBehavior;
    private GetPhotoUtil mGetPhotoUtil;

    private DetailEventModel eventModel;
    private UserModel currentUser;

    private String eventId = "";
    private String authToken;
    private boolean isBottomSheetShowed = false;
    private boolean needToCleanBottomSheet = false;
    private int currentScrollY = 0;

    private GoogleApiClient mGoogleApiClient;

    private ArrayList<TicketsModel> tickets = new ArrayList<>();


    public static EventDetailsFragment newInstance(String eventId) {
        EventDetailsFragment eventDetails = new EventDetailsFragment();
        eventDetails.eventId = eventId;
        return eventDetails;
    }


    @Override
    public void onResume() {
        super.onResume();
        initGoogleApiClient();
        if (eventModel != null) {
            setToolbarTitle(eventModel.getName());
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        cancelProgressDialog();
        KeyboardUtil.hideKeyboard(getActivity());
        if (mGoogleApiClient != null && getActivity() != null) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }


    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Wallet.API, new Wallet.WalletOptions.Builder()
                        .setEnvironment(ConstantsAndroidPay.WALLET_ENVIRONMENT)
                        .build())
                .enableAutoManage(getActivity(), this)
                .build();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_event_details, container, false);
        }
        ButterKnife.bind(this, root);

        checkResultIntent();
        setupBottomSheet();
        checkUserAuthorizated();

        initFonts();

        initPhotoPicker(savedInstanceState);
        addToolbarBackArrow(toolbar);
        adjestToolbar(currentScrollY);
        showProgressDialog();
        if (((MainActivity) getActivity()).isNetworkAvailable()) {
            RESTClient.getInstance().getEventById(eventId, requestEventByIdCallback);
            RESTClient.getInstance().getMe(authToken, getMeCallback);
        }

        scroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                currentScrollY = scrollY;
                adjestToolbar(currentScrollY);
            }
        });
        setupBottomSheetOnBackPress();
        return root;
    }


    private void adjestToolbar(int scrollY) {
        if (scrollY > layout.getY()) {
            final PorterDuffColorFilter colorFilter
                    = new PorterDuffColorFilter(getResources().getColor(R.color.app_red_color), PorterDuff.Mode.MULTIPLY);

            for (int i = 0; i < toolbar.getChildCount(); i++) {
                final View v = toolbar.getChildAt(i);
                if (v instanceof ImageButton) {
                    ((ImageButton) v).getDrawable().setColorFilter(colorFilter);
                }
            }
            toolbar.setBackgroundColor(getResources().getColor(R.color.white));
            toolbar.setTitleTextColor(getResources().getColor(R.color.app_red_color));
        } else {
            final PorterDuffColorFilter colorFilter
                    = new PorterDuffColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);

            for (int i = 0; i < toolbar.getChildCount(); i++) {
                final View v = toolbar.getChildAt(i);
                if (v instanceof ImageButton) {
                    ((ImageButton) v).getDrawable().setColorFilter(colorFilter);
                }
            }
            toolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            toolbar.setTitleTextColor(getResources().getColor(android.R.color.transparent));
        }
    }


    private void initFonts() {
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        Typeface semibold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_SEMI_BOLD);
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_KHULA_BOLD);

        FontUtil.impl((ViewGroup) root, medium);
        detailsWho.setTitleFont(bold);
        detailsWhere.setTitleFont(bold);
        detailsWho.setTextFont(medium);
        detailsWhere.setTextFont(medium);
        description.setTypeface(medium);
        name.setTypeface(bold);
        eventDate.setTypeface(bold);
        eventTime.setTypeface(semibold);
        tvTitle.setTypeface(bold);
        tvFanTitle.setTypeface(bold);
        tvFanDescribtion.setTypeface(medium);
        bsTitle.setTypeface(bold);
        bsTicketType.setTypeface(semibold);
        bsPrice.setTypeface(semibold);
        bsQty.setTypeface(semibold);
        pleaseInputEmail.setTypeface(semibold);
    }


    private void checkUserAuthorizated() {
        if (((MainActivity) getActivity()).isUserAuthorized()) {
            authToken = SharedPrefManager.getInstance(getActivity()).getStoredAuthToken();
        }
    }


    private void checkResultIntent() {
        Intent result = getActivity().getIntent();
        int resultCode = result.getIntExtra(ConstantsAndroidPay.PAY_RESULT, 0);
        if (resultCode == ConstantsAndroidPay.PAY_RESULT_SUCCESS) {
            showBottomBar();
            needToCleanBottomSheet = true;
            getActivity().setIntent(new Intent());
        }
    }


    private void cleanBottomSheet() {
        ticketAmount1.setText("");
        ticketAmount2.setText("");
        ticketAmount3.setText("");
        emailForTickets.setText("");
    }


    private void setupBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setHideable(true);
        if (!isBottomSheetShowed) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        if (isClickUnlock()) {
                            showBottomBar();
                            viewForHideBottomSheet.setVisibility(View.GONE);
                        }
                        if (needToCleanBottomSheet) {
                            cleanBottomSheet();
                        }
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        isBottomSheetShowed = true;
                        break;
                    default:
                        viewForHideBottomSheet.setVisibility(View.VISIBLE);
                }
            }


            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        viewForHideBottomSheet.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                showBottomBar();
                return false;
            }
        });
    }


    private void fillBottomSheet(ArrayList<TicketsModel> result) {
        tickets = result;
        for (int i = 0; i < tickets.size(); i++) {
            TicketsModel ticket = tickets.get(i);
            if (ticket != null) {
                switch (i) {
                    case 0:
                        ticketLayout1.setVisibility(View.VISIBLE);
                        ticketAmount1.setHint(String.valueOf(ticket.getRemainingCount()));
                        ticketName1.setText(ticket.getTicketCategory());
                        ticketPrice1.setText(ticket.getCurrencyCode() + " " + DigitUtil.floatFormat(ticket.getPrice()));
                        ticketDescription1.setVisibility(ticket.getDescription().isEmpty() ? View.GONE : View.VISIBLE);
                        ticketDescription1.setText(ticket.getDescription());
                        ticketEnd1.setText("Sales end: " + DateUtil.convertDateEndTicketing(ticket.getSalesEndDate()));
                        break;
                    case 1:
                        ticketLayout2.setVisibility(View.VISIBLE);
                        ticketAmount2.setHint(String.valueOf(ticket.getRemainingCount()));
                        ticketName2.setText(ticket.getTicketCategory());
                        ticketPrice2.setText(ticket.getCurrencyCode() + " " + DigitUtil.floatFormat(ticket.getPrice()));
                        ticketDescription2.setVisibility(ticket.getDescription().isEmpty() ? View.GONE : View.VISIBLE);
                        ticketDescription2.setText(ticket.getDescription());
                        ticketEnd2.setText("Sales end: " + DateUtil.convertDateEndTicketing(ticket.getSalesEndDate()));
                        break;
                    case 2:
                        ticketLayout3.setVisibility(View.VISIBLE);
                        ticketAmount3.setHint(String.valueOf(ticket.getRemainingCount()));
                        ticketName3.setText(ticket.getTicketCategory());
                        ticketPrice3.setText(ticket.getCurrencyCode() + " " + DigitUtil.floatFormat(ticket.getPrice()));
                        ticketDescription3.setVisibility(ticket.getDescription().isEmpty() ? View.GONE : View.VISIBLE);
                        ticketDescription3.setText(ticket.getDescription());
                        ticketEnd3.setText("Sales end: " + DateUtil.convertDateEndTicketing(ticket.getSalesEndDate()));
                        break;
                }
            }
        }
    }


    private void showBottomBar() {
        isBottomSheetShowed = false;
        /**we need this hack to prevent crash when user press back while bottom sheet hiding**/
        try {
            KeyboardUtil.hideKeyboard(getActivity());
            ((MainActivity) getActivity()).showBottomToolbar();
        } catch (Exception e) {
        }
    }


    private void fillEventDetails(DetailEventModel event) {
        eventModel = event;
        trackOpenDetailsFirebaseEvent(eventModel.getName());
        /*we need to check context for android 6+ after remove permission in app settings*/
        if (getContext() != null) {
            Picasso.with(getContext()).load(eventModel.getCoverImage().getFullSize())
                    .fit().centerCrop().into(eventImageView);
        }

        setToolbarTitle(eventModel.getName());
        name.setText(eventModel.getName());
        bsPromoterName.setText(eventModel.getPromoter().getName());
        eventPlace.setText(eventModel.getAddress().getAddress1());
        eventTime.setText(DateUtil.convertTime(eventModel.getStart()) + " - " + DateUtil.convertTime(eventModel.getEnd()));
        eventDate.setText(DateUtil.convertEventFullDate(eventModel.getStart(), eventModel.getEnd()));
        description.setText(eventModel.getDescription());
        description.setText(eventModel.getDescription());
        detailsWho.setFieldText(eventModel.getArtistNames());
        detailsWhere.setFieldText(eventModel.getAddress().getAddressForDetails());
        bsTitle.setText(eventModel.getName());
        bsPlace.setText(eventModel.getAddress().getAddress1());
        bsDate.setText(DateUtil.convertEventCompletelyFullDate(eventModel.getStart()) + " " + DateUtil.convertTime(eventModel.getStart())
                + " - " + DateUtil.convertEventCompletelyFullDate(eventModel.getEnd()) + " " + DateUtil.convertTime(eventModel.getEnd()));
        friendsCounter.setText(String.valueOf(event.getAttendees_count()));
        if (getActivity() != null) {
            if (event.getAttendees_count() == 1) {
                friendText.setText(getResources().getString(R.string.events_attendees_friend));
            } else {
                friendText.setText(getResources().getString(R.string.events_attendees_friends));
            }
        }
    }


    private void initPhotoPicker(Bundle savedInstanceState) {
        mGetPhotoUtil = new GetPhotoUtil(this, getActivity(), new GetPhotoUtil.PhotoUtilListener() {

            @Override
            public void onImageDecoding() {
            }


            @Override
            public void onPicObtained(GetPhotoUtil.LoadedPhoto loadedPhoto) {
                LogUtil.logE("loadedPhoto = " + loadedPhoto);
                showProgressDialog();
                RESTClient.getInstance().uploadFanImageToEvent(authToken,
                        eventId, loadedPhoto.file, uploadFanImage);
            }
        });

        if (savedInstanceState != null) {
            mGetPhotoUtil.loadInstance(savedInstanceState);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mGetPhotoUtil != null) {
            mGetPhotoUtil.saveInstance(outState);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mGetPhotoUtil.resolveResult(requestCode, resultCode, data);
    }


    private void setupBottomSheetOnBackPress() {
        if (root != null) {
            root.setFocusableInTouchMode(true);
            root.requestFocus();
            root.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN) {
                        showBottomBar();
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        return true;
                    }
                    return false;
                }
            });
        }
    }


    @OnClick(R.id.btn_event_details_get_ticket)
    public void onGetTicketClick() {
        showProgressDialog();
        RESTClient.getInstance().getEventTicketsById(eventId, getTicketsCallback);
    }


    private void getTickets(ArrayList<TicketsModel> result) {
        if (result == null || result.isEmpty()) {
            brouserTicketing();
        } else {
            fillBottomSheet(result);
//            Wallet.Payments.isReadyToPay(mGoogleApiClient).setResultCallback(
//                    new ResultCallback<BooleanResult>() {
//                        @Override
//                        public void onResult(@NonNull BooleanResult booleanResult) {
//                            cancelProgressDialog();
//                            if (booleanResult.getStatus().isSuccess()) {
//                                if (booleanResult.getValue()) {
                                    if (getActivity() != null) {
                                        if (((MainActivity) getActivity()).isUserAuthorized()) {
                                            androidPayTicketing();
                                        } else {
                                            ((MainActivity) getActivity()).enableTab(MainActivity.PROFILE_TAB);
                                        }
                                    }
//                                } else {
//                                    DialogManager.showDisableAndroidPayDialog(getContext(), new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialogInterface, int i) {
//                                            goToGooglePlay();
//                                        }
//                                    }, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialogInterface, int i) {
//                                            brouserTicketing();
//                                        }
//                                    });
//                                }
//                            } else {
//                                LogUtil.logE("isReadyToPay:" + booleanResult.getStatus());
//                            }
//                        }
//                    });
        }
    }


    private void goToGooglePlay() {
        final String appPackageName = "com.google.android.apps.walletnfcrel";
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }


    private void brouserTicketing() {
        if (((MainActivity) getActivity()).isNetworkAvailable() && isClickUnlock()) {
            trackBuyTicketFirebaseEvent();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(eventModel.getTicketUrl()));
            startActivity(browserIntent);
        }
    }


    private void androidPayTicketing() {
        ((MainActivity) getActivity()).hideBottomToolbar();
        /*we use post delay because we have to wait until bottom bar hide*/
        trackBuyTicketFirebaseEvent();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        }, DELAY_FOR_HIDE_BOTTOM_BAR);
    }


    @OnClick(R.id.detailed_event_more_btn)
    public void onMoreClick() {
        int maxLines = description.getText().toString().length() / 30;
        description.setMaxLines(maxLines > MIN_DESCRIPTION_LINES ? maxLines : MIN_DESCRIPTION_LINES);
        btnMore.setVisibility(View.INVISIBLE);
    }


    @OnClick(R.id.btn_add_fan_photo)
    public void onAddFanPhotoClick() {
        if (((MainActivity) getActivity()).isUserAuthorized()) {
            mGetPhotoUtil.takeFromGallery();
        } else {
            ((MainActivity) getActivity()).enableTab(MainActivity.PROFILE_TAB);
        }
    }


    @OnClick(R.id.btn_next)
    public void onNextClick() {
        if (((MainActivity) getActivity()).isUserAuthorized()) {
            ((MainActivity) getActivity()).setFragmentWithBackStack(EventDetailsUsersFragment.newInstance(eventId), null);
        } else {
            ((MainActivity) getActivity()).enableTab(MainActivity.PROFILE_TAB);
        }
    }


    @OnClick(R.id.btn_order_now)
    public void onOrderNowClick() {
        if (((MainActivity) getActivity()).isNetworkAvailable()) {
            String early = ticketAmount1.getText().toString();
            String standard = ticketAmount2.getText().toString();
            String vip = ticketAmount3.getText().toString();
            early = early.isEmpty() ? DEFAULT_AMOUNT : early;
            standard = standard.isEmpty() ? DEFAULT_AMOUNT : standard;
            vip = vip.isEmpty() ? DEFAULT_AMOUNT : vip;
            if (!isValidateTicketsAmount(early, standard, vip)) {
                ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_choose_ticket));
                return;
            }
            if (!isValidateTicketsAmountOfRemainingCount()) {
                ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_tickets_not_enough));
                return;
            }
            String email = emailForTickets.getText().toString();
            if (!Regex.isValidEmail(email)) {
                if (email.isEmpty()) {
                    ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.signup_info_enter_email));
                } else {
                    ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_invalid_email));
                }
                return;
            }
            ((MainActivity) getActivity()).setFragmentWithBackStack(TicketCheckoutFragment.newInstance(eventModel, tickets, early, standard, vip, email), TicketCheckoutFragment.TAG);
        }
    }


    private boolean isValidateTicketsAmountOfRemainingCount() {
        return isValidRemainingCount(ticketAmount1) && isValidRemainingCount(ticketAmount2) && isValidRemainingCount(ticketAmount3);
    }


    private boolean isValidRemainingCount(EditText editText) {
        int count = editText.getText().toString().isEmpty() ? 0 : Integer.parseInt(editText.getText().toString());
        int remaining = editText.getText().toString().isEmpty() ? 0 : Integer.parseInt(editText.getHint().toString());
        return count <= remaining;
    }


    private boolean isValidateTicketsAmount(String early, String standard, String vip) {
        return !(early.equals(DEFAULT_AMOUNT) && standard.equals(DEFAULT_AMOUNT) && vip.equals(DEFAULT_AMOUNT));
    }


    @OnClick(R.id.detailed_event_rsvp)
    public void onRsvpClick() {
        if (isClickUnlock() && ((MainActivity) getActivity()).isNetworkAvailable()) {
            if (((MainActivity) getActivity()).isUserAuthorized()) {
                showProgressDialog();
                RESTClient.getInstance().rsvpToEvent(authToken, eventId, true, rsvpCallback);
            } else {
                ((MainActivity) getActivity()).enableTab(MainActivity.PROFILE_TAB);
            }
        }
    }


    private void fillPromoterImages(PromoterResult promoterResult) {
        if (promoterResult.getPromoters() != null) {
            promotersImageAdapter = new EventDetailsHorizontalImagesAdapter(eventId, getContext(), R.layout.item_event_photos);
            promotersImageAdapter.setPromoters(promoterResult.getPromoters());
            promotersImageAdapter.setPromoterPhotosClickListener(onPromoterPhotoClick);
            rvPromoterImages.setAdapter(promotersImageAdapter);
        }
    }


    private void fillUsersImage(UserResult userResult) {
        if (userResult.getUsers() != null && !userResult.getUsers().isEmpty()) {
            usersImagesAdapter = new EventDetailsHorizontalImagesAdapter(eventId, getContext(), R.layout.item_event_people_going_photos);
            usersImagesAdapter.setUsers(userResult);
            usersImagesAdapter.setOnUsersItemClick(this);
            rvWhosGoing.setAdapter(usersImagesAdapter);
            emptyUsers.setVisibility(View.GONE);
            btnNext.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onUserItemClick(View v, int position, UserModel user) {
        onNextClick();
    }


    private void fillFanGallery(FanImageResult funResult) {
        if (funResult.getFanImages() != null && !funResult.getFanImages().isEmpty()) {
            emptyFanImages.setVisibility(View.GONE);
            fanImagesAdapter = new EventDetailsFanImagesAdapter(getContext(), funResult.getFanImages());
            fanImagesAdapter.setItemClickListener(onFanImageClickListener);
            StaggeredGridLayoutManager gridManager = new StaggeredGridLayoutManager(2, 1);
            gridManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
            rvFanGalleryImages.setLayoutManager(gridManager);
            rvFanGalleryImages.setNestedScrollingEnabled(false);
            rvFanGalleryImages.setAdapter(fanImagesAdapter);
        } else {
            emptyFanImages.setVisibility(View.VISIBLE);
        }
    }


    private void openBottomSheetOnResume() {
        if (isBottomSheetShowed) {
            onGetTicketClick();
        }
    }


    EventDetailsFanImagesAdapter.OnItemClickListener onFanImageClickListener = new EventDetailsFanImagesAdapter.OnItemClickListener() {

        @Override
        public void onUserItemClick(View v, int position, FanImageModel fanImageModel) {
            openFanImagePagerFragment(position);
        }
    };

    Callback<DetailEventModel> requestEventByIdCallback = new Callback<DetailEventModel>() {

        @Override
        public void onResponse(Call<DetailEventModel> call, Response<DetailEventModel> response) {
            if (response.body() != null) {
                fillEventDetails(response.body());
            }
            RESTClient.getInstance().getPromoterImagesById(eventId, getPromoterImagesByIdCallback);
            RESTClient.getInstance().getUsersAttendingToEventById(eventId, 1, getUsersImagesByIdCallback);
            RESTClient.getInstance().getEventFanImageById(eventId, getUsersFanEventImageCallback);
        }


        @Override
        public void onFailure(Call<DetailEventModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    Callback<PromoterResult> getPromoterImagesByIdCallback = new Callback<PromoterResult>() {

        @Override
        public void onResponse(Call<PromoterResult> call, Response<PromoterResult> response) {
            if (response.body() != null) {
                fillPromoterImages(response.body());
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<PromoterResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    Callback<FanImageModel> uploadFanImage = new Callback<FanImageModel>() {

        @Override
        public void onResponse(Call<FanImageModel> call, Response<FanImageModel> response) {
            trackUploadPhotoFirebaseEvent();
            RESTClient.getInstance().getEventFanImageById(eventId, getUsersFanEventImageCallback);
        }


        @Override
        public void onFailure(Call<FanImageModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    Callback<UserResult> getUsersImagesByIdCallback = new Callback<UserResult>() {

        @Override
        public void onResponse(Call<UserResult> call, Response<UserResult> response) {
            if (response.body() != null) {
                fillUsersImage(response.body());
                openBottomSheetOnResume();
                cancelProgressDialog();
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<UserResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };

    Callback<FanImageResult> getUsersFanEventImageCallback = new Callback<FanImageResult>() {

        @Override
        public void onResponse(Call<FanImageResult> call, Response<FanImageResult> response) {
            if (response.body() != null) {
                fillFanGallery(response.body());
                cancelProgressDialog();
                openBottomSheetOnResume();
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<FanImageResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private Callback<Object> rsvpCallback = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            if (response.body() != null) {
                if (usersImagesAdapter == null) {
                    UserResult tempUserResult = new UserResult();
                    tempUserResult.setUsers(new ArrayList<UserModel>());
                    tempUserResult.getUsers().add(currentUser);
                    fillUsersImage(tempUserResult);
                } else {
                    usersImagesAdapter.addUser(currentUser);
                }
                ((MainActivity) getActivity()).addToMyFollowsEvent(eventModel);
                cancelProgressDialog();
            } else {
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<Object> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private Callback<UserModel> getMeCallback = new Callback<UserModel>() {
        @Override
        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
            if (response.body() != null) {
                currentUser = response.body();
            } else {
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<UserModel> call, Throwable t) {

        }
    };


    private Callback<ArrayList<TicketsModel>> getTicketsCallback = new Callback<ArrayList<TicketsModel>>() {
        @Override
        public void onResponse(Call<ArrayList<TicketsModel>> call, Response<ArrayList<TicketsModel>> response) {
            cancelProgressDialog();
            getTickets(response.body());
        }


        @Override
        public void onFailure(Call<ArrayList<TicketsModel>> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    EventDetailsHorizontalImagesAdapter.OnPromoterPhotoClick onPromoterPhotoClick = new EventDetailsHorizontalImagesAdapter.OnPromoterPhotoClick() {
        @Override
        public void onPromoterClick(int position) {
            openImagePromoterPagerFragment(position);
        }
    };


    private void openImagePromoterPagerFragment(int position) {
        ((MainActivity) getActivity()).setFragmentWithBackStack(PromoterPagerFragment.newInstance(
                promotersImageAdapter.getPromoters(), position, name.getText().toString()), null);
    }


    private void openFanImagePagerFragment(int position) {
        ((MainActivity) getActivity()).setFragmentWithBackStack(FanImagesPagerFragment.newInstance(
                fanImagesAdapter.getFanImages(), position, name.getText().toString()), null);
    }


    private void trackOpenDetailsFirebaseEvent(String name) {
        if (getActivity() != null) {
            Bundle bundle = new Bundle();
            bundle.putString(ConstantsFireBase.EVENT_NAME_PROPERTY, name);
            FirebaseAnalytics.getInstance(getActivity()).logEvent(ConstantsFireBase.EVENT_CARD_EVENT, bundle);
        }
    }


    private void trackUploadPhotoFirebaseEvent() {
        FirebaseAnalytics.getInstance(getContext()).logEvent(ConstantsFireBase.UPLOAD_PHOTO_EVENT, new Bundle());
    }


    private void trackBuyTicketFirebaseEvent() {
        FirebaseAnalytics.getInstance(getContext()).logEvent(ConstantsFireBase.TICKET_PURCHASE_EVENT, new Bundle());
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        LogUtil.logE("onConnectionFailed:" + connectionResult.getErrorMessage());
        Toast.makeText(getContext(), "Google Play Services error", Toast.LENGTH_SHORT).show();
    }
}
