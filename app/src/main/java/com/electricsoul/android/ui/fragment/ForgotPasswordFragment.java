package com.electricsoul.android.ui.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.Regex;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yuliasokolova on 15.11.16.
 */
public class ForgotPasswordFragment extends BaseFragment {

    private View rootView;

    @BindView(R.id.forgot_et_email)
    EditText etEmail;

    @BindView(R.id.reset_screen)
    LinearLayout resetScreen;

    @BindView(R.id.reset_password_content)
    LinearLayout resetPasswordContent;

    @BindView(R.id.tv_reset_password_title)
    TextView resetPswdTitle;

    @BindView(R.id.reset_pswd_ok)
    Button resetPswdOk;

    @BindView(R.id.tv_reset_sent)
    TextView tvResetSent;

    @BindView(R.id.tv_check_email)
    TextView tvCheckEmail;

    private Context context;


    public static ForgotPasswordFragment getInstance() {
        return new ForgotPasswordFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ButterKnife.bind(this, rootView);

        initFonts();

        KeyboardUtil.initFocusAndShowKeyboard(etEmail, getActivity());
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                    KeyboardUtil.hideKeyboard(getActivity());
                    requestResetPassword();
                }
                return false;
            }
        });
        return rootView;
    }


    private void requestResetPassword() {
        if(Regex.isValidEmail(etEmail.getText().toString())) {
            if (isClickUnlock() && ((MainActivity) getActivity()).isNetworkAvailable()) {
                showProgressDialog();
                RESTClient.getInstance().postPasswordReset(etEmail.getText().toString(), new Callback<JSONObject>() {
                    @Override
                    public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                        cancelProgressDialog();
                        resetScreen.setVisibility(View.VISIBLE);
                        resetPasswordContent.setVisibility(View.GONE);
                        if (isUserLogged()) {
                            resetPswdOk.setText(getString(R.string.forgot_reset_ok));
                        } else {
                            resetPswdOk.setText(getString(R.string.forgot_reset_back_to_login));
                        }
                    }


                    @Override
                    public void onFailure(Call<JSONObject> call, Throwable t) {
                        cancelProgressDialog();
                        Toast.makeText(getActivity(), "Reset was failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Toast.makeText(getActivity(), R.string.error_invalid_email, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        initKeyboardListener(rootView);
    }


    @Override
    public void onPause() {
        super.onPause();
        removeGlobalLayoutListener();
    }


    @OnClick(R.id.btn_get_email)
    void onGetEmail() {
        KeyboardUtil.hideKeyboard(getActivity());
        requestResetPassword();
    }


    @OnClick(R.id.reset_pswd_ok)
    void onClickResetOkButton() {
        resetScreen.setVisibility(View.GONE);

        if (isUserLogged()) {
            getFragmentManager().popBackStack();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            Fragment currentFrag = getActivity().getSupportFragmentManager().findFragmentByTag(ChangePasswordFragment.class.getSimpleName());

            if (currentFrag != null) {
                transaction.remove(currentFrag).commit();
            }
        }
        getActivity().onBackPressed();
    }


    private boolean isUserLogged() {
        return !SharedPrefManager.getInstance(getActivity()).getStoredUserId().equals("");
    }


    private void initFonts() {
        Typeface typeface_khula_semibold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_KHULA_SEMI_BOLD);
        Typeface typeface_exo_bold = Typeface.createFromAsset(getActivity().getAssets(),FontUtil.FONT_KHULA_EXO_BOLD);
        Typeface typeface_black = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_BLACK);
        Typeface typeface_medium = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        FontUtil.impl(resetPasswordContent, typeface_medium);
        resetPswdTitle.setTypeface(typeface_black);
        tvCheckEmail.setTypeface(typeface_khula_semibold);
        tvResetSent.setTypeface(typeface_exo_bold);
        resetPswdOk.setTypeface(typeface_exo_bold);
    }

}
