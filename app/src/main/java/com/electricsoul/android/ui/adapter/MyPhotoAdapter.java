package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.fragment.FanImagesPagerFragment;
import com.electricsoul.android.utils.PixelUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eugenetroyanskii on 09.12.16.
 */

public class MyPhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private Context context;
    private ArrayList<FanImageModel> fanImages;
    private String eventName;


    public MyPhotoAdapter(Context context, ArrayList<FanImageModel> fanImages, String eventName) {
        this.context = context;
        this.fanImages = fanImages;
        this.eventName = eventName;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyPhotoAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_photos, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        FanImageModel fanImageModel = fanImages.get(position);
        if (fanImageModel.hasFullImage()) {
            Picasso.with(context).load(fanImageModel.getImage().getFullSize()).placeholder(R.drawable.ic_event_empty).fit().centerCrop()
                    .into(((MyPhotoAdapter.ViewHolder) holder).image);
        }

        ((MyPhotoAdapter.ViewHolder) holder).cvImage.setVisibility(View.VISIBLE);
        ((MyPhotoAdapter.ViewHolder) holder).cvImage.setTag(position);
        ((MyPhotoAdapter.ViewHolder) holder).cvImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).setFragmentWithBackStack(FanImagesPagerFragment.newInstance(fanImages, position, eventName), null);
            }
        });
    }


    @Override
    public int getItemCount() {
        return fanImages.size();
    }


    @Override
    public void onClick(View view) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv_image)
        CardView cvImage;

        @BindView(R.id.image)
        ImageView image;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
