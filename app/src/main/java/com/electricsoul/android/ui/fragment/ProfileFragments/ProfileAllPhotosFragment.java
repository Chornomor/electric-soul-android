package com.electricsoul.android.ui.fragment.ProfileFragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.electricsoul.android.R;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.ui.adapter.PhotosAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.fragment.ProfileFragment;

import java.util.ArrayList;

public class ProfileAllPhotosFragment extends BaseFragment implements PhotosAdapter.onPhotoItemClickListenen {

    OnProfilePhotoClick listener;
    private RecyclerView photos;
    private PhotosAdapter photosAdapter;
    private boolean forProfileScreen;
    private ProfileFragment.OnScrollChangeProfileListener scrollChangeProfileListener;


    public static ProfileAllPhotosFragment newInstance(boolean forProfileScreen, boolean withBackButton
            , ProfileFragment.OnScrollChangeProfileListener scrollChangeProfileListener) {
        ProfileAllPhotosFragment fragment = new ProfileAllPhotosFragment();
        fragment.forProfileScreen = forProfileScreen;
        fragment.scrollChangeProfileListener = scrollChangeProfileListener;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_all_photos, container, false);

        photos = (RecyclerView) view.findViewById(R.id.scroll);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (position == 0) ? 2 : 1;
            }
        });
        photos.setLayoutManager(manager);
        photos.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (recyclerView.getScrollState()) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        scrollChangeProfileListener.onScrollChangeProfile();
                        break;
                }

            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        return view;
    }


    @Nullable
    @Override
    public void onItemClick(View view, int position, String eventId) {
        switch (view.getId()) {
            case R.id.image:
                listener.onPhotoClick(eventId);
                break;
        }
    }


    public void setOnPhotoClickListener(OnProfilePhotoClick listener) {
        this.listener = listener;
    }


    public void updatePhotosList(ArrayList<FanImageModel> fanImages) {
        photosAdapter = new PhotosAdapter(getContext(), fanImages);
        photosAdapter.setForProfileScreen(forProfileScreen);
        photosAdapter.setClickListener(this);
        this.photos.setAdapter(photosAdapter);
    }


    public interface OnProfilePhotoClick {
        void onPhotoClick(String eventId);
    }
}
