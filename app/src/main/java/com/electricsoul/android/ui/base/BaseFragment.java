package com.electricsoul.android.ui.base;

import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;

import com.electricsoul.android.network.NetworkReceiver;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.dialog.LoadingDialog;
import com.electricsoul.android.utils.FragmentUtils;
import com.electricsoul.android.utils.KeyboardUtil;

/**
 * Created by denisshovhenia on 09.11.16.
 */

public class BaseFragment extends Fragment implements NetworkReceiver.NetworkStateReceiverListener {

    View rootView;

    private MainActivity mainActivity;

    private LoadingDialog loadingDialog;

    public static long mLastClickTime;

    private NetworkReceiver networkStateReceiver;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }


    public void initKeyboardListener(View view) {
        mainActivity = (MainActivity) getActivity();
        mainActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        rootView = view;
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
    }


    public void removeGlobalLayoutListener() {
        rootView.getViewTreeObserver().removeOnGlobalLayoutListener(globalLayoutListener);
    }


    private ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            if (KeyboardUtil.isKeyboardVisible(rootView, mainActivity)) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        mainActivity.hideBottomToolbar();
                    }
                });
                mainActivity.hideBottomToolbar();
            } else {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        mainActivity.showBottomToolbar();
                    }
                });
            }
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        registerNetworkListener();
    }


    @Override
    public void onPause() {
        super.onPause();
        unregisterNetworkListener();
        cancelProgressDialog();
    }


    public void registerNetworkListener() {
        if (networkStateReceiver == null) {
            networkStateReceiver = new NetworkReceiver(this);
            getActivity().registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }


    public void unregisterNetworkListener() {
        if (networkStateReceiver != null) {
            getActivity().unregisterReceiver(networkStateReceiver);
            networkStateReceiver = null;
        }
    }


    public void showProgressDialog() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(getActivity());
        }
        loadingDialog.showProgressDialog();
    }


    public void cancelProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.cancelProgressDialog();
        }
    }


    public static boolean isClickUnlock() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 800) {
            return false;
        } else {
            mLastClickTime = SystemClock.elapsedRealtime();
            return true;
        }
    }


    public void addToolbarBackArrow(Toolbar toolbar) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);


        // add back arrow to toolbar
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    public void setToolbarTitle(String title) {

        if (getActivity() != null && ((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {
            };
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }


    @Override
    public void networkAvailable() {

    }


    @Override
    public void networkUnavailable() {
        cancelProgressDialog();
    }
}
