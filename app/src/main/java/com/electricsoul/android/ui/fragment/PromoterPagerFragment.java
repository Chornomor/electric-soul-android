package com.electricsoul.android.ui.fragment;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.Likes.EventImageLike;
import com.electricsoul.android.model.PromoterModel;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.dialog.DialogManager;
import com.electricsoul.android.utils.AnimateView;
import com.electricsoul.android.utils.DoubleClickListener;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.PhotoUtil;
import com.github.mmin18.widget.RealtimeBlurView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by yuliasokolova on 30.11.16.
 */

public class PromoterPagerFragment extends BaseFragment {

    private static final String LIKE_TAG = "like";
    private static final String UNLIKE_TAG = "unlike";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_promoter_details_name)
    TextView tvEventName;

    @BindView(R.id.tv_promoter_likes)
    TextView tvPromoterLikes;

    @BindView(R.id.iv_promoter_like)
    ImageView ivPromoterLike;

    @BindView(R.id.promoted_pager)
    ViewPager pager;

    @BindView(R.id.tv_current_position)
    TextView tvCurrentPosition;

    @BindView(R.id.tv_all_positions)
    TextView tvAllPositions;

    @BindView(R.id.iv_zoom_image)
    ImageView ivZoomImage;

    @BindView(R.id.zoom_layout)
    FrameLayout zoomLayout;

    @BindView(R.id.hidden_like)
    LinearLayout hiddenLike;

    @BindView(R.id.iv_hidden_like)
    ImageView ivHiddenLike;

    private ImagePagerAdapter imagePagerAdapter;
    private MainActivity mainActivity;
    private PhotoViewAttacher mAttacher;

    private ArrayList<PromoterModel> promotersList;
    private int position;
    private String eventName;


    public static PromoterPagerFragment newInstance(ArrayList<PromoterModel> promoters, int position, String name) {
        PromoterPagerFragment instance = new PromoterPagerFragment();
        instance.promotersList = promoters;
        instance.position = position;
        instance.eventName = name;
        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_promoted_image, container, false);
        ButterKnife.bind(this, rootView);

        mainActivity = (MainActivity) getActivity();
        tvAllPositions.setText(String.valueOf(promotersList.size()));
        tvCurrentPosition.setText(String.valueOf(position + 1));
        tvEventName.setText(eventName);
        mAttacher = new PhotoViewAttacher(ivZoomImage);
        setPager();
        addToolbarBackArrow(toolbar);
        initFonts();
        return rootView;
    }


    private void initFonts() {
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_KHULA_BOLD);
        tvEventName.setTypeface(bold);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        cancelProgressDialog();
    }


    private void setPager() {
        imagePagerAdapter = new ImagePagerAdapter(new OnImageClickForFragment() {
            @Override
            public void onImageFragmentClick(final String image) {
                Picasso.with(getActivity()).load(image).fit().centerCrop().into(ivZoomImage);
                mAttacher.update();
                zoomLayout.setVisibility(View.VISIBLE);
                mAttacher.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        DialogManager.showConfirmDialog(getContext(), R.string.save_option, R.string.save_photo_message, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                PhotoUtil.imageDownload(PromoterPagerFragment.this, getActivity(), image);
                            }
                        }, null);
                        return false;
                    }
                });
                try {
                    ((MainActivity) getActivity()).hideBottomToolbar();
                } catch (NullPointerException e) {
                }
            }


            @Override
            public void onImageFragmentDoubleClick() {
                onLikeClick();

            }
        });

        pager.setAdapter(imagePagerAdapter);
        pager.setCurrentItem(position, true);
        getLikes(position);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }


            @Override
            public void onPageSelected(int position) {
                tvCurrentPosition.setText(String.valueOf(position + 1));
                getLikes(position);
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void getLikes(int position) {
        if (mainActivity.isNetworkAvailable()) {
            RESTClient.getInstance().getEventLikesByEventId(
                    SharedPrefManager.getInstance(getActivity()).getStoredAuthToken(),
                    promotersList.get(position).getId(),
                    new Callback<EventImageLike>() {
                        @Override
                        public void onResponse(Call<EventImageLike> call, Response<EventImageLike> response) {
                            if (response.body() == null) {
                                tvPromoterLikes.setText("");
                                ivPromoterLike.setTag(UNLIKE_TAG);
                                return;
                            } else {
                                tvPromoterLikes.setText(String.valueOf(response.body().getCount()));
                            }

                            if (response.body().getEventIsLike().equals("1")) {
                                ivPromoterLike.setImageResource(R.drawable.lightening_like);
                                ivPromoterLike.setTag(LIKE_TAG);
                            } else {
                                ivPromoterLike.setImageResource(R.drawable.lighting_unlike);
                                ivPromoterLike.setTag(UNLIKE_TAG);
                            }
                        }


                        @Override
                        public void onFailure(Call<EventImageLike> call, Throwable t) {

                        }
                    });
        }
    }


    private class ImagePagerAdapter extends PagerAdapter {

        private PromoterPagerFragment.OnImageClickForFragment fragmentListener;


        public ImagePagerAdapter(PromoterPagerFragment.OnImageClickForFragment fragmentListener) {
            this.fragmentListener = fragmentListener;
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_promoter_pager_item, null);

            fillView(view, position);

            container.addView(view, 0);
            return view;
        }


        private void fillView(View view, int position) {
            RealtimeBlurView blurView = (RealtimeBlurView) view.findViewById(R.id.blur_view);
            ImageView blurImage = (ImageView) view.findViewById(R.id.iv_blur_image);
            setBlurImage(blurView, blurImage, position);
            ImageView clearImage = (ImageView) view.findViewById(R.id.iv_clear_image);
            setClearImage(clearImage, position);
            setImageClickListener(clearImage, position);
        }


        private void setBlurImage(RealtimeBlurView blurView, ImageView blurImage, int position) {
            blurView.setBlurRadius(25f);
            blurView.setOverlayColor(R.color.black);
            Picasso.with(getActivity()).load(promotersList.get(position).getImage().getThumbnail())
                    .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE).into(blurImage);
        }


        private void setClearImage(ImageView clearImage, int position) {
            Picasso.with(getActivity()).load(promotersList.get(position).getImage().getFullSize()).into(clearImage);
        }


        boolean isDoubleTap = false;


        private void setImageClickListener(final ImageView clearImage, final int position) {
            clearImage.setOnClickListener(new DoubleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    isDoubleTap = false;
                    callClickOptions(clearImage, position);
                }


                @Override
                public void onDoubleClick(View v) {
                    isDoubleTap = true;
                }
            });

        }


        private void callClickOptions(ImageView clearImage, final int position) {
            clearImage.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isDoubleTap) {
                        fragmentListener.onImageFragmentDoubleClick();
                    } else {
                        fragmentListener.onImageFragmentClick(promotersList.get(position).getImage().getFullSize());
                    }
                }
            }, 300);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }


        @Override
        public int getCount() {
            return promotersList.size();
        }
    }


    @OnClick(R.id.iv_back_zoom)
    void onClickBackFromZoom() {
        zoomLayout.setVisibility(View.GONE);
        ((MainActivity) getActivity()).showBottomToolbar();
    }


    @OnClick(R.id.iv_promoter_like)
    void onLikeClick() {
        if (getActivity() != null && !((MainActivity) getActivity()).isUserAuthorized()) {
            ((MainActivity) getActivity()).enableTab(MainActivity.PROFILE_TAB);
        }
        String imageId = promotersList.get(pager.getCurrentItem()).getId();
        String userId = SharedPrefManager.getInstance(getContext()).getStoredUserId();
        int isLike = -1;
        if (ivPromoterLike.getTag() != null && ivPromoterLike.getTag().equals(LIKE_TAG)) {
            isLike = 0;
        } else {
            isLike = 1;
        }
        final int finalIsLike = isLike;
        if (mainActivity.isNetworkAvailable()) {
            RESTClient.getInstance().postEventImageLike(imageId, userId, String.valueOf(isLike),
                    new Callback<JSONObject>() {
                        @Override
                        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                            AnimateView animation = new AnimateView(getActivity(), hiddenLike);
                            if (finalIsLike == 1) {
                                ivHiddenLike.setImageDrawable(getResources().getDrawable(R.drawable.white_anim_like));
                                animation.likeAnimation();
                                ivPromoterLike.setImageResource(R.drawable.lightening_like);
                                ivPromoterLike.setTag(LIKE_TAG);
                            } else {
                                ivHiddenLike.setImageDrawable(getResources().getDrawable(R.drawable.white_anim_unlike));
                                animation.likeAnimation();
                                ivPromoterLike.setImageResource(R.drawable.lighting_unlike);
                                ivPromoterLike.setTag(UNLIKE_TAG);
                            }
                            getLikes(pager.getCurrentItem());
                        }


                        @Override
                        public void onFailure(Call<JSONObject> call, Throwable t) {

                        }
                    }
            );
        }
    }


    public interface OnImageClickForFragment {
        void onImageFragmentClick(String image);


        void onImageFragmentDoubleClick();
    }

}
