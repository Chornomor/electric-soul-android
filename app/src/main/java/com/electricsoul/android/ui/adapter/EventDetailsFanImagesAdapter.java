package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.utils.PixelUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

public class EventDetailsFanImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private ArrayList<FanImageModel> fanImages;
    private Context context;
    private OnItemClickListener itemClickListener;


    public EventDetailsFanImagesAdapter(Context context, ArrayList<FanImageModel> promoters) {
        this.context = context;
        this.fanImages = promoters;
    }


    public ArrayList<FanImageModel> getFanImages() {
        return fanImages;
    }


    public boolean isHeader(int position) {
        return position == 0;
    }


    @Override
    public int getItemCount() {
        return fanImages.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.item_fan_photos, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        initFanImages(viewHolder, position);
    }


    private void initFanImages(RecyclerView.ViewHolder viewHolder, final int position) {
        FanImageModel fanImageModel = fanImages.get(position);
        viewHolder.itemView.setTag(position);

        if (fanImageModel.hasFullImage()) {
            Picasso.with(context).load(fanImageModel.getImage().getFullSize()).placeholder(R.drawable.ic_event_empty).fit().centerCrop()
                    .into(((ItemViewHolder) viewHolder).imageView);
        }
        if (isHeader(position)) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = new StaggeredGridLayoutManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setFullSpan(true);
            viewHolder.itemView.setLayoutParams(layoutParams);
            viewHolder.itemView.getLayoutParams().height = (int) PixelUtil.dpToPx(250, context);
            return;
        }
        viewHolder.itemView.getLayoutParams().height = getRandomHeight();
    }

    private int getRandomHeight(){
        int min = (int) PixelUtil.dpToPx(75, context);
        int max = (int) PixelUtil.dpToPx(100, context);
        return new Random().nextInt(min)+max;
    }


    @Override
    public void onClick(View view) {
        int position = Integer.parseInt(String.valueOf(view.getTag()));
        itemClickListener.onUserItemClick(view, position, null);
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;


        public ItemViewHolder(View view) {
            super(view);
            this.itemView.setOnClickListener(EventDetailsFanImagesAdapter.this);
            imageView = (ImageView) view.findViewById(R.id.image);
        }
    }


    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    public interface OnItemClickListener {
        void onUserItemClick(View v, int position, FanImageModel fanImageModel);
    }
}
