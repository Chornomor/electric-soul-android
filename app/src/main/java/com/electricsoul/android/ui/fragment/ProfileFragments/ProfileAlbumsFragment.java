package com.electricsoul.android.ui.fragment.ProfileFragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.electricsoul.android.R;
import com.electricsoul.android.model.EventModel;
import com.electricsoul.android.ui.adapter.AlbumsAdapter;
import com.electricsoul.android.ui.adapter.PhotosAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.fragment.ProfileFragment;

import java.util.ArrayList;

public class ProfileAlbumsFragment extends BaseFragment implements PhotosAdapter.onPhotoItemClickListenen {

    ProfileAllPhotosFragment.OnProfilePhotoClick listener;
    private RecyclerView photos;
    private AlbumsAdapter albumsAdapter;
    private boolean forProfileScreen;
    private ProfileFragment.OnScrollChangeProfileListener scrollChangeProfileListener;


    public static ProfileAlbumsFragment newInstance(boolean forProfileScreen, boolean withBackButton
            , ProfileFragment.OnScrollChangeProfileListener scrollChangeProfileListener) {
        ProfileAlbumsFragment fragment = new ProfileAlbumsFragment();
        fragment.forProfileScreen = forProfileScreen;
        fragment.scrollChangeProfileListener = scrollChangeProfileListener;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_all_photos, container, false);

        photos = (RecyclerView) view.findViewById(R.id.scroll);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        photos.setLayoutManager(manager);
        photos.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (recyclerView.getScrollState()) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        scrollChangeProfileListener.onScrollChangeProfile();
                        break;
                }

            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        return view;
    }


    @Nullable
    @Override
    public void onItemClick(View view, int position, String eventId) {
        switch (view.getId()) {
            case R.id.images_layout:
                listener.onPhotoClick(eventId);
                break;
        }
    }


    public void setOnPhotoClickListener(ProfileAllPhotosFragment.OnProfilePhotoClick listener) {
        this.listener = listener;
    }


    public void updatePhotosList(ArrayList<EventModel> eventsPhoto) {
        albumsAdapter = new AlbumsAdapter(getContext(), eventsPhoto);
        albumsAdapter.setForProfileScreen(forProfileScreen);
        albumsAdapter.setClickListener(this);
        this.photos.setAdapter(albumsAdapter);
    }
}
