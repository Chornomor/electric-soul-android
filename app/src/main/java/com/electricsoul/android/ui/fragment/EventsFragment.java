package com.electricsoul.android.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.EventModel;
import com.electricsoul.android.model.EventResultModel;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.EventsTabAdapter;
import com.electricsoul.android.ui.adapter.SelectLocationAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.model.LocationModel;
import com.electricsoul.android.model.LocationsResultModel;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.utils.IntentUtil;
import com.electricsoul.android.utils.LogUtil;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lumenghz.com.pullrefresh.PullToRefreshView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denisshovhenia on 09.11.16.
 */

public class EventsFragment extends BaseFragment implements View.OnClickListener, EventsTabAdapter.OnEventsClickListener,
        EventsTabAdapter.OnRSVPClickListener, EventsTabAdapter.OnCommentClickListener , GoogleApiClient.OnConnectionFailedListener {

    private static final String SUGGEST_CITY_ADDRESS        = "plur@electricsoul.com";
    private static final boolean AUTO_LAUNCH_DEEP_LINK      = false;

    public final static int FIRST_PAGE_NUMBER               = 1;
    public final static String FESTIVAL_TAG                 = "y";
    public final static String EVENT_TAG                    = "n";

    private View root;

    @BindView(R.id.rv_locations)
    RecyclerView rvSelectLocation;

    @BindView(R.id.pull_to_refresh)
    PullToRefreshView mPullToRefresh;

    @BindView(R.id.events_recycler_view)
    RecyclerView events;

    @BindView(R.id.select_location_conteiner)
    LinearLayout selectLocationConteiner;

    @BindView(R.id.btn_location_selection)
    Button btnSelectLocationToolBar;

    @BindView(R.id.btn_suggest_city)
    Button btnSuggestCity;

    @BindView(R.id.events_tabs)
    TabLayout eventsTabs;

    @BindView(R.id.content_layout)
    LinearLayout contentLayout;

    private LocationModel currentLocation;

    private EventsTabAdapter eventsAdapter;

    private GoogleApiClient mGoogleApiClient;

    private MainActivity mMainActivity;

    private int lastFollowEventPageNumber = FIRST_PAGE_NUMBER;

    private TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mMainActivity.selectedTabInEventFragment = tab.getPosition();
            fillEvents();
        }


        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }


        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };


    public static EventsFragment newInstance() {
        return new EventsFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_events, container, false);
        ButterKnife.bind(this, root);

        mMainActivity = (MainActivity) getActivity();
        initViews();

        if (mMainActivity.currentEventResultModel == null) {
            startLoadingData();
        } else {
            reinitData();
        }

        setupChooseLocationOnBackPress();
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        mMainActivity.showBottomToolbar();
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && getActivity() != null) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }


    private void reinitData() {
        eventsTabs.getTabAt(mMainActivity.selectedTabInEventFragment).select();
        setupLocationSelection(prepearListLocates(mMainActivity.locationsResult.getListLocations()));
        String storedCityCode = SharedPrefManager.getInstance(getActivity()).getPickedCityCode();
        btnSelectLocationToolBar.setText(storedCityCode);
        fillEvents();
    }


    private void startLoadingData() {
        if (mMainActivity.isNetworkAvailable()) {
            showProgressDialog();
            RESTClient.getInstance().getCities(getCitiesCallBack);
            if (mMainActivity.isUserAuthorized()) {
                lastFollowEventPageNumber = FIRST_PAGE_NUMBER;
                RESTClient.getInstance().getMyEvents(SharedPrefManager.getInstance(getActivity()).getStoredAuthToken(), FIRST_PAGE_NUMBER, getMyEventsCallback);
            }
        } else {
            mPullToRefresh.setRefreshing(false);
        }
    }


    private void setupLocationSelection(LinkedHashMap<String, ArrayList<LocationModel>> listLocations) {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvSelectLocation.setLayoutManager(llm);
        SelectLocationAdapter adapter = new SelectLocationAdapter(listLocations);
        adapter.setItemClickListener(new SelectLocationAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position, LocationModel locationModel) {
                if (mMainActivity.isNetworkAvailable()) {
                    showProgressDialog();
                    mMainActivity.needShowCityList = false;
                    currentLocation = locationModel;
                    requestEventsByCityId(currentLocation.getId(), currentLocation.getCityCode());
                    SharedPrefManager.getInstance(getActivity()).storePickedCityId(currentLocation.getId());
                    SharedPrefManager.getInstance(getActivity()).storePickedCityCode(currentLocation.getCityCode());
                    trackPickCityFirebaseEvent(currentLocation.getName());
                }
            }
        });
        rvSelectLocation.setAdapter(adapter);
    }


    private void setupChooseLocationOnBackPress() {
        if (root != null) {
            root.setFocusableInTouchMode(true);
            root.requestFocus();
            root.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && selectLocationConteiner.getVisibility() == View.VISIBLE) {
                        mMainActivity.needShowCityList = false;
                        hideSelectLocation();
                        return true;
                    }
                    getFragmentManager().popBackStack();
                    return false;
                }
            });
        }
    }


    private void requestEventsByCityId(String cityId, String cityCode) {
        btnSelectLocationToolBar.setText(cityCode);
        RESTClient.getInstance().getEventsByCity(cityId, EVENT_TAG, FIRST_PAGE_NUMBER, getEventsByCitiesCallback);
    }


    private LinkedHashMap<String, ArrayList<LocationModel>> prepearListLocates(List<LocationModel> list) {
        LinkedHashMap<String, ArrayList<LocationModel>> result = new LinkedHashMap<>();
        for (int i = 0; i < list.size(); ++i) {
            if (result.containsKey(list.get(i).getCountry())) {
                result.get(list.get(i).getCountry()).add(list.get(i));
            } else {
                ArrayList<LocationModel> cityList = new ArrayList<>();
                cityList.add(list.get(i));
                result.put(list.get(i).getCountry(), cityList);
            }
        }
        return result;
    }


    private void initViews() {
        eventsTabs.addTab(eventsTabs.newTab().setText(getString(R.string.events_tab_events)));
//        eventsTabs.addTab(eventsTabs.newTab().setText(R.string.events_tab_festivals));
        eventsTabs.setTabGravity(TabLayout.GRAVITY_CENTER);
        eventsTabs.addOnTabSelectedListener(tabSelectedListener);

        btnSuggestCity.setOnClickListener(this);
        btnSelectLocationToolBar.setOnClickListener(this);
        mPullToRefresh.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startLoadingData();
            }
        });
    }


    private void fillEvents() {
        if (!mMainActivity.needShowCityList) {
            hideSelectLocation();
        } else {
            showSelectLocations();
        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        events.setLayoutManager(mLayoutManager);
        switch (mMainActivity.selectedTabInEventFragment) {
            case 0:
                if (mMainActivity.currentEventResultModel != null) {
                    eventsAdapter = new EventsTabAdapter(getActivity(), mMainActivity.currentEventResultModel, false, "");
                }
                break;
            case 1:
                if (mMainActivity.festivals != null) {
                    eventsAdapter = new EventsTabAdapter(getActivity(), mMainActivity.festivals, false, "");
                }
                break;
        }
        if (eventsAdapter != null) {
            eventsAdapter.setEventsClickListener(this);
            eventsAdapter.setRSVPClickListener(this);
            eventsAdapter.setCommentClickListener(this);
        }
        events.setAdapter(eventsAdapter);

        handleDynamicLink();
    }


    private void handleDynamicLink() {
        if (getActivity() != null) {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                        .enableAutoManage(getActivity(), this)
                        .addApi(AppInvite.API)
                        .build();
            }

            AppInvite.AppInviteApi.getInvitation(mGoogleApiClient, getActivity(), AUTO_LAUNCH_DEEP_LINK)
                    .setResultCallback(
                            new ResultCallback<AppInviteInvitationResult>() {
                                @Override
                                public void onResult(@NonNull AppInviteInvitationResult result) {
                                    if (result.getStatus().isSuccess()) {
                                        Intent intent = result.getInvitationIntent();
                                        String eventId = AppInviteReferral.getDeepLink(intent).replace(EventsTabAdapter.DEEP_LINK, "");
                                        mMainActivity.setFragmentWithBackStack(EventDetailsFragment.newInstance(eventId), null);
                                    }
                                }
                            });
        }
    }


    @Override
    public void onClick(View view) {
        if (isClickUnlock()) {
            switch (view.getId()) {
                case R.id.btn_location_selection:
                    mMainActivity.needShowCityList = true;
                    showSelectLocations();
                    break;
                case R.id.btn_suggest_city:
                    sendSuggestCity();
                    break;
            }
        }
    }


    private void sendSuggestCity() {
        IntentUtil.callSendIntent(getActivity(), getContext().getResources().getString(R.string.email_subject),
                getContext().getResources().getString(R.string.email_body),
                getContext().getResources().getString(R.string.email_title), SUGGEST_CITY_ADDRESS);
    }


    @Override
    public void onEventsItemClick(View view, int position, EventModel event) {
        switch (view.getId()) {
            case R.id.event_root:
                if(((MainActivity)getActivity()).isNetworkAvailable()) {
                    mMainActivity.setFragmentWithBackStack(EventDetailsFragment.newInstance(event.getId()), null);
                }
                break;
        }
    }


    @Override
    public void onRSVPClickListener(int itemsCount) {
        if (itemsCount < 1) {
            contentLayout.setVisibility(View.GONE);
        } else {
            contentLayout.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onCommentClickListener(String eventId, String eventName) {
        mMainActivity.setFragmentWithBackStack(EventCommentsFragment.getInstance(eventId, eventName), null);
    }


    private void fillTemporaryEvents() {
        LocationModel tempCity = mMainActivity.locationsResult.getListLocations().get(0);
        requestEventsByCityId(tempCity.getId(), tempCity.getCityCode());
    }


    private Callback<LocationsResultModel> getCitiesCallBack = new Callback<LocationsResultModel>() {
        @Override
        public void onResponse(Call<LocationsResultModel> call, Response<LocationsResultModel> response) {
            if (response.body() != null) {
                mMainActivity.locationsResult = response.body();
                setupLocationSelection(prepearListLocates(mMainActivity.locationsResult.getListLocations()));
                String storedCityId = SharedPrefManager.getInstance(getActivity()).getPickedCityId();

                if (storedCityId.isEmpty()) {
                    mMainActivity.needShowCityList = true;
                    fillTemporaryEvents();
                    showSelectLocations();
                    cancelProgressDialog();
                } else {
                    mMainActivity.needShowCityList = false;
                    String storedCityCode = SharedPrefManager.getInstance(getActivity()).getPickedCityCode();
                    requestEventsByCityId(storedCityId, storedCityCode);
                }
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<LocationsResultModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };

    private Callback<EventResultModel> getEventsByCitiesCallback = new Callback<EventResultModel>() {
        @Override
        public void onResponse(Call<EventResultModel> call, Response<EventResultModel> response) {
            if (response.body() != null) {
                mMainActivity.currentEventResultModel = response.body();
                RESTClient.getInstance().getFestivalsByCity(SharedPrefManager.getInstance(getActivity()).getPickedCityId(), FESTIVAL_TAG, getFestivalsCallback);
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<EventResultModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private Callback<EventResultModel> getMyEventsCallback = new Callback<EventResultModel>() {
        @Override
        public void onResponse(Call<EventResultModel> call, Response<EventResultModel> response) {
            if (response.body() != null) {
                if (mMainActivity.rsvpListIds.size() < 1) {
                    mMainActivity.myFollowsEvent = response.body();
                    mMainActivity.rsvpListIds = prepareFollowsIdList(response.body());
                    if (mMainActivity.rsvpListIds.size() < mMainActivity.myFollowsEvent.getCount()) {
                        RESTClient.getInstance().getMyEvents(SharedPrefManager.getInstance(getActivity()).getStoredAuthToken(), ++lastFollowEventPageNumber, getMyEventsCallback);
                    } else if (getActivity() != null && !getActivity().isFinishing() && !getActivity().isDestroyed()) {
                        cancelProgressDialog();
                    }
                } else {
                    mMainActivity.myFollowsEvent.getEventModelList().addAll(response.body().getEventModelList());
                    mMainActivity.rsvpListIds.addAll(prepareFollowsIdList(response.body()));
                    if (mMainActivity.rsvpListIds.size() < mMainActivity.myFollowsEvent.getCount()) {
                        RESTClient.getInstance().getMyEvents(SharedPrefManager.getInstance(getActivity()).getStoredAuthToken(), ++lastFollowEventPageNumber, getMyEventsCallback);
                    } else if (getActivity() != null && !getActivity().isFinishing() && !getActivity().isDestroyed()) {
                        cancelProgressDialog();
                    }
                }
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<EventResultModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private Callback<EventResultModel> getFestivalsCallback = new Callback<EventResultModel>() {
        @Override
        public void onResponse(Call<EventResultModel> call, Response<EventResultModel> response) {
            if (response.body() != null) {
                mMainActivity.festivals = response.body();
                fillEvents();
                if (getActivity() != null && !getActivity().isFinishing() && !getActivity().isDestroyed()) {
                    cancelProgressDialog();
                }
                mPullToRefresh.setRefreshing(false);
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<EventResultModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private ArrayList<String> prepareFollowsIdList(EventResultModel follows) {
        ArrayList<String> followsIds = new ArrayList<>();
        if (follows != null) {
            for (int i = 0; i < follows.getEventModelList().size(); ++i) {
                followsIds.add(follows.getEventModelList().get(i).getId());
            }
        }
        return followsIds;
    }


    private void showSelectLocations() {
        selectLocationConteiner.setVisibility(View.VISIBLE);
        btnSelectLocationToolBar.setVisibility(View.GONE);
    }


    private void hideSelectLocation() {
        selectLocationConteiner.setVisibility(View.GONE);
        btnSelectLocationToolBar.setVisibility(View.VISIBLE);
    }


    private void trackPickCityFirebaseEvent(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsFireBase.CITY_NAME_PROPERTY, name);
        FirebaseAnalytics.getInstance(getContext()).logEvent(ConstantsFireBase.GEOLOCATION_EVENT, bundle);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        LogUtil.logE("onConnectionFailed");
    }
}
