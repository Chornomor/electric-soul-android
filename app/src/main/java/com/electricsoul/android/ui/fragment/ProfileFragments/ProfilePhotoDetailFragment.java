package com.electricsoul.android.ui.fragment.ProfileFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.MyPhotoAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.DateUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eugenetroyanskii on 08.12.16.
 */

public class ProfilePhotoDetailFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.event_date)
    TextView eventDate;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.photo_amount)
    TextView photoAmount;

    @BindView(R.id.tv_place_name)
    TextView placeName;

    @BindView(R.id.my_photo_scroll)
    RecyclerView photos;

    private ArrayList<FanImageModel> photosList;
    private MyPhotoAdapter photosAdapter;
    private String location, eventName;


    public static ProfilePhotoDetailFragment newInstance(ArrayList<FanImageModel> photos, String location, String eventName) {
        ProfilePhotoDetailFragment fragment = new ProfilePhotoDetailFragment();
        fragment.photosList = photos;
        fragment.location = location;
        fragment.eventName = eventName;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_profile_photo_detail, container, false);
        ButterKnife.bind(this, root);
        setupToolbar();
        if (photosList.size() < 1) {
            if (getActivity() != null) {
                ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_profile_photo), getString(R.string.error_message_profile_photo));
            }
            return root;
        }

        StaggeredGridLayoutManager gridManager = new StaggeredGridLayoutManager(2, 1);
        gridManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        photos.setLayoutManager(gridManager);
        photos.setHasFixedSize(false);

        if (photosList != null) {
            photoAmount.setText(photosList.size() + "");
            placeName.setText(location);
            String date = DateUtil.convertDate(photosList.get(0).getCreated());
            eventDate.setText(date);
        }

        updatePhotosList();
        return root;
    }


    private void setupToolbar() {
        addToolbarBackArrow(toolbar);
        toolbarTitle.setText(eventName);
    }


    private void updatePhotosList() {
        photosAdapter = new MyPhotoAdapter(getContext(), photosList, eventName);
        photos.setAdapter(photosAdapter);
    }
}
