package com.electricsoul.android.ui.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.widget.LinearLayout;

import com.electricsoul.android.R;
import com.electricsoul.android.constants.ConstantsNotification;
import com.electricsoul.android.manager.MixpanelManager;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.network.NetworkManager;
import com.electricsoul.android.network.NetworkReceiver;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.base.BaseActivity;
import com.electricsoul.android.utils.AnimateView;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.LogUtil;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yuliasokolova on 14.11.16.
 */

public class SplashActivity extends BaseActivity implements NetworkReceiver.NetworkStateReceiverListener {

    @BindView(R.id.error_view)
    LinearLayout noConnectionView;

    @BindView(R.id.root_layout)
    LinearLayout rootLayout;

    private String fireBaseToken;
    private static final int SPLASH_TIME = 2000;
    private NetworkReceiver networkStateReceiver;
    private Typeface bold;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        initFont();
        fireBaseToken = FirebaseInstanceId.getInstance().getToken();
        LogUtil.logE("SplashActivity FireBase Token [" + fireBaseToken + "]");
    }


    private void initFont() {
        bold = Typeface.createFromAsset(getAssets(), FontUtil.FONT_KHULA_BOLD);
        FontUtil.impl(rootLayout, bold);
    }


    @Override
    protected void onResume() {
        super.onResume();
        tryToLoginByStoredData();
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterNetworkListener();
    }


    private void tryToLoginByStoredData() {
        if (NetworkManager.isNetworkAvailable(this)) {
            String fbToken = SharedPrefManager.getInstance(this).getStoredFbToken();
            if (fbToken.isEmpty()) {
                RESTClient.getInstance().requestLogin(SharedPrefManager.getInstance(SplashActivity.this).getStoredLogin()
                        , SharedPrefManager.getInstance(this).getStoredPassword(), fireBaseToken, loginCallback);
            } else {
                RESTClient.getInstance().requestFacebookLogin(fbToken, fireBaseToken, loginCallback);
            }
        } else {
            registerNetworkListener();
            AnimateView animation = new AnimateView(this, noConnectionView);
            animation.showNoConnection();
        }
    }


    Callback<UserModel> loginCallback = new Callback<UserModel>() {
        @Override
        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
            UserModel currentUser = response.body();
            if (currentUser != null) {
                if (response.body().getAuthToken() == null) {
                    clearUserData();
                } else {
                    storeUserData(currentUser);
                }
            } else {
                clearUserData();
            }
            startLoadingMainActivity();
        }


        @Override
        public void onFailure(Call<UserModel> call, Throwable t) {
            clearUserData();
            startLoadingMainActivity();
        }
    };


    private void storeUserData(UserModel userModel) {
        SharedPrefManager.getInstance(this).storeEmail(userModel.getEmail());
        SharedPrefManager.getInstance(this).storeUserId(userModel.getId());
        SharedPrefManager.getInstance(this).storeAuthToken(userModel.getAuthToken());
        SharedPrefManager.getInstance(this).storeUserName(userModel.getUserName());
        MixpanelManager.registerMixpanel(this, fireBaseToken, userModel);
    }


    private void clearUserData() {
        SharedPrefManager.getInstance(this).storeEmail("");
        SharedPrefManager.getInstance(this).storeUserId("");
        SharedPrefManager.getInstance(this).storeAuthToken("");
        SharedPrefManager.getInstance(this).storeFbToken("");
        SharedPrefManager.getInstance(this).storeUserName("");
    }


    private void startLoadingMainActivity() {
        Handler handler = new Handler(Looper.getMainLooper());
        unregisterNetworkListener();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                if (getIntent() != null) {
                    String eventId = getIntent().getStringExtra(ConstantsNotification.NOTIFICATION_EVENT_ID);
                    if (eventId != null) {
                        setIntent(new Intent());
                        intent.putExtra(ConstantsNotification.NOTIFICATION_EVENT_ID, eventId);
                    }
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }, SPLASH_TIME);
    }


    public void registerNetworkListener() {
        if (networkStateReceiver == null) {
            networkStateReceiver = new NetworkReceiver(this);
            registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }


    public void unregisterNetworkListener() {
        if (networkStateReceiver != null) {
            unregisterReceiver(networkStateReceiver);
            networkStateReceiver = null;
        }
    }


    @Override
    public void networkAvailable() {
        tryToLoginByStoredData();
    }


    @Override
    public void networkUnavailable() {

    }
}
