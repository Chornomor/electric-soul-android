package com.electricsoul.android.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by alexchern on 11.01.17.
 */

public class ProfilePhotoFrameLayout extends FrameLayout {

    int currentHeight;

    private boolean isPagingEnabled = true;


    public ProfilePhotoFrameLayout(Context context) {
        super(context);
    }


    public ProfilePhotoFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public void measureCurrentHeight(int height) {
        currentHeight = height;
        requestLayout();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (currentHeight == 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(currentHeight, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}