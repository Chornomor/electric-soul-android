package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.model.UserResult;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.dialog.LoadingDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexchern on 10.11.16.
 */
public class EventDetailsUsersAdapter extends RecyclerView.Adapter<EventDetailsUsersAdapter.ViewHolder> {

    private final static int INITIAL_PAGE_NUMBER = 1;

    private OnItemClickListener itemClickListener;
    private List<UserModel> userModels = new ArrayList<>();
    private int totalUsersCount;
    private String eventId;
    private int pageNumber = INITIAL_PAGE_NUMBER;

    private Context context;
    private LoadingDialog loadingDialog;


    public EventDetailsUsersAdapter(Context context, UserResult usersResult, String eventId) {
        this.context = context;
        userModels = usersResult.getUsers();
        totalUsersCount = usersResult.getCount();
        this.eventId = eventId;
        notifyDataSetChanged();
    }


    @Override
    public EventDetailsUsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_going_users, parent, false);

        return new EventDetailsUsersAdapter.ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final UserModel item = userModels.get(position);
        holder.userName.setText(item.getUserName());

        if (item.getImage().getThumbnail() != null && !item.getImage().getThumbnail().isEmpty()) {
            Picasso.with(context)
                    .load(item.getImage().getThumbnail()).placeholder(context.getResources().getDrawable(R.drawable.avatar)).fit().centerCrop()
                    .into(holder.userPhoto);
        }

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int position = Integer.parseInt(String.valueOf(v.getTag()));
                itemClickListener.onItemClick(v, position, userModels.get(position));
            }
        });
        if (userModels.size() == position + 1 && position + 1 < totalUsersCount) {
            uploadMoreUsers();
        }
    }


    @Override
    public int getItemCount() {
        return userModels.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.events_user_photo)
        ImageView userPhoto;

        @BindView(R.id.events_user_name)
        TextView userName;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        itemClickListener = listener;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, UserModel user);
    }


    private void uploadMoreUsers() {
        showProgressDialog();
        RESTClient.getInstance().getUsersAttendingToEventById(eventId, ++pageNumber, getUsersImagesByIdCallback);
    }


    private void showProgressDialog() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(context);
        }
        loadingDialog.showProgressDialog();
    }


    private void cancelProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.cancelProgressDialog();
        }
    }


    private Callback<UserResult> getUsersImagesByIdCallback = new Callback<UserResult>() {

        @Override
        public void onResponse(Call<UserResult> call, Response<UserResult> response) {
            if (response.body() != null) {
                userModels.addAll(response.body().getUsers());
                notifyDataSetChanged();
                cancelProgressDialog();
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (context != null) {
                            ((MainActivity) context).showErrorView(context.getString(R.string.error_title_sorry), context.getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (context != null) {
                            ((MainActivity) context).showErrorView(context.getString(R.string.error_title_error), context.getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<UserResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };
}