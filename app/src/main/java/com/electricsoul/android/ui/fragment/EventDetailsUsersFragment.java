package com.electricsoul.android.ui.fragment;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.model.UserResult;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.EventDetailsUsersAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailsUsersFragment extends BaseFragment implements EventDetailsUsersAdapter.OnItemClickListener {

    @BindView(R.id.event_details_users)
    RecyclerView rvUsers;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.event_details_toolbar_title)
    TextView tvToolbarTitle;

    private EventDetailsUsersAdapter usersAdapter;

    private UserResult users;
    private String eventId;
    private int pageNumber = 1;


    public static EventDetailsUsersFragment newInstance(String eventId) {
        EventDetailsUsersFragment fragment = new EventDetailsUsersFragment();
        fragment.eventId = eventId;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_event_details_users, container, false);
        ButterKnife.bind(this, root);
        showProgressDialog();
        addToolbarBackArrow(toolbar);
        setFonts();
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        RESTClient.getInstance().getUsersAttendingToEventById(eventId, pageNumber, getUsersImagesByIdCallback);
    }


    private void initUsers() {
        usersAdapter = new EventDetailsUsersAdapter(getContext(), users, eventId);
        usersAdapter.setOnItemClickListener(this);
        rvUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        rvUsers.setAdapter(usersAdapter);
    }


    private void setFonts() {
        Typeface typeface_extrabold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_EXTRA_BOLD);
        tvToolbarTitle.setTypeface(typeface_extrabold);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tvToolbarTitle.setLetterSpacing(0.06f);
        }
    }


    @Override
    public void onItemClick(View view, int position, UserModel user) {
        ((MainActivity) getActivity()).setFragmentWithBackStack(ProfileFragment.newInstance(user.getId(), true), null);
    }


    Callback<UserResult> getUsersImagesByIdCallback = new Callback<UserResult>() {

        @Override
        public void onResponse(Call<UserResult> call, Response<UserResult> response) {
            if (response.body() != null) {
                users = response.body();
                initUsers();
                cancelProgressDialog();
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<UserResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };
}
