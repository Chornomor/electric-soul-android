package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.EventModel;
import com.electricsoul.android.model.EventResultModel;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.network.clients.FirebaseRESTClient;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.dialog.LoadingDialog;
import com.electricsoul.android.ui.fragment.EventsFragment;
import com.electricsoul.android.utils.DateUtil;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.IntentUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.electricsoul.android.ui.base.BaseFragment.isClickUnlock;
import static com.electricsoul.android.ui.fragment.EventsFragment.EVENT_TAG;

/**
 * Created by alexchern on 16.11.16.
 */

public class EventsTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private final static String DEEP_LINK_DOMEN = "https://pbs4v.app.goo.gl/?link=";
    public final static String DEEP_LINK = "http://electricsoul.com/event_id/";
    public final static String DOUBLE_NEW_LINE = "\n\n";
    private final static String PLATFORMS_PACKAGE_NAME = "&apn=com.electricsoul.android&amv=44&ibi=com.electricSoul&st=";
    private String SHARE_DYNAMIC_LINK = "";

    private List<EventModel> events = new ArrayList<>();
    private Context context;
    private OnEventsClickListener eventsClickListener;
    private OnRSVPClickListener rsvpClickListener;
    private OnCommentClickListener commentClickListener;
    private MainActivity mMainActivity;
    private boolean isMyEventsAdapter = true;
    private boolean isEmptyEventsList = false;
    private boolean forProfileScreen = false;
    private boolean withBackButton = false;
    private Typeface typeface_bold, typeface_semibold, typeface_medium;
    private LoadingDialog loadingDialog;
    private String userId;
    private int totalEventsCount;
    private int lastPageNumber = EventsFragment.FIRST_PAGE_NUMBER;


    public EventsTabAdapter(Context context, EventResultModel events, boolean isMyEventsAdapter, String userId) {
        this.context = context;
        this.events = events.getEventModelList();
        this.userId = userId;
        totalEventsCount = events.getCount();
        this.isMyEventsAdapter = isMyEventsAdapter;
        mMainActivity = (MainActivity) context;
        /*we need this check if user instant tap on profile after login*/
        if (context != null) {
            typeface_bold = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_KHULA_BOLD);
            typeface_semibold = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_KHULA_SEMI_BOLD);
            typeface_medium = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_event, parent, false));
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (isEmptyEventsList) {
            bindEmptyItemViewHolder(holder, position);
        } else {
            bindItemViewHolder(holder, position);
        }
    }


    private void bindItemViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final EventModel event = events.get(position);
        if (isImageExist(event)) {
            Picasso.with(context).load(event.getCoverImage().getFullSize()).centerCrop().fit().into(((ViewHolder) holder).photo);
        }

        ((ViewHolder) holder).emptyItem.setVisibility(View.GONE);
        ((ViewHolder) holder).event.setVisibility(View.VISIBLE);

        ((ViewHolder) holder).date.setText(DateUtil.convertEventFullDate(event.getStart(), event.getEnd()));
        ((ViewHolder) holder).name.setText(event.getName());
        ((ViewHolder) holder).place.setText(event.getAddress().getAddress1());
        ((ViewHolder) holder).time.setText(DateUtil.convertTime(event.getStart()) + " - " + DateUtil.convertTime(event.getEnd()));
        ((ViewHolder) holder).comment.setTag(position);
        ((ViewHolder) holder).comment.setOnClickListener(this);
        ((ViewHolder) holder).share.setTag(position);
        ((ViewHolder) holder).share.setOnClickListener(this);
        ((ViewHolder) holder).itemView.setTag(position);
        ((ViewHolder) holder).itemView.setOnClickListener(this);
        ((ViewHolder) holder).friendsCounter.setText(String.valueOf(event.getAttendees_count()));
        setTextFriendGoing((ViewHolder) holder, event.getAttendees_count());

        if (isUserRSVP(event.getId())) {
            ((ViewHolder) holder).rsvpText.setTextColor(ContextCompat.getColor(context, R.color.app_red_color));
            ((ViewHolder) holder).rsvpImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rsvp_ic_red));
        } else {
            ((ViewHolder) holder).rsvpText.setTextColor(ContextCompat.getColor(context, R.color.dark_gray_color));
            ((ViewHolder) holder).rsvpImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rsvp_ic));
        }

        if (event.isFestival()) {
            ((ViewHolder) holder).eventIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fest_icon));
        }

        ((ViewHolder) holder).follow.setTag(position);
        ((ViewHolder) holder).follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickUnlock()) {
                    if (mMainActivity.isNetworkAvailable()) {
                        if (mMainActivity.isUserAuthorized()) {
                            showProgressDialog();
                            int friendsCounter;
                            if (isUserRSVP(event.getId())) {
                                RESTClient.getInstance().rsvpToEvent(mMainActivity.currentUserToken, event.getId(), false, rsvpCallback);
                                friendsCounter = (Integer.parseInt((((ViewHolder) holder).friendsCounter).getText().toString()) - 1);
                                setTextFriendGoing((ViewHolder) holder, friendsCounter);
                                ((ViewHolder) holder).friendsCounter.setText(friendsCounter + "");
                                ((ViewHolder) holder).rsvpText.setTextColor(ContextCompat.getColor(context, R.color.dark_gray_color));
                                ((ViewHolder) holder).rsvpImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rsvp_ic));
                                mMainActivity.rsvpListIds.remove(event.getId());
                                mMainActivity.removeFromMyFollowsEvent(event);
                                if (!withBackButton) {
                                    removeRSVPFromMyEvents(event);
                                    rsvpClickListener.onRSVPClickListener(getItemCount());
                                }
                                trackRSVPSFirebaseEvent("No");
                            } else {
                                RESTClient.getInstance().rsvpToEvent(mMainActivity.currentUserToken, event.getId(), true, rsvpCallback);
                                friendsCounter = (Integer.parseInt((((ViewHolder) holder).friendsCounter).getText().toString()) + 1);
                                setTextFriendGoing((ViewHolder) holder, friendsCounter);
                                ((ViewHolder) holder).friendsCounter.setText(friendsCounter + "");
                                ((ViewHolder) holder).rsvpText.setTextColor(ContextCompat.getColor(context, R.color.app_red_color));
                                ((ViewHolder) holder).rsvpImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rsvp_ic_red));
                                mMainActivity.addToMyFollowsEvent(event);
                                trackRSVPSFirebaseEvent("Yes");
                            }
                        } else {
                            openAuthFragment();
                        }
                    }
                }
            }
        });

        ((ViewHolder) holder).commentText.setTypeface(typeface_medium);
        ((ViewHolder) holder).shareText.setTypeface(typeface_medium);
        ((ViewHolder) holder).rsvpText.setTypeface(typeface_medium);
        ((ViewHolder) holder).friendsCounter.setTypeface(typeface_medium);
        ((ViewHolder) holder).friendText.setTypeface(typeface_medium);
        ((ViewHolder) holder).time.setTypeface(typeface_semibold);
        ((ViewHolder) holder).place.setTypeface(typeface_semibold);
        ((ViewHolder) holder).name.setTypeface(typeface_bold);

        if (events.size() == position + 1 && position + 1 < totalEventsCount) {
            uploadMoreEvents();
        }
    }


    private void setTextFriendGoing(ViewHolder holder, int friendsCounter) {
        if (friendsCounter == 1) {
            holder.friendText.setText(context.getResources().getString(R.string.events_attendees_friend));
        } else {
            holder.friendText.setText(context.getResources().getString(R.string.events_attendees_friends));
        }
    }


    private void uploadMoreEvents() {
        if (isMyEventsAdapter) {
            showProgressDialog();
            RESTClient.getInstance().getUserEventsById(userId, lastPageNumber + 1, getUserEventsCallback);
        } else {
            showProgressDialog();
            RESTClient.getInstance().getEventsByCity(SharedPrefManager.getInstance(context).getPickedCityId(), EVENT_TAG, lastPageNumber + 1, getEventsByCitiesCallback);
        }
    }


    private void trackRSVPSFirebaseEvent(String yesOrNo) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsFireBase.RSVP_STATE_PROPERTY, yesOrNo);
        FirebaseAnalytics.getInstance(context).logEvent(ConstantsFireBase.RSVPS_EVENT, bundle);
    }


    private void bindEmptyItemViewHolder(final RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).emptyItem.setVisibility(View.VISIBLE);
        ((ViewHolder) holder).event.setVisibility(View.GONE);
    }


    private void removeRSVPFromMyEvents(EventModel event) {
        if (isMyEventsAdapter) {
            events.remove(event);
            totalEventsCount--;
            notifyDataSetChanged();
        }
    }


    private boolean isUserRSVP(String eventId) {
        return mMainActivity.rsvpListIds.contains(eventId);
    }


    private boolean isImageExist(EventModel event) {
        return event.getCoverImage().getFullSize() != null && !event.getCoverImage().getFullSize().isEmpty();
    }


    private void openAuthFragment() {
        mMainActivity.enableTab(MainActivity.PROFILE_TAB);
    }


    @Override
    public int getItemCount() {
        if (forProfileScreen) {
            if (events.size() == 0) {
                isEmptyEventsList = true;
                return events.size() + 1;
            } else {
                return events.size();
            }
        } else {
            return events.size();
        }
    }


    public void setForProfileScreen(boolean forProfileScreen) {
        this.forProfileScreen = forProfileScreen;
    }


    public void setWithBackButton(boolean withBackButton) {
        this.withBackButton = withBackButton;
    }


    private int sharedEventPosition = 0;
    @Override
    public void onClick(View view) {
        if (isClickUnlock()) {
            switch (view.getId()) {
                case R.id.event_comments:
                    if (mMainActivity.isUserAuthorized()) {
                        int position = (int) view.getTag();
                        commentClickListener.onCommentClickListener(events.get(position).getId(), events.get(position).getName());
                    } else {
                        openAuthFragment();
                    }
                    break;
                case R.id.event_share:
                    if (mMainActivity.isUserAuthorized()) {

                        sharedEventPosition = (int) view.getTag();
                        EventModel event = events.get(sharedEventPosition);
                        SHARE_DYNAMIC_LINK = DEEP_LINK_DOMEN + DEEP_LINK + event.getId() + PLATFORMS_PACKAGE_NAME + getShareTextForLink(event.getName())
                                + "&si=" + event.getCoverImage().getFullSize();

                        showProgressDialog();
                        FirebaseRESTClient.getInstance().getShortDynamicLick(SHARE_DYNAMIC_LINK, getShortDynamicLickCallback);
                    } else {
                        openAuthFragment();
                    }
                    break;
                case R.id.event_root:
                    if (eventsClickListener != null) {
                        int position = Integer.parseInt(String.valueOf(view.getTag()));
                        eventsClickListener.onEventsItemClick(view, position, events.get(position));
                    }
                    break;
            }
        }
    }


    private String getShareTextForLink(String name){
        try {
            name = URLEncoder.encode(name, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return name;
    }


    private void shareDynamicLink(String dynamicLink){
        EventModel event = events.get(sharedEventPosition);
        String sharingBody = "Event name: " + event.getName() + DOUBLE_NEW_LINE + "Venue: " +
                event.getAddress().getAddress1() + DOUBLE_NEW_LINE + "Time and Date " +
                DateUtil.convertFullDate(event.getStart(), event.getEnd()) + DOUBLE_NEW_LINE + dynamicLink;
        IntentUtil.callShareIntent(mMainActivity, event.getName(), sharingBody, "Share via:");
    }


    private Callback<EventResultModel> getEventsByCitiesCallback = new Callback<EventResultModel>() {
        @Override
        public void onResponse(Call<EventResultModel> call, Response<EventResultModel> response) {
            if (response.body() != null) {
                lastPageNumber++;
                events.addAll(response.body().getEventModelList());
                cancelProgressDialog();
                notifyDataSetChanged();
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (context != null) {
                            ((MainActivity) context).showErrorView(context.getString(R.string.error_title_sorry), context.getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (context != null) {
                            ((MainActivity) context).showErrorView(context.getString(R.string.error_title_error), context.getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<EventResultModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private Callback<EventResultModel> getUserEventsCallback = new Callback<EventResultModel>() {
        @Override
        public void onResponse(Call<EventResultModel> call, Response<EventResultModel> response) {
            if (response.body() != null) {
                lastPageNumber++;
                events.addAll(response.body().getEventModelList());
                cancelProgressDialog();
                notifyDataSetChanged();
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (context != null) {
                            ((MainActivity) context).showErrorView(context.getString(R.string.error_title_sorry), context.getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (context != null) {
                            ((MainActivity) context).showErrorView(context.getString(R.string.error_title_error), context.getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<EventResultModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private Callback<Object> rsvpCallback = new Callback<Object>() {
        @Override
        public void onResponse(Call<Object> call, Response<Object> response) {
            cancelProgressDialog();
        }


        @Override
        public void onFailure(Call<Object> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private void showProgressDialog() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(context);
        }
        loadingDialog.showProgressDialog();
    }


    private void cancelProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.cancelProgressDialog();
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_events_photo)
        ImageView photo;

        @BindView(R.id.event_icon)
        ImageView eventIcon;

        @BindView(R.id.tv_event_date)
        TextView date;

        @BindView(R.id.tv_event_name)
        TextView name;

        @BindView(R.id.tv_event_friends_counter)
        TextView friendsCounter;

        @BindView(R.id.tv_event_friends_text)
        TextView friendText;

        @BindView(R.id.tv_event_place)
        TextView place;

        @BindView(R.id.tv_event_time)
        TextView time;

        @BindView(R.id.event_follow)
        View follow;

        @BindView(R.id.event_comments)
        LinearLayout comment;

        @BindView(R.id.event_share)
        LinearLayout share;

        @BindView(R.id.empty_layout)
        FrameLayout emptyItem;

        @BindView(R.id.event)
        LinearLayout event;

        @BindView(R.id.iv_event_item_rsvp)
        ImageView rsvpImage;

        @BindView(R.id.tv_event_item_rsvp)
        TextView rsvpText;

        @BindView(R.id.tv_event_item_share)
        TextView shareText;

        @BindView(R.id.tv_event_item_comment)
        TextView commentText;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void setEventsClickListener(OnEventsClickListener listener) {
        eventsClickListener = listener;
    }


    public interface OnEventsClickListener {
        void onEventsItemClick(View view, int position, EventModel event);
    }


    public void setRSVPClickListener(OnRSVPClickListener listener) {
        rsvpClickListener = listener;
    }


    public void setCommentClickListener(OnCommentClickListener listener) {
        commentClickListener = listener;
    }


    public interface OnRSVPClickListener {
        void onRSVPClickListener(int itemsCount);
    }


    public interface OnCommentClickListener {
        void onCommentClickListener(String eventId, String eventName);
    }




    private Callback<JsonObject> getShortDynamicLickCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            cancelProgressDialog();
            if (response.body() != null) {
                String shortLink = null;
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    shortLink = jsonObject.getString("shortLink");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (shortLink == null){
                    shareDynamicLink(SHARE_DYNAMIC_LINK);
                } else {
                    shareDynamicLink(shortLink);
                }
            }
        }


        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            cancelProgressDialog();
        }
    };

}
