package com.electricsoul.android.ui.fragment;

import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.DetailEventModel;
import com.electricsoul.android.model.EventResultModel;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.model.FanImageResult;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.CacheFragmentStatePagerAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.dialog.FollowDialog;
import com.electricsoul.android.ui.fragment.ProfileFragments.ProfileEventsFragment;
import com.electricsoul.android.ui.fragment.ProfileFragments.ProfilePhotoDetailFragment;
import com.electricsoul.android.ui.fragment.ProfileFragments.ProfileAllPhotosFragment;
import com.electricsoul.android.ui.fragment.ProfileFragments.ProfilePhotosFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.PixelUtil;
import com.electricsoul.android.ui.views.WrapContentHeightViewPager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denisshovhenia on 09.11.16.
 */

public class ProfileFragment extends BaseFragment implements ProfileAllPhotosFragment.OnProfilePhotoClick {

    private final int TAB_EVENTS = 0;
    private final int TAB_PHOTOS = 1;
    private final long ANIMATION_DURATION = 200;

    private static final String FOLLOW_KEY = "1";
    private static final String NOT_FOLLOW_KEY = "0";

    @BindView(R.id.tabs)
    TabLayout tabs;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.view_pager)
    WrapContentHeightViewPager viewPager;

    @BindView(R.id.profile_user_name)
    TextView profileUserName;

    @BindView(R.id.profile_user_photo)
    CircleImageView provileUserPhoto;

    @BindView(R.id.profile_image)
    ImageView profileImage;

    @BindView(R.id.profile_followers_count)
    TextView profileFollowersCount;

    @BindView(R.id.profile_followings_count)
    TextView profileFollowingsCount;

    @BindView(R.id.profile_followings)
    TextView profileFollowingsTitle;

    @BindView(R.id.profile_followers)
    TextView profileFollowersTitle;

    @BindView(R.id.profile_settings)
    ImageView settingsBtn;

    @BindView(R.id.profile_header)
    LinearLayout profileHeader;

    @BindView(R.id.btn_follow_to_user)
    View btnFollowToUser;

    @BindView(R.id.icon_btn_follow)
    ImageView icBtnFollow;

    @BindView(R.id.app_bar)
    AppBarLayout appBar;

    @BindView(R.id.follow_btn)
    TextView followBtnTitle;

    private MainActivity mainActivity;
    private ProfilePagerAdapter mPagerAdapter;
    private ProfileFragment instance;

    private UserModel currentUser;
    private EventResultModel eventResultModel;
    private FanImageResult fanImageResult;

    private HashMap<String, ArrayList<FanImageModel>> myPhotos = new HashMap<>();
    private String userId;
    private String currentUserId;
    private boolean withBackButton;
    private boolean mFabIsShown;
    private boolean isContainedInMyFollowings;
    private int lastVerticalOffset = -1;
    private int currentVerticalOffset = -1;

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            viewPager.setCurrentItem(tab.getPosition());
            switch (tab.getPosition()) {
                case TAB_EVENTS:
                    if (mainActivity.isNetworkAvailable() && eventResultModel == null) {
                        showProgressDialog();
                        RESTClient.getInstance().getUserEventsById(userId, 1, getMyEventsCallback);
                    }
                    break;
                case TAB_PHOTOS:
                    if (mainActivity.isNetworkAvailable() && fanImageResult == null) {
                        showProgressDialog();
                        RESTClient.getInstance().getUserFanImages(userId
                                , SharedPrefManager.getInstance(getActivity()).getStoredAuthToken(), getUserFanImagesCallback);
                    } else {
                        fillPhotos();
                    }
                    break;
            }
        }


        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }


        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };


    public static ProfileFragment newInstance(String userId, boolean withBackButton) {
        ProfileFragment instance = new ProfileFragment();
        instance.userId = userId;
        instance.withBackButton = withBackButton;
        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, root);
        showProgressDialog();
        mainActivity = ((MainActivity) getActivity());

        setPagerHeight();

        checkIsContainedInMyFollowings();

        currentUserId = SharedPrefManager.getInstance(getContext()).getStoredUserId();

        tabs.addTab(tabs.newTab().setText(getString(R.string.events_tab)));
        tabs.addTab(tabs.newTab().setText(getString(R.string.profile_photos_tab)));

        if (withBackButton) {
            addToolbarBackArrow(toolbar);
            if (!userId.equals(currentUserId)) {
                btnFollowToUser.setVisibility(View.VISIBLE);
            } else {
                btnFollowToUser.setVisibility(View.GONE);
            }
            settingsBtn.setVisibility(View.GONE);
            checkInMyFollowings();
        } else {
            btnFollowToUser.setVisibility(View.GONE);
            settingsBtn.setVisibility(View.VISIBLE);
        }

        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                lastVerticalOffset = currentVerticalOffset;
                currentVerticalOffset = verticalOffset;
                adjustSettingsBtn(currentVerticalOffset);
            }
        });

        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.addOnTabSelectedListener(tabSelectedListener);

        if (instance == null) {
            instance = this;
        }
        mPagerAdapter = new ProfileFragment.ProfilePagerAdapter(getChildFragmentManager(), tabs.getTabCount());
        viewPager.setAdapter(mPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        if (mainActivity.isNetworkAvailable()) {
            RESTClient.getInstance().getUserById(userId, requestUserCallback);
            RESTClient.getInstance().getUserEventsById(userId, 1, getMyEventsCallback);
        }

        initTypeFace();
        KeyboardUtil.hideKeyboard(mainActivity);
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        mainActivity.showBottomToolbar();
    }


    @Override
    public void onPause() {
        super.onPause();
        eventResultModel = null;
        fanImageResult = null;
    }


    private void setPagerHeight() {
        Display display = mainActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        int pagerHeight = size.y - actionBarHeight - (int) PixelUtil.dpToPx(80, getContext());
        viewPager.measureCurrentHeight(pagerHeight);
    }


    private void checkInMyFollowings() {
        if (isContainedInMyFollowings) {
            icBtnFollow.setImageResource(R.drawable.ic_follow_minus_red_icon);
        } else {
            icBtnFollow.setImageResource(R.drawable.ic_plus_btn);
        }
    }


    private void checkIsContainedInMyFollowings() {
        if (mainActivity.followings != null) {
            isContainedInMyFollowings = mainActivity.followings.isUserFollowing(userId);
        }
    }


    @OnClick(R.id.profile_settings)
    public void onSettingClick() {
        if (isClickUnlock()) {
            ((MainActivity) getActivity()).setFragmentWithBackStack(
                    ProfileSettingsFragment.newInstance(userId), ProfileSettingsFragment.class.getSimpleName());
        }
    }


    @OnClick(R.id.ll_followings)
    public void onClickFollowing() {
        if (isClickUnlock()) {
            ((MainActivity) getActivity()).setFragmentWithBackStack(FollowFragment.getInstance(true, userId), null);
        }
    }


    @OnClick(R.id.ll_followers)
    public void onClickFollowers() {
        if (isClickUnlock()) {
            ((MainActivity) getActivity()).setFragmentWithBackStack(FollowFragment.getInstance(false, userId), null);
        }
    }


    @OnClick(R.id.btn_follow_to_user)
    public void onClickFollowToUser() {
        if (isClickUnlock()) {
            if (!currentUserId.isEmpty()) {
                if (isContainedInMyFollowings) {
                    if (currentUser != null) {
                        new FollowDialog().showDialog(mainActivity, currentUser, false, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                isContainedInMyFollowings = false;
                                mainActivity.followings.removeFollowings(currentUser);
                                icBtnFollow.setImageResource(R.drawable.ic_plus_btn);
                                followBtnTitle.setText(R.string.btn_follow);
                                trackUnFollowFirebaseEvent();
                                RESTClient.getInstance().postFollowUser(SharedPrefManager.getInstance(getActivity()).getStoredAuthToken(), NOT_FOLLOW_KEY, currentUser.getId(), currentUserId, followCallback);
                            }
                        });
                    }
                } else {
                    if (currentUser != null) {
                        new FollowDialog().showDialog(mainActivity, currentUser, true, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                isContainedInMyFollowings = true;
                                mainActivity.followings.addFollowings(currentUser);
                                icBtnFollow.setImageResource(R.drawable.ic_follow_minus_red_icon);
                                followBtnTitle.setText(R.string.btn_unfollow);
                                trackFollowFirebaseEvent();
                                RESTClient.getInstance().postFollowUser(SharedPrefManager.getInstance(getActivity()).getStoredAuthToken(), FOLLOW_KEY, currentUser.getId(), currentUserId, followCallback);
                            }
                        });
                    }
                }
            } else {
                Toast.makeText(getContext(), R.string.error_login_to_follow, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void adjustSettingsBtn(int verticalOffset) {
        if (isNeedHideSettingBtn(verticalOffset)) {
            hideFab(true);
        } else {
            showFab(true);
        }
    }


    private void adjustProfileHeader() {
        if (lastVerticalOffset < currentVerticalOffset) {
            appBar.setExpanded(true, true);
        } else {
            appBar.setExpanded(false, true);
        }
    }


    private boolean isNeedHideSettingBtn(int verticalOffset) {
        int settingY = (int) settingsBtn.getY() + settingsBtn.getHeight();
        int getTranslationY = profileHeader.getHeight() + verticalOffset;
        return settingY >= getTranslationY;
    }


    private void showFab(boolean animated) {
        toolbar.setVisibility(View.VISIBLE);
        if (settingsBtn == null) {
            return;
        }
        if (!mFabIsShown) {
            settingsBtn.setClickable(true);
            settingsBtn.setEnabled(true);
            if (animated) {
                ViewPropertyAnimator.animate(settingsBtn).cancel();
                ViewPropertyAnimator.animate(settingsBtn).scaleX(1).scaleY(1).setDuration(ANIMATION_DURATION).start();
            } else {
                ViewHelper.setScaleX(settingsBtn, 1);
                ViewHelper.setScaleY(settingsBtn, 1);
            }
            mFabIsShown = true;
        } else {
            // Ensure that FAB is shown
            ViewHelper.setScaleX(settingsBtn, 1);
            ViewHelper.setScaleY(settingsBtn, 1);
        }
    }


    private void hideFab(boolean animated) {
        toolbar.setVisibility(View.GONE);
        if (settingsBtn == null) {
            return;
        }
        if (mFabIsShown) {
            settingsBtn.setClickable(false);
            settingsBtn.setEnabled(false);
            if (animated) {
                ViewPropertyAnimator.animate(settingsBtn).cancel();
                ViewPropertyAnimator.animate(settingsBtn).scaleX(0).scaleY(0).setDuration(ANIMATION_DURATION).start();
            } else {
                ViewHelper.setScaleX(settingsBtn, 0);
                ViewHelper.setScaleY(settingsBtn, 0);
            }
            mFabIsShown = false;
        } else {
            // Ensure that FAB is hidden
            ViewHelper.setScaleX(settingsBtn, 0);
            ViewHelper.setScaleY(settingsBtn, 0);
        }
    }


    public void fillEvents() {
        /*we use try/catch for prevent crash when user open pager tab and instantly close this fragment*/
        try {
            ((ProfileEventsFragment) mPagerAdapter.getItemAt(0)).updateEventsList(eventResultModel, userId);
            if (fanImageResult != null) {
                fillPhotos();
            }
        } catch (NullPointerException exception) {
            exception.printStackTrace();
        }
    }


    public void fillPhotos() {
        collectMyPhotosForEvents(fanImageResult);
        /*we use try/catch for prevent crash when user open pager tab and instantly close this fragment*/
        try {
            ((ProfilePhotosFragment) mPagerAdapter.getItemAt(1)).updatePhotosList(eventResultModel, fanImageResult.getFanImages(), this);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void trackFollowFirebaseEvent() {
        FirebaseAnalytics.getInstance(mainActivity).logEvent(ConstantsFireBase.FOLLOW_USER_EVENT, new Bundle());
    }


    private void trackUnFollowFirebaseEvent() {
        FirebaseAnalytics.getInstance(mainActivity).logEvent(ConstantsFireBase.UNFOLLOW_USER_EVENT, new Bundle());
    }


    @Override
    public void onPhotoClick(String eventId) {
        RESTClient.getInstance().getEventById(eventId, getEventCallback);
    }


    public class ProfilePagerAdapter extends CacheFragmentStatePagerAdapter {
        int numOfTabs;


        ProfilePagerAdapter(FragmentManager fm, int numOfTabs) {
            super(fm);
            this.numOfTabs = numOfTabs;
        }


        @Override
        protected Fragment createItem(int position) {
            switch (position) {
                case TAB_EVENTS:
                    return ProfileEventsFragment.newInstance(true, withBackButton, scrollChangeProfileListener);
                case TAB_PHOTOS:
                    return ProfilePhotosFragment.newInstance(withBackButton, scrollChangeProfileListener);
                default:
                    return null;
            }
        }


        @Override
        public int getCount() {
            return numOfTabs;
        }
    }


    private void setupProfile() {
        if (currentUser.hasImageThumbnail()) {
            Picasso.with(getActivity()).load(currentUser.getImage().getThumbnail())
                    .centerCrop().fit().into(provileUserPhoto);
        }
        if (currentUser.hasCoverImageThumbnail()) {
            Picasso.with(getActivity()).load(currentUser.getCoverImage().getFullSize())
                    .centerCrop().fit().into(profileImage);
        }
        profileUserName.setText(currentUser.getUserName());
        profileFollowersCount.setText(String.valueOf(currentUser.getFollowerCount()));
        profileFollowingsCount.setText(String.valueOf(currentUser.getFollowingCount()));
    }


    private Callback<EventResultModel> getMyEventsCallback = new Callback<EventResultModel>() {
        @Override
        public void onResponse(Call<EventResultModel> call, Response<EventResultModel> response) {
            if (response.body() != null) {
                eventResultModel = response.body();
                fillEvents();
            } else {
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
            cancelProgressDialog();
        }


        @Override
        public void onFailure(Call<EventResultModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    Callback<UserModel> requestUserCallback = new Callback<UserModel>() {
        @Override
        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
            currentUser = response.body();
            if (currentUser != null) {
                setupProfile();
            } else {
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<UserModel> call, Throwable t) {
        }
    };


    private void collectMyPhotosForEvents(FanImageResult result) {
        if (result == null) {
            return;
        }
        collectPhotosForEvents(result);
        myPhotos.clear();
        for (FanImageModel model : result.getFanImages()) {
            if (myPhotos.containsKey(model.getEventId())) {
                myPhotos.get(model.getEventId()).add(model);
            } else {
                ArrayList<FanImageModel> photosValue = new ArrayList<>();
                photosValue.add(model);
                myPhotos.put(model.getEventId(), photosValue);
            }
        }
    }


    private void collectPhotosForEvents(FanImageResult result) {
        if (result == null) {
            return;
        }
        if (eventResultModel != null) {
            eventResultModel.collectPhotosForEvents(result);
        }
    }


    Callback<FanImageResult> getUserFanImagesCallback = new Callback<FanImageResult>() {
        @Override
        public void onResponse(Call<FanImageResult> call, Response<FanImageResult> response) {
            fanImageResult = response.body();
            if (fanImageResult != null) {
                fillPhotos();
            } else {
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
            cancelProgressDialog();
        }


        @Override
        public void onFailure(Call<FanImageResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };

    Callback<DetailEventModel> getEventCallback = new Callback<DetailEventModel>() {
        @Override
        public void onResponse(Call<DetailEventModel> call, Response<DetailEventModel> response) {
            cancelProgressDialog();
            if (response.body() != null) {
                DetailEventModel eventModel = response.body();
                ((MainActivity) getActivity()).setFragmentWithBackStack(ProfilePhotoDetailFragment.newInstance(myPhotos.get(eventModel.getId()), eventModel.getAddress().getAddress1(), eventModel.getName()), null);
            } else {
                switch (response.code()) {
                    case ConstantsAPI.CODE_404_NO_MATCHES:
                        ((MainActivity) getActivity()).setFragmentWithBackStack(ProfilePhotoDetailFragment.newInstance(new ArrayList<FanImageModel>(), null, null), null);
                        break;
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<DetailEventModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private Callback<String> followCallback = new Callback<String>() {
        @Override
        public void onResponse(Call<String> call, Response<String> response) {

        }


        @Override
        public void onFailure(Call<String> call, Throwable t) {

        }
    };


    private void initTypeFace() {
        Typeface typeface_medium = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        Typeface typeface_black = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_BLACK);
        profileFollowingsCount.setTypeface(typeface_black);
        profileFollowersCount.setTypeface(typeface_black);
        followBtnTitle.setTypeface(typeface_black);
        profileFollowingsTitle.setTypeface(typeface_medium);
        profileFollowersTitle.setTypeface(typeface_medium);
        profileUserName.setTypeface(typeface_black);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            profileFollowingsCount.setLetterSpacing(0.06f);
            profileFollowersCount.setLetterSpacing(0.06f);
            followBtnTitle.setLetterSpacing(0.06f);
            profileFollowingsTitle.setLetterSpacing(0.06f);
            profileFollowersTitle.setLetterSpacing(0.06f);
            profileUserName.setLetterSpacing(0.06f);
        }

        changeTabsFont();
    }


    private void changeTabsFont() {
        Typeface typeface_semibold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_SEMI_BOLD);
        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeface_semibold);
                    ((TextView) tabViewChild).setTextColor(ContextCompat.getColor(getContext(), R.color.dark_gray_color));
                }
            }
        }
    }


    private OnScrollChangeProfileListener scrollChangeProfileListener = new OnScrollChangeProfileListener() {
        @Override
        public void onScrollChangeProfile() {
            adjustProfileHeader();
        }
    };


    public interface OnScrollChangeProfileListener {
        void onScrollChangeProfile();
    }
}
