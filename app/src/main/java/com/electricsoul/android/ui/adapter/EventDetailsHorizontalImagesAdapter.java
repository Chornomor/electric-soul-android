package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.PromoterModel;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.model.UserResult;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.dialog.LoadingDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailsHorizontalImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private final int ITEM_PROMOTER = R.layout.item_event_photos;

    private final static int INITIAL_PAGE_NUMBER = 1;

    /******** data for promoters **********/
    private ArrayList<PromoterModel> promoters;
    private OnPromoterPhotoClick onPromoterPhotoClickListener;

    /******** data for users **************/
    private ArrayList<UserModel> users;
    private OnItemClickListener itemClickListener;
    private int totalUsersCount;

    /******** general data ****************/
    private Context context;
    private int resourceId;
    private String eventId;
    private int pageNumber = INITIAL_PAGE_NUMBER;
    private LoadingDialog loadingDialog;


    public EventDetailsHorizontalImagesAdapter(String eventId, Context context, int resourceId) {
        this.context = context;
        this.resourceId = resourceId;
        this.eventId = eventId;
    }


    public void setPromoters(ArrayList<PromoterModel> promoters) {
        this.promoters = promoters;
    }


    public ArrayList<PromoterModel> getPromoters() {
        return promoters;
    }


    public void setUsers(UserResult users) {
        this.users = users.getUsers();
        totalUsersCount = users.getCount();
    }


    public void addUser(UserModel user) {
        for(UserModel userModel : users) {
            if(userModel.getId().equals(user.getId())) {
                return;
            }
        }
        users.add(user);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        if (resourceId == ITEM_PROMOTER) {
            return promoters.size();
        } else {
            return users.size();
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(resourceId, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (resourceId == ITEM_PROMOTER) {
            initPromoters(viewHolder, position);
        } else {
            initPeopleWhosGoing(viewHolder, position);
        }
    }


    private void initPromoters(RecyclerView.ViewHolder viewHolder, final int position) {
        PromoterModel promoter = promoters.get(position);
        viewHolder.itemView.setTag(position);
        if (promoter.hasImageThumbnail()) {
            Picasso.with(context).load(promoter.getImage().getThumbnail()).placeholder(R.drawable.ic_event_empty).fit().centerCrop()
                    .into(((ItemViewHolder) viewHolder).imageView);
        }
        ((ItemViewHolder) viewHolder).divider.setVisibility(View.VISIBLE);
        if (position == promoters.size() - 1) {
            ((ItemViewHolder) viewHolder).divider.setVisibility(View.GONE);
        }
        ((ItemViewHolder) viewHolder).imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPromoterPhotoClickListener.onPromoterClick(position);
            }
        });
    }


    private void initPeopleWhosGoing(RecyclerView.ViewHolder viewHolder, int position) {
        UserModel user = users.get(position);
        viewHolder.itemView.setTag(position);
        if (user.hasImageThumbnail()) {
            Picasso.with(context).load(user.getImage().getThumbnail()).placeholder(R.drawable.avatar).fit().centerCrop().into(((ItemViewHolder) viewHolder).imageView);
        }
        ((ItemViewHolder) viewHolder).divider.setVisibility(View.VISIBLE);
        if (position == users.size() - 1) {
            ((ItemViewHolder) viewHolder).divider.setVisibility(View.GONE);
        }
        if (users.size() == position + 1 && position + 1 < totalUsersCount) {
            uploadMoreUsers();
        }
    }


    public void setPromoterPhotosClickListener(OnPromoterPhotoClick listener) {
        onPromoterPhotoClickListener = listener;
    }


    public interface OnPromoterPhotoClick {
        void onPromoterClick(int position);
    }


    @Override
    public void onClick(View view) {
        int position = Integer.parseInt(String.valueOf(view.getTag()));
        itemClickListener.onUserItemClick(view, position, users.get(position));
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView imageView;

        @BindView(R.id.divider)
        View divider;

        public ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.itemView.setOnClickListener(EventDetailsHorizontalImagesAdapter.this);
        }
    }


    public void setOnUsersItemClick(OnItemClickListener listener) {
        this.itemClickListener = listener;
    }


    public interface OnItemClickListener {
        void onUserItemClick(View v, int position, UserModel user);
    }


    private void showProgressDialog() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(context);
        }
        loadingDialog.showProgressDialog();
    }


    private void cancelProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.cancelProgressDialog();
        }
    }


    /*************************** users pagination *************************/
    private void uploadMoreUsers() {
        showProgressDialog();
        RESTClient.getInstance().getUsersAttendingToEventById(eventId, ++pageNumber, getUsersImagesByIdCallback);
    }


    private Callback<UserResult> getUsersImagesByIdCallback = new Callback<UserResult>() {

        @Override
        public void onResponse(Call<UserResult> call, Response<UserResult> response) {
            if (response.body() != null) {
                users.addAll(response.body().getUsers());
                notifyDataSetChanged();
                cancelProgressDialog();
            } else {
                cancelProgressDialog();
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (context != null) {
                            ((MainActivity) context).showErrorView(context.getString(R.string.error_title_sorry), context.getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (context != null) {
                            ((MainActivity) context).showErrorView(context.getString(R.string.error_title_error), context.getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<UserResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };
}
