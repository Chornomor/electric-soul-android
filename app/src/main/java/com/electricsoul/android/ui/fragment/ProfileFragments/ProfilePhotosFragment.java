package com.electricsoul.android.ui.fragment.ProfileFragments;

import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.EventResultModel;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.CacheFragmentStatePagerAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.fragment.ProfileFragment;
import com.electricsoul.android.ui.views.ProfilePhotoFrameLayout;
import com.electricsoul.android.ui.views.WrapContentHeightViewPager;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.PixelUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by denisshovhenia on 09.11.16.
 */

public class ProfilePhotosFragment extends BaseFragment {

    private final static int TAB_ALL_PHOTOS = 0;
    private final static int TAB_ALBUMS = 1;

    @BindView(R.id.photos_tabs)
    TabLayout tabs;

    @BindView(R.id.view_pager)
    WrapContentHeightViewPager viewPager;

    private boolean withBackButton;

    private ProfilePagerAdapter mPagerAdapter;

    private ProfileFragment.OnScrollChangeProfileListener scrollChangeProfileListener;

    private MainActivity mainActivity;

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            viewPager.setCurrentItem(tab.getPosition());
        }


        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }


        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };


    public static ProfilePhotosFragment newInstance(boolean withBackButton, ProfileFragment.OnScrollChangeProfileListener scrollChangeProfileListener) {
        ProfilePhotosFragment instance = new ProfilePhotosFragment();
        instance.withBackButton = withBackButton;
        instance.scrollChangeProfileListener = scrollChangeProfileListener;
        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile_photos, container, false);
        ButterKnife.bind(this, root);
        mainActivity = ((MainActivity) getActivity());

        setPagerHeight();

        initTabs();

        mPagerAdapter = new ProfilePhotosFragment.ProfilePagerAdapter(getChildFragmentManager(), tabs.getTabCount());
        viewPager.setAdapter(mPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        viewPager.setPagingEnabled(false);

        KeyboardUtil.hideKeyboard(mainActivity);
        return root;
    }


    private void initTabs() {
        tabs.addTab(tabs.newTab().setText(getContext().getString(R.string.all_photos_tab)));
        tabs.addTab(tabs.newTab().setText(getContext().getString(R.string.photos_album_tab)));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.addOnTabSelectedListener(tabSelectedListener);
    }


    @Override
    public void onResume() {
        super.onResume();
        mainActivity.showBottomToolbar();
    }


    @Override
    public void onPause() {
        super.onPause();
    }


    public void updatePhotosList(EventResultModel eventResultModel, ArrayList<FanImageModel> fanImages, ProfileAllPhotosFragment.OnProfilePhotoClick listener) {
        try {
            ((ProfileAllPhotosFragment) mPagerAdapter.getItemAt(0)).updatePhotosList(fanImages);
            ((ProfileAllPhotosFragment) mPagerAdapter.getItemAt(0)).setOnPhotoClickListener(listener);
            ((ProfileAlbumsFragment) mPagerAdapter.getItemAt(1)).updatePhotosList(eventResultModel.getEventModelListWithPhotos());
            ((ProfileAlbumsFragment) mPagerAdapter.getItemAt(1)).setOnPhotoClickListener(listener);
        } catch (NullPointerException e) {

        }
    }


    private void setPagerHeight() {
        Display display = mainActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        int pagerHeight = size.y - actionBarHeight - actionBarHeight - (int) PixelUtil.dpToPx(80, getContext());
        viewPager.measureCurrentHeight(pagerHeight);
    }




    public class ProfilePagerAdapter extends CacheFragmentStatePagerAdapter {
        int numOfTabs;


        ProfilePagerAdapter(FragmentManager fm, int numOfTabs) {
            super(fm);
            this.numOfTabs = numOfTabs;
        }


        @Override
        protected Fragment createItem(int position) {
            switch (position) {
                case TAB_ALL_PHOTOS:
                    return ProfileAllPhotosFragment.newInstance(true, withBackButton, scrollChangeProfileListener);
                case TAB_ALBUMS:
                    return ProfileAlbumsFragment.newInstance(true, withBackButton, scrollChangeProfileListener);
                default:
                    return null;
            }
        }


        @Override
        public int getCount() {
            return numOfTabs;
        }
    }


    private void initTypeFace() {
//        changeTabsFont();
    }


    private void changeTabsFont() {
        Typeface typefaceSemibold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_SEMI_BOLD);
        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typefaceSemibold);
                    ((TextView) tabViewChild).setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
                }
            }
        }
    }
}
