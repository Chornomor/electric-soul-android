package com.electricsoul.android.ui.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by chornomor on 11.12.2016.
 */

public class WrapContentHeightViewPager extends ViewPager {

    int currentHeight;

    private boolean isPagingEnabled = true;


    public WrapContentHeightViewPager(Context context) {
        super(context);
    }


    public WrapContentHeightViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public void measureCurrentHeight(int height) {
        currentHeight = height;
        requestLayout();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (currentHeight == 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(currentHeight, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }


//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        if (this.isPagingEnabled) {
//            return super.onTouchEvent(event);
//        }
//
//        return false;
//    }
//
//
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent event) {
//        if (this.isPagingEnabled) {
//            return super.onInterceptTouchEvent(event);
//        }
//
//        return false;
//    }


    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }


    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        if (v != this && v instanceof ViewPager) {
            return false;
        }
        return super.canScroll(v, checkV, dx, x, y);
    }
}
