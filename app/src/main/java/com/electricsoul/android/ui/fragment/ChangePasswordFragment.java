package com.electricsoul.android.ui.fragment;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.Regex;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denisshovhenia on 25.11.16.
 */
public class ChangePasswordFragment extends BaseFragment {

    public static final String USER_ID_EXTRA = "com.electricsoul.android.ui.fragment.USER_ID_EXTRA";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView title;

    @BindView(R.id.toolbar_option)
    TextView saveOption;

    @BindView(R.id.tv_old_password)
    EditText tvOldPassword;

    @BindView(R.id.tv_new_password)
    EditText tvNewPassword;

    @BindView(R.id.tv_confirm_password)
    EditText tvConfirmPassword;

    @BindView(R.id.tv_forgot_your_password)
    TextView tvForgotYourPassword;

    @BindView(R.id.root_layout)
    LinearLayout rootLayout;

    private String userAuthToken = "";


    public static ChangePasswordFragment newInstance() {
        return new ChangePasswordFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this, root);
        userAuthToken = SharedPrefManager.getInstance(getActivity()).getStoredAuthToken();

        addToolbarBackArrow(toolbar);

        setOnClickListeners();

        initFonts();

        return root;
    }


    @Override
    public void onPause() {
        super.onPause();
        cancelProgressDialog();
    }


    private void initFonts() {
        Typeface typeface_extrabold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_SEMI_BOLD);
        FontUtil.impl(rootLayout, typeface_extrabold);

        Typeface typeface_bold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_EXTRA_BOLD);
        title.setTypeface(typeface_bold);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            title.setLetterSpacing(0.06f);
        }
    }


    private void setOnClickListeners() {
        saveOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickUnlock()) {
                    showProgressDialog();
                    KeyboardUtil.hideKeyboard(getActivity());
                    sendNewPassword();
                }
            }
        });

        tvConfirmPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                    if (isClickUnlock()) {
                        showProgressDialog();
                        sendNewPassword();
                    }
                }
                return false;
            }
        });

        tvForgotYourPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickUnlock()) {
                    ((MainActivity) getActivity()).setFragmentWithBackStack(ForgotPasswordFragment.getInstance(), null);
                }
            }
        });
    }


    private void sendNewPassword() {
        if (isValidPasswords()) {
            RESTClient.getInstance().postUpdatePassword(userAuthToken,
                    tvOldPassword.getText().toString(),
                    tvNewPassword.getText().toString(),
                    tvConfirmPassword.getText().toString(),
                    new Callback<JSONObject>() {
                        @Override
                        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                            if (getActivity() != null) {
                                if (response.code() == ConstantsAPI.CODE_200_SUCCESS) {
                                    /*we use try/catch for prevent crash if app was closed after start updating password*/
                                    try {
                                        Toast.makeText(getActivity(), R.string.password_changed_successfully, Toast.LENGTH_SHORT).show();
                                        SharedPrefManager.getInstance(getActivity()).storePassword(tvConfirmPassword.getText().toString());
                                        KeyboardUtil.hideKeyboard(getActivity());
                                        cancelProgressDialog();
                                        getActivity().onBackPressed();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    cancelProgressDialog();
                                    Toast.makeText(getActivity(), getString(R.string.incorrect_password_or_password_mismatch), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }


                        @Override
                        public void onFailure(Call<JSONObject> call, Throwable t) {
                            cancelProgressDialog();
                            Toast.makeText(getActivity(), getString(R.string.incorrect_password_or_password_mismatch), Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            cancelProgressDialog();
        }
    }


    private boolean isValidPasswords() {
        return (isValidOldPassword() && isValidNewPassword() && isValidConfirmNewPassword());
    }


    private boolean isValidOldPassword() {
        String errorMsg = null;
        if (Regex.isEmpty(tvOldPassword.getText().toString())) {
            errorMsg = getString(R.string.provide_your_password);
        } else if (!isPasswordValid(tvOldPassword.getText().toString())) {
            errorMsg = getString(R.string.invalid_password_try_again);
        }

        if (errorMsg != null) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();
            tvOldPassword.requestFocus();
            return false;
        }
        return true;
    }


    private boolean isPasswordValid(String password) {
        return password.length() > 7;
    }


    private boolean isValidNewPassword() {
        String errorMsg = null;
        if (Regex.isEmpty(tvNewPassword.getText().toString())) {
            errorMsg = getString(R.string.provide_your_password);
        } else if (!isPasswordValid(tvNewPassword.getText().toString())) {
            errorMsg = getString(R.string.invalid_password_try_again);
        } else if (tvNewPassword.getText().toString().trim().contentEquals(
                tvOldPassword.getText().toString().trim())) {
            errorMsg = getString(R.string.passwords_must_not_be_matched);
        }

        if (errorMsg != null) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();
            tvNewPassword.requestFocus();
            return false;
        }
        return true;
    }


    private boolean isValidConfirmNewPassword() {
        String errorMsg = null;
        if (Regex.isEmpty(tvConfirmPassword.getText().toString())) {
            errorMsg = getString(R.string.provide_your_password);
        } else if (!tvConfirmPassword.getText().toString().trim().contentEquals(
                tvNewPassword.getText().toString().trim())) {
            errorMsg = getString(R.string.passwords_do_not_match);
        }

        if (errorMsg != null) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();
            tvConfirmPassword.requestFocus();
            return false;
        }
        return true;
    }

}
