package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.DetailEventModel;
import com.electricsoul.android.utils.DateUtil;
import com.electricsoul.android.utils.FontUtil;
import com.squareup.picasso.Picasso;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by denisshovhenia on 10.11.16.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private ClicksListener mClickListener;
    private List<DetailEventModel> mItemList = new ArrayList<>();
    private Context mContext;
    private Typeface medium, bold;

    private boolean isFestivals;


    public EventsAdapter(Context context, List<DetailEventModel> eventsList, boolean isFestivals) {
        mContext = context;
        this.mItemList = eventsList;
        this.isFestivals = isFestivals;
        medium = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        bold = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_KHULA_BOLD);
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_event, parent, false);

        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final DetailEventModel item = mItemList.get(position);
        holder.title.setText(item.getName());
        holder.place.setText(item.getAddress().getAddress1());
        holder.date.setText(DateUtil.convertTime(item.getStart()) + " - " + DateUtil.convertTime(item.getEnd()));

        holder.place.setTypeface(medium);
        holder.date.setTypeface(medium);
        holder.title.setTypeface(bold);

        if (isFestivals) {
            holder.eventIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fest_icon_red));
        }

        Picasso.with(mContext)
                .load(item.getCoverImage().getThumbnail()).fit().centerCrop()
                .into(holder.mainImage);

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int position = Integer.parseInt(String.valueOf(holder.itemView.getTag()));
                if (mClickListener != null) {
                    mClickListener.onItemClick(holder.itemView, position, mItemList.get(position));
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mItemList.size();
    }


    public void updateItems(ArrayList<DetailEventModel> usersList) {
        mItemList = usersList;
        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.event_icon)
        ImageView eventIcon;

        @BindView(R.id.main_image)
        ImageView mainImage;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.place)
        TextView place;

        @BindView(R.id.date)
        TextView date;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public void setClickListener(ClicksListener listener) {
        mClickListener = listener;
    }


    public interface ClicksListener {
        void onItemClick(View view, int position, DetailEventModel model);
    }

}