package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.FollowerResult;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.utils.FontUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denisshovhenia on 10.11.16.
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private static final String FOLLOW_KEY               = "1";
    private static final String NOT_FOLLOW_KEY           = "0";
    private static final String MUTUAL_FOLLOW_TAG        = "mutual";
    private static final String NOT_MUTUAL_FOLLOW_TAG    = "not_mutual";

    private ClicksListener mClickListener;
    private List<UserModel> mItemList = new ArrayList<>();
    private List<String> followerList = new ArrayList<>();
    private List<String> followingList = new ArrayList<>();

    private Context mContext;
    private MainActivity mMainActivity;
    private Typeface medium;
    private String currentUserId;


    public UsersAdapter(Context context, List<UserModel> itemList) {
        mContext = context;
        mItemList = itemList;
        mMainActivity = (MainActivity) mContext;
        medium = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        currentUserId = SharedPrefManager.getInstance(context).getStoredUserId();
        if (mMainActivity.isUserAuthorized()) {
            initFollowersList();
            initFollowingsList();
        }
        notifyDataSetChanged();
    }


    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_user, parent, false);

        return new UsersAdapter.ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final UserModel item = mItemList.get(position);
        holder.userName.setText(item.getUserName());
        holder.userName.setTypeface(medium);
        if (item.hasImageThumbnail()) {
            Picasso.with(mContext)
                    .load(item.getImage().getThumbnail()).fit().centerCrop()
                    .into(holder.userPhoto);
        }
        if (currentUserId.equals(item.getId())) {
            holder.friendshipIcon.setVisibility(View.INVISIBLE);
            holder.folliwingStatus.setVisibility(View.GONE);
        } else {
            holder.friendshipIcon.setVisibility(View.VISIBLE);
            holder.folliwingStatus.setVisibility(View.VISIBLE);
            setupIsFollowing(holder.folliwingStatus, item);
        }
        holder.folliwingStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currentUserId = SharedPrefManager.getInstance(mContext).getStoredUserId();
                if (!currentUserId.isEmpty()) {
                    String followKey;
                    if (!isFollowing(item)) {
                        followKey = FOLLOW_KEY;
                        setDrawable(holder.folliwingStatus, R.drawable.ic_follow_minus_red_icon);
                        followingList.add(item.getId());
                        mMainActivity.addToFollowings(item);
                    } else {
                        followKey = NOT_FOLLOW_KEY;
                        setDrawable(holder.folliwingStatus, R.drawable.ic_plus_btn);
                        followingList.remove(item.getId());
                        mMainActivity.removeFromFollowings(item);
                    }
                    RESTClient.getInstance().postFollowUser(SharedPrefManager.getInstance(mContext).getStoredAuthToken(), followKey, item.getId(), currentUserId, followCallback);
                    isMutualFollow(holder, item);
                } else {
                    Toast.makeText(mContext, R.string.error_login_to_follow, Toast.LENGTH_SHORT).show();
                }
            }
        });

        isMutualFollow(holder, item);
        holder.userModel = item;
        holder.itemView.setTag(holder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ViewHolder viewHolder = (ViewHolder) v.getTag();
                mClickListener.onItemClick(v, viewHolder.getAdapterPosition(), viewHolder.userModel);
            }
        });
    }


    public void initFollowingsList() {
        if(mMainActivity.followings != null) {
            followingList = prepareFollowsIdList(mMainActivity.followings.getResults());
        }
    }


    private void initFollowersList() {
        if(mMainActivity.followers != null) {
            followerList = prepareFollowsIdList(mMainActivity.followers.getResults());
        }
    }


    private boolean isFollowing(UserModel item) {
        return followingList.contains(item.getId());
    }


    private boolean isFollower(UserModel item) {
        return followerList.contains(item.getId());
    }


    private void setupIsFollowing(ImageView view, UserModel item) {
        if (isFollowing(item)) {
            setDrawable(view, R.drawable.ic_follow_minus_red_icon);
        } else {
            setDrawable(view, R.drawable.ic_plus_btn);
        }
    }


    private void isMutualFollow(ViewHolder holder, UserModel item) {
        if (isFollower(item) && isFollowing(item)) {
            setDrawable(holder.friendshipIcon, R.drawable.ic_follow_hand_selected_icon);
            holder.friendshipIcon.setTag(MUTUAL_FOLLOW_TAG);
        } else {
            setDrawable(holder.friendshipIcon, R.drawable.ic_follow_hand_unselected_icon);
            holder.friendshipIcon.setTag(NOT_MUTUAL_FOLLOW_TAG);
        }
    }


    private void setDrawable(ImageView imgView, int resourceId) {
        imgView.setImageDrawable(ContextCompat.getDrawable(mContext, resourceId));
    }


    @Override
    public int getItemCount() {
        return mItemList.size();
    }


    public void updateItems(ArrayList<UserModel> usersList) {
        mItemList = usersList;
        notifyDataSetChanged();
    }


    private ArrayList<String> prepareFollowsIdList(ArrayList<FollowerResult.Follower> followerList) {
        ArrayList<String> followsIds = new ArrayList<>();
        for (int i = 0; i < followerList.size(); ++i) {
            followsIds.add(followerList.get(i).getId());
        }
        return followsIds;
    }


    private Callback<String> followCallback = new Callback<String>() {
        @Override
        public void onResponse(Call<String> call, Response<String> response) {
        }


        @Override
        public void onFailure(Call<String> call, Throwable t) {

        }
    };


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_photo_image)
        ImageView userPhoto;

        @BindView(R.id.plus_minus_icon)
        ImageView folliwingStatus;

        @BindView(R.id.friendship_icon)
        ImageView friendshipIcon;

        @BindView(R.id.user_name_text)
        TextView userName;

        UserModel userModel;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public void setClickListener(ClicksListener listener) {
        mClickListener = listener;
    }


    public interface ClicksListener {
        void onItemClick(View view, int position, UserModel user);
    }

}