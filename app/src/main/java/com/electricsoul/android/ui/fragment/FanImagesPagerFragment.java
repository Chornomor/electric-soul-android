package com.electricsoul.android.ui.fragment;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.model.Likes.FanImageLike;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.ui.dialog.DialogManager;
import com.electricsoul.android.utils.AnimateView;
import com.electricsoul.android.utils.DoubleClickListener;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.PhotoUtil;
import com.github.mmin18.widget.RealtimeBlurView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by alexchern on 30.11.16.
 */

public class FanImagesPagerFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_promoter_details_name)
    TextView tvEventName;

    @BindView(R.id.tv_promoter_likes)
    TextView tvPromoterLikes;

    @BindView(R.id.iv_promoter_like)
    ImageView ivPromoterLike;

    @BindView(R.id.promoted_pager)
    ViewPager pager;

    @BindView(R.id.tv_current_position)
    TextView tvCurrentPosition;

    @BindView(R.id.tv_all_positions)
    TextView tvAllPositions;

    @BindView(R.id.iv_zoom_image)
    ImageView ivZoomImage;

    @BindView(R.id.zoom_layout)
    FrameLayout zoomLayout;

    @BindView(R.id.hidden_like)
    LinearLayout hiddenLike;

    @BindView(R.id.iv_hidden_like)
    ImageView ivHiddenLike;


    private PhotoViewAttacher mAttacher;

    private ArrayList<FanImageModel> fanImageList;
    private int position;
    private String eventName;
    private ImagePagerAdapter imagePagerAdapter;
    private MainActivity mainActivity;
    private static final String LIKE_TAG = "like";
    private static final String UNLIKE_TAG = "unlike";
    private Typeface bold;


    public static FanImagesPagerFragment newInstance(ArrayList<FanImageModel> fanImageList, int position, String name) {
        FanImagesPagerFragment instance = new FanImagesPagerFragment();
        instance.fanImageList = fanImageList;
        instance.position = position;
        instance.eventName = name;
        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_promoted_image, container, false);
        ButterKnife.bind(this, rootView);

        mainActivity = (MainActivity) getActivity();
        tvAllPositions.setText(String.valueOf(fanImageList.size()));
        tvCurrentPosition.setText(String.valueOf(position + 1));
        tvEventName.setText(eventName);
        mAttacher = new PhotoViewAttacher(ivZoomImage);
        setPager();
        addToolbarBackArrow(toolbar);
        initFonts();
        return rootView;
    }


    private void initFonts() {
        bold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_KHULA_BOLD);
        tvEventName.setTypeface(bold);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        cancelProgressDialog();
    }


    private void setPager() {
        imagePagerAdapter = new ImagePagerAdapter(new OnImageClickForFragment() {
            @Override
            public void onImageFragmentClick(final String image) {
                Picasso.with(getActivity()).load(image).into(ivZoomImage);
                mAttacher.update();
                zoomLayout.setVisibility(View.VISIBLE);
                mAttacher.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        DialogManager.showConfirmDialog(getContext(), R.string.save_option, R.string.save_photo_message, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                PhotoUtil.imageDownload(FanImagesPagerFragment.this, getActivity(), image);
                            }
                        }, null);
                        return false;
                    }
                });
                try {
                    ((MainActivity) getActivity()).hideBottomToolbar();
                } catch (NullPointerException e) {
                }
            }


            @Override
            public void onImageFragmentDoubleClick() {
                onLikeClick();
            }
        });

        pager.setAdapter(imagePagerAdapter);
        pager.setCurrentItem(position, true);
        getLikes(position);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }


            @Override
            public void onPageSelected(int position) {
                tvCurrentPosition.setText(String.valueOf(position + 1));
                getLikes(position);
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void getLikes(int position) {
        if (mainActivity.isNetworkAvailable()) {
            RESTClient.getInstance().getFanImageLikesByFanImageId(
                    SharedPrefManager.getInstance(getActivity()).getStoredAuthToken(),
                    fanImageList.get(position).getId(),
                    new Callback<FanImageLike>() {
                        @Override
                        public void onResponse(Call<FanImageLike> call, Response<FanImageLike> response) {
                            if (response.body() == null) {
                                tvPromoterLikes.setText("");
                                ivPromoterLike.setTag(UNLIKE_TAG);
                                cancelProgressDialog();
                                return;
                            } else {
                                tvPromoterLikes.setText(String.valueOf(response.body().getCount()));
                            }

                            if (response.body().getFanImageIsLike().equals("1")) {
                                ivPromoterLike.setImageResource(R.drawable.lightening_like);
                                ivPromoterLike.setTag(LIKE_TAG);
                            } else {
                                ivPromoterLike.setImageResource(R.drawable.lighting_unlike);
                                ivPromoterLike.setTag(UNLIKE_TAG);
                            }
                            cancelProgressDialog();
                        }


                        @Override
                        public void onFailure(Call<FanImageLike> call, Throwable t) {
                            cancelProgressDialog();
                        }
                    });
        } else {
            cancelProgressDialog();
        }
    }


    private class ImagePagerAdapter extends PagerAdapter {

        private OnImageClickForFragment fragmentListener;


        private ImagePagerAdapter(OnImageClickForFragment fragmentListener) {
            this.fragmentListener = fragmentListener;
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_promoter_pager_item, null);

            fillView(view, position);

            container.addView(view, 0);
            return view;
        }


        private void fillView(View view, int position) {
            RealtimeBlurView blurView = (RealtimeBlurView) view.findViewById(R.id.blur_view);
            ImageView blurImage = (ImageView) view.findViewById(R.id.iv_blur_image);
            setBlurImage(blurView, blurImage, position);
            ImageView clearImage = (ImageView) view.findViewById(R.id.iv_clear_image);
            setClearImage(clearImage, position);
            setImageClickListener(clearImage, position);
        }


        private void setBlurImage(RealtimeBlurView blurView, ImageView blurImage, int position) {
            blurView.setBlurRadius(25f);
            blurView.setOverlayColor(R.color.black);
            Picasso.with(getActivity()).load(fanImageList.get(position).getImage().getThumbnail())
                    .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE).into(blurImage);
        }


        private void setClearImage(ImageView clearImage, int position) {
            Picasso.with(getActivity()).load(fanImageList.get(position).getImage().getFullSize()).into(clearImage);
        }


        boolean isDoubleTap = false;


        private void setImageClickListener(final ImageView clearImage, final int position) {
            clearImage.setOnClickListener(new DoubleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    isDoubleTap = false;
                    callClickOptions(clearImage, position);
                }


                @Override
                public void onDoubleClick(View v) {
                    isDoubleTap = true;
                }
            });

        }


        private void callClickOptions(ImageView clearImage, final int position) {
            clearImage.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isDoubleTap) {
                        fragmentListener.onImageFragmentDoubleClick();
                    } else {
                        fragmentListener.onImageFragmentClick(fanImageList.get(position).getImage().getFullSize());
                    }
                }
            }, 300);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }


        @Override
        public int getCount() {
            return fanImageList.size();
        }
    }


    @OnClick(R.id.iv_back_zoom)
    public void onClickBackFromZoom() {
        zoomLayout.setVisibility(View.GONE);
        ((MainActivity) getActivity()).showBottomToolbar();
    }


    @OnClick(R.id.iv_promoter_like)
    void onLikeClick() {
        if (getActivity() != null && !((MainActivity) getActivity()).isUserAuthorized()) {
            ((MainActivity) getActivity()).enableTab(MainActivity.PROFILE_TAB);
        }
        String imageId = fanImageList.get(pager.getCurrentItem()).getId();
        String userId = SharedPrefManager.getInstance(getContext()).getStoredUserId();
        int isLike = -1;
        if (ivPromoterLike.getTag() != null && ivPromoterLike.getTag().equals(LIKE_TAG)) {
            isLike = 0;
        } else {
            isLike = 1;
        }
        final int finalIsLike = isLike;
        if (mainActivity.isNetworkAvailable()) {
            RESTClient.getInstance().postFanImageLike(imageId, userId, String.valueOf(isLike),
                    new Callback<JSONObject>() {
                        @Override
                        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                            AnimateView animation = new AnimateView(getActivity(), hiddenLike);
                            if (finalIsLike == 1) {
                                ivHiddenLike.setImageDrawable(getResources().getDrawable(R.drawable.white_anim_like));
                                animation.likeAnimation();
                                ivPromoterLike.setImageResource(R.drawable.lightening_like);
                                ivPromoterLike.setTag(LIKE_TAG);
                            } else {
                                ivHiddenLike.setImageDrawable(getResources().getDrawable(R.drawable.white_anim_unlike));
                                animation.likeAnimation();
                                ivPromoterLike.setImageResource(R.drawable.lighting_unlike);
                                ivPromoterLike.setTag(UNLIKE_TAG);
                            }
                            getLikes(pager.getCurrentItem());
                        }


                        @Override
                        public void onFailure(Call<JSONObject> call, Throwable t) {
                            cancelProgressDialog();
                        }
                    }
            );
        }
        cancelProgressDialog();
    }


    public interface OnImageClickForFragment {
        void onImageFragmentClick(String image);


        void onImageFragmentDoubleClick();
    }

}
