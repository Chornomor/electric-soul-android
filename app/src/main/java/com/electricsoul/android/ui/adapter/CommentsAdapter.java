package com.electricsoul.android.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.model.Comment;
import com.electricsoul.android.network.NetworkManager;
import com.electricsoul.android.utils.FontUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private ClicksListener mClickListener;
    private ArrayList<Comment> mItemList = new ArrayList<>();
    private Context mContext;
    private Typeface medium, bold;
    private int checkedPosition = -1;
    private String TAG_FULL  = "full";
    private String TAG_EMPTY = "empty";

    public CommentsAdapter(Context context) {
        mContext = context;
        medium = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        bold = Typeface.createFromAsset(context.getAssets(), FontUtil.FONT_RALEWAY_EXTRA_BOLD);
        notifyDataSetChanged();
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comment, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CommentsAdapter.ViewHolder holder, final int position) {
        final Comment item = mItemList.get(position);
        if(item.getUserImage() != null && !item.getUserImage().isEmpty()) {
            Picasso.with(mContext).load(item.getUserImage()).fit().centerCrop().into(holder.userPhoto);
        }

        holder.userName.setText(item.getUserName());
        holder.userComment.setText(item.getComment());
        holder.userName.setTypeface(bold);
        holder.userComment.setTypeface(medium);
        holder.tvLikes.setTypeface(medium);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.userName.setLetterSpacing(0.06f);
            holder.userComment.setLetterSpacing(0.06f);
        }

        holder.itemView.setTag(holder);
        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setCheckedPosition(position);
                mClickListener.onItemClick(item.getUserName(), item.getUserId());
            }
        });

        holder.tvLikes.setText(item.getLikeCount() + "");
        if(item.isLike()) {
            setDrawable(holder.iv_likes, R.drawable.events_comment_full_icon);
            holder.iv_likes.setTag(TAG_FULL);
        } else {
            setDrawable(holder.iv_likes, R.drawable.events_comment_empty_icon);
            holder.iv_likes.setTag(TAG_EMPTY);
        }

        holder.iv_likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkManager.isNetworkAvailable(mContext)) {
                    if (holder.iv_likes.getTag() == null)
                        holder.iv_likes.setTag(TAG_EMPTY);
                    if (holder.iv_likes.getTag().equals(TAG_EMPTY)) {
                        mItemList.get(position).setLike(true);
                        mItemList.get(position).setLikeCount(mItemList.get(position).getLikeCount() + 1);
                        holder.tvLikes.setText((Integer.parseInt(holder.tvLikes.getText().toString()) + 1) + "");
                        setDrawable(holder.iv_likes, R.drawable.events_comment_full_icon);
                        holder.iv_likes.setTag(TAG_FULL);
                    } else {
                        mItemList.get(position).setLike(false);
                        mItemList.get(position).setLikeCount(mItemList.get(position).getLikeCount() - 1);
                        holder.tvLikes.setText((Integer.parseInt(holder.tvLikes.getText().toString()) - 1) + "");
                        setDrawable(holder.iv_likes, R.drawable.events_comment_empty_icon);
                        holder.iv_likes.setTag(TAG_EMPTY);
                    }
                }
                mClickListener.onLikesClick(item.getCommentId());
            }
        });
        if (checkedPosition == position) {
            holder.bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.gray_light_color));
        } else  {
            holder.bg.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.white));
        }

        holder.userPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               mClickListener.onPhotoClick(item.getUserId());
            }
        });

        holder.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onPhotoClick(item.getUserId());
            }
        });
    }

    private void setCheckedPosition(int position) {
        checkedPosition = position;
        notifyDataSetChanged();
    }

    public void clearCheckedItem() {
        checkedPosition = -1;
        notifyDataSetChanged();
    }


    public void updateComments(ArrayList<Comment> mItemList) {
        this.mItemList = mItemList;
        notifyDataSetChanged();
    }


    public void setDrawable(ImageView imgView, int resourceId) {
        imgView.setImageDrawable(ContextCompat.getDrawable(mContext, resourceId));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.comment_user_photo)
        ImageView userPhoto;
        @BindView(R.id.comment_user_name)
        TextView userName;
        @BindView(R.id.comment_text)
        TextView userComment;
        @BindView(R.id.comment_tv_likes)
        TextView tvLikes;
        @BindView(R.id.comment_btn_likes)
        ImageView iv_likes;
        @BindView(R.id.item_comment_bg)
        LinearLayout bg;
        @BindView(R.id.ll_item)
        LinearLayout ll_item;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setClickListener(ClicksListener listener) {
        mClickListener = listener;
    }

    public interface ClicksListener {
        void onItemClick(String name, String userId);
        void onLikesClick(String commentId);
        void onPhotoClick (String userId);
    }

}