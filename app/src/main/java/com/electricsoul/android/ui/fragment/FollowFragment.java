package com.electricsoul.android.ui.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.FollowerResult;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.adapter.FollowAdapter;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.electricsoul.android.R.id.et_search;

/**
 * Created by yuliasokolova on 18.11.16.
 */

public class FollowFragment extends BaseFragment implements FollowAdapter.OnItemClickListener, FollowAdapter.OnItemUpdateListener {

    @BindView(R.id.follow_recycle_view)
    RecyclerView rvFollow;

    @BindView(R.id.toolbar_title)
    TextView tvToolbarTitle;

    @BindView(R.id.iv_search_cancel_icon)
    ImageView ivCancel;

    @BindView(R.id.iv_search_icon)
    ImageView ivSearch;

    @BindView(et_search)
    EditText etSearch;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Context context;

    private ArrayList<FollowerResult.Follower> followUsers = new ArrayList<>();
    private ArrayList<UserModel> searchResults = new ArrayList<>();

    private FollowAdapter adapter;

    private boolean isSearchMode = false;
    private boolean isFollowingScreen;

    private String userId;
    private String currentUserId;

    private int totalCount;
    private int lastPageNumber = EventsFragment.FIRST_PAGE_NUMBER;


    public static FollowFragment getInstance(boolean isFollowing, String userId) {
        FollowFragment followFragment = new FollowFragment();
        followFragment.isFollowingScreen = isFollowing;
        followFragment.userId = userId;
        return followFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_follow, container, false);
        ButterKnife.bind(this, view);

        context = getActivity();
        currentUserId = SharedPrefManager.getInstance(context).getStoredUserId();
        initToolbar();
        initLayout();

//        if (followUsers.isEmpty()) {
        getData();
//        } else {
//            adapter.setTotalCount(totalCount);
//            adapter.updateItems(getFollowUsers());
//        }

        initSearch();

        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        KeyboardUtil.hideKeyboard(getActivity());
    }


    private void initToolbar() {
        addToolbarBackArrow(toolbar);

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSearchMode();
            }
        });
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
                KeyboardUtil.initFocusAndShowKeyboard(etSearch, context);
            }
        });
        if (isFollowingScreen) {
            tvToolbarTitle.setText(getResources().getString(R.string.followings_title));
        } else {
            tvToolbarTitle.setText(getResources().getString(R.string.followers_title));
        }
    }


    private void setReadMode() {
        isSearchMode = false;
        etSearch.setVisibility(View.GONE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        ivCancel.setVisibility(View.GONE);
        ivSearch.setVisibility(View.VISIBLE);
        adapter.updateItems(getFollowUsers());
        adapter.setOnSearchMode(false);
        if (getActivity() != null) {
            KeyboardUtil.hideKeyboard(getActivity());
        }
    }


    private void setSearchMode() {
        isSearchMode = true;
        etSearch.setVisibility(View.VISIBLE);
        etSearch.setText("");
        tvToolbarTitle.setVisibility(View.GONE);
        ivCancel.setVisibility(View.GONE);
        ivSearch.setVisibility(View.VISIBLE);
        adapter.setOnSearchMode(true);
        KeyboardUtil.initFocusAndShowKeyboard(etSearch, context);
    }


    private void initSearch() {
        etSearch.addTextChangedListener(new TextWatcher() {
            String searchFilter = "";


            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            @Override
            public void afterTextChanged(Editable editable) {
                searchFilter = getText();
                if (searchFilter.equals("")) {
                    ivSearch.setVisibility(View.VISIBLE);
                    ivCancel.setVisibility(View.GONE);
                } else {
                    ivCancel.setVisibility(View.VISIBLE);
                    ivSearch.setVisibility(View.GONE);
                }
                adapter.updateItems(getSearchResult(searchFilter));
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                KeyboardUtil.hideKeyboard(getActivity());
                return true;
            }
        });

    }


    private ArrayList<UserModel> getSearchResult(String filter) {
        ArrayList<UserModel> allUsers = getFollowUsers();
        searchResults.clear();
        for (UserModel user : allUsers) {
            if (user.getUserName().toLowerCase().contains(filter)) {
                searchResults.add(user);
            }
        }
        return searchResults;
    }


    private String getText() {
        return etSearch.getText().toString().trim().toLowerCase();
    }


    private void getData() {
        showProgressDialog();
        if (isFollowingScreen) {
            RESTClient.getInstance().getFollowingById(userId, lastPageNumber, followerResultCallback);
        } else {
            RESTClient.getInstance().getFollowersById(userId, lastPageNumber, followerResultCallback);
        }
    }


    private void initLayout() {
        ((MainActivity) getActivity()).hideBottomToolbar();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvFollow.setLayoutManager(mLayoutManager);
        rvFollow.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isSearchMode && etSearch.getText().toString().trim().equals("")) {
                    setReadMode();
                } else {
                    KeyboardUtil.hideKeyboard(getActivity());
                }
                return false;
            }
        });
        adapter = new FollowAdapter(getActivity(), userId);
        adapter.setFollowingScreen(isFollowingScreen);
        adapter.setOnItemClickListener(this);
        adapter.setOnItemUpdateListener(this);
        rvFollow.setAdapter(adapter);
        setToolbarTitleFont();
    }


    private void setToolbarTitleFont() {
        Typeface typeface_extrabold = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_EXTRA_BOLD);
        tvToolbarTitle.setTypeface(typeface_extrabold);
        etSearch.setTypeface(typeface_extrabold);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tvToolbarTitle.setLetterSpacing(0.06f);
            etSearch.setLetterSpacing(0.06f);
        }
    }


    private ArrayList<UserModel> getFollowUsers() {
        ArrayList<UserModel> users = new ArrayList<>();
        if (followUsers != null) {
            for (FollowerResult.Follower follower : followUsers) {
                UserModel user = new UserModel(follower);
                users.add(user);
            }
        }
        return users;
    }


    @Override
    public void onItemClick(View view, int position, UserModel user) {
        KeyboardUtil.hideKeyboard(getActivity());
        if (isClickUnlock()) {
            ((MainActivity) getActivity()).showBottomToolbar();
            ((MainActivity) getActivity()).setFragmentWithBackStack(ProfileFragment.newInstance(user.getId(), true), null);
        }
    }


    private void addNewUsers(ArrayList<FollowerResult.Follower> result) {
        boolean notExist = true;
        for (FollowerResult.Follower follower : result) {
            for (int i = (30 * (lastPageNumber - 1)) - 1; i < followUsers.size(); i++) {
                if (follower.getId().equals(followUsers.get(i).getId())) {
                    notExist = false;
                }
            }
            if (notExist) {
                followUsers.add(follower);
            }
            notExist = true;
        }
    }


    private Callback<FollowerResult> followerResultCallback = new Callback<FollowerResult>() {
        @Override
        public void onResponse(Call<FollowerResult> call, Response<FollowerResult> response) {
            if (response.body() != null) {
                if (lastPageNumber == EventsFragment.FIRST_PAGE_NUMBER) {
                    followUsers = response.body().getResults();
                } else {
                    addNewUsers(response.body().getResults());

                    if (userId.equals(currentUserId)) {
                        if (isFollowingScreen) {
                            ((MainActivity) getActivity()).addFollowingsList(followUsers);
                        } else {
                            ((MainActivity) getActivity()).addFollowersList(followUsers);
                        }
                        adapter.fillFollowLists();
                    }
                }
                totalCount = response.body().getCount();
                adapter.setTotalCount(totalCount);
                adapter.updateItems(getFollowUsers());
                setReadMode();
                cancelProgressDialog();
            } else {
                cancelProgressDialog();
                lastPageNumber--;
                switch (response.code()) {
                    case ConstantsAPI.CODE_429_SERVER_UPDATING:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                        }
                        break;
                    default:
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        }
                        break;
                }
            }
        }


        @Override
        public void onFailure(Call<FollowerResult> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    @Override
    public void onItemListUpdate() {
        lastPageNumber++;
        getData();
    }


    @Override
    public void onUnFollowed(UserModel user) {
        for (int i = 0; i < followUsers.size(); i++) {
            FollowerResult.Follower follower = followUsers.get(i);
            if (user.getId().equals(follower.getId())) {
                followUsers.remove(follower);
            }
        }
    }
}
