package com.electricsoul.android.ui.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.manager.FacebookManager;
import com.electricsoul.android.manager.MixpanelManager;
import com.electricsoul.android.manager.SharedPrefManager;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.LogUtil;
import com.electricsoul.android.utils.Regex;
import com.facebook.AccessToken;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yuliasokolova on 10.11.16.
 */

public class AuthFragment extends BaseFragment {

    private View root;

    public static final String AUTH_TAG = "AuthFragment";

    @BindView(R.id.tv_auth_subtitle)
    TextView tvSubtitle;

    @BindView(R.id.auth_title)
    TextView tvTitle;

    @BindView(R.id.btn_fb)
    TextView btnFb;

    @BindView(R.id.tv_bottom_panel)
    TextView tvBottom;

    @BindView(R.id.btn_sign_up_email)
    LinearLayout btnSignUpEmail;

    @BindView(R.id.btn_log_in_email)
    TextView btnLogInEmail;

    @BindView(R.id.login_et_email)
    EditText et_email;

    @BindView(R.id.login_et_password)
    EditText et_password;

    @BindView(R.id.login_forgot_password)
    TextView tv_forgot_pswd;

    @BindView(R.id.ll_font_root)
    LinearLayout layoutFontRoot;

    private boolean isLoggingIn;
    private String password, fireBaseToken;

    FacebookManager facebookManager;


    public static AuthFragment newInstance() {
        return new AuthFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_auth, container, false);
        ButterKnife.bind(this, root);

        fireBaseToken = FirebaseInstanceId.getInstance().getToken();
        LogUtil.logE("AuthFragment FireBase Token [" + fireBaseToken + "]");
        setupViews();
        initForgotPswd();
        KeyboardUtil.initFocusAndShowKeyboard(et_email, getActivity());

        initTypeFace();
        initSignUpWithEmail();
        initLogInWithEmail();
        isLoggingIn = false;
        facebookManager = FacebookManager.getInstance(getContext());
        return root;
    }


    private void initForgotPswd() {
        tv_forgot_pswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).setFragmentWithBackStack(ForgotPasswordFragment.getInstance(), null);
            }
        });
    }


    private void setupViews() {
        et_password.setOnEditorActionListener(keyboardDoneListener);
        et_email.setOnEditorActionListener(keyboardDoneListener);
    }


    private boolean isPasswordValid(String password) {
        if (password.length() < 8) {
            Toast.makeText(getActivity(), R.string.error_password_length, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void initLogInWithEmail() {
        btnLogInEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser();
            }
        });
    }


    private void initSignUpWithEmail() {
        btnSignUpEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickUnlock()) {
                    ((MainActivity) getActivity()).setFragmentWithBackStack(SignUpEmailFragment.getInstance("", ""), SignUpEmailFragment.TAG_SIGN_UP);
                }
            }
        });
    }


    private void initTypeFace() {
        Typeface typeface_black = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_BLACK);
        Typeface typeface_medium = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        FontUtil.impl(layoutFontRoot, typeface_medium);
        tvBottom.setTypeface(typeface_black);
        tvTitle.setTypeface(typeface_black);
    }


    @Override
    public void onResume() {
        super.onResume();
        KeyboardUtil.hideKeyboard(getActivity());
        initKeyboardListener(root);
    }


    @Override
    public void onPause() {
        super.onPause();
        cancelProgressDialog();
        removeGlobalLayoutListener();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookManager.onActivityResult(requestCode, resultCode, data);
    }


    @OnClick(R.id.btn_fb)
    public void onFacebookBtnClick() {
        if (!isLoggingIn) {
            if(isClickUnlock()) {
                isLoggingIn = true;
                facebookManager.login(getActivity(), new FacebookManager.LoginListener() {

                    @Override
                    public void onSuccess() {
                        showProgressDialog();
                        if (AccessToken.getCurrentAccessToken() != null) {
                            SharedPrefManager.getInstance(getActivity()).storeFbToken(AccessToken.getCurrentAccessToken().getToken());
                            RESTClient.getInstance().requestFacebookLogin(fireBaseToken, fbLoginCallback);
                        } else {
                            fbLoginCallback.onFailure(null, null);
                        }
                        isLoggingIn = false;
                    }


                    @Override
                    public void onError(FacebookManager.LoginError error) {
                        switch (error) {
                            case NOT_ALL_PERMISSIONS_GRANTED:
                                Toast.makeText(getContext(), R.string.facebook_login_fail_permissions, Toast.LENGTH_SHORT)
                                        .show();
                                break;
                            case CANCELED_BY_USER:
                                Toast.makeText(getContext(), R.string.facebook_login_cancelled, Toast.LENGTH_SHORT).show();
                                break;
                            case FACEBOOK_ERROR:
                                facebookManager.logout();
                                Toast.makeText(getContext(), R.string.social_login_failed, Toast.LENGTH_LONG).show();
                                break;
                        }
                        isLoggingIn = false;
                        cancelProgressDialog();
                    }
                });
            }
        }
    }


    private TextView.OnEditorActionListener keyboardDoneListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                loginUser();
            }
            return false;
        }
    };


    private void loginUser() {
        KeyboardUtil.hideKeyboard(getActivity());
        if (isClickUnlock() && ((MainActivity) getActivity()).isNetworkAvailable()) {
            String email = et_email.getText().toString();
            if (Regex.isValidEmail(email)) {
                password = et_password.getText().toString();
                if (isPasswordValid(password)) {
                    showProgressDialog();
                    RESTClient.getInstance().requestLogin(email, password, fireBaseToken, loginCallback);
                }
            } else {
                if (email.isEmpty()) {
                    Toast.makeText(getActivity(), R.string.signup_info_enter_email, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), R.string.error_invalid_email, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    Callback<UserModel> loginCallback = new Callback<UserModel>() {
        @Override
        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
            cancelProgressDialog();
            switch (response.code()) {
                case ConstantsAPI.CODE_200_SUCCESS:
                    storeLoginUserData(response.body());
                    trackFirebaseEvent(response.body().getUserName());
                    startActivity(new Intent(getActivity(), MainActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    break;
                case ConstantsAPI.CODE_400_BAD_REQUEST:
                    Toast.makeText(getActivity(), R.string.error_wrong_email_or_password, Toast.LENGTH_LONG).show();
                    break;
                case ConstantsAPI.CODE_403_VEREFIY_ACCOUNT:
                    RESTClient.getInstance().postResendEmailActivation(et_email.getText().toString(), resendActivationCallback);
                    Toast.makeText(getActivity(), R.string.error_confirm_email, Toast.LENGTH_SHORT).show();
                    break;
                case ConstantsAPI.CODE_429_SERVER_UPDATING:
                    if (getActivity() != null) {
                        ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                    }
                    break;
                default:
                    if (getActivity() != null) {
                        ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                    }
                    break;

            }
        }


        @Override
        public void onFailure(Call<UserModel> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private void trackFirebaseEvent(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsFireBase.USER_NAME_PROPERTY, name);
        FirebaseAnalytics.getInstance(getContext()).logEvent(ConstantsFireBase.LOGIN_EVENT, bundle);
    }


    private Callback<String> resendActivationCallback = new Callback<String>() {
        @Override
        public void onResponse(Call<String> call, Response<String> response) {

        }


        @Override
        public void onFailure(Call<String> call, Throwable t) {

        }
    };


    Callback<UserModel> fbLoginCallback = new Callback<UserModel>() {
        @Override
        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
            cancelProgressDialog();
            switch (response.code()) {
                case ConstantsAPI.CODE_200_SUCCESS:
                    if (response.body().getAuthToken() == null) {
                        String fbToken = SharedPrefManager.getInstance(getActivity()).getStoredFbToken();
                        SharedPrefManager.getInstance(getActivity()).storeFbToken("");
                        if (response.body().getEmail() == null) {
                            ((MainActivity) getActivity()).setFragmentWithBackStack(
                                    SignUpEmailFragment.getInstance("", fbToken), SignUpEmailFragment.TAG_SIGN_UP);
                        } else {
                            ((MainActivity) getActivity()).setFragmentWithBackStack(
                                    SignUpUserPhoto.getInstance(response.body().getEmail(), fbToken), SignUpUserPhoto.TAG_SIGN_UP_PHOTO);
                        }
                    } else {
                        storeFbLoginUserData(response.body());
                        trackLoginFbFirebaseEvent(response.body().getUserName());
                        startActivity(new Intent(getActivity(), MainActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    }
                    break;
                case ConstantsAPI.CODE_400_BAD_REQUEST:
                    SharedPrefManager.getInstance(getActivity()).storeFbToken("");
                    Toast.makeText(getActivity(), R.string.error_wrong_email_or_password, Toast.LENGTH_LONG).show();
                    break;
                case ConstantsAPI.CODE_403_VEREFIY_ACCOUNT:
                    SharedPrefManager.getInstance(getActivity()).storeFbToken("");
                    Toast.makeText(getActivity(), R.string.error_confirm_email, Toast.LENGTH_SHORT).show();
                    break;
                case ConstantsAPI.CODE_429_SERVER_UPDATING:
                    if (getActivity() != null) {
                        SharedPrefManager.getInstance(getActivity()).storeFbToken("");
                        ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_sorry), getString(R.string.error_updating_server));
                    }
                    break;
                default:
                    if (getActivity() != null) {
                        ((MainActivity) getActivity()).showErrorView(getString(R.string.error_title_error), getString(R.string.error_internal_server_error));
                        SharedPrefManager.getInstance(getActivity()).storeFbToken("");
                    }
                    break;
            }
        }


        @Override
        public void onFailure(Call<UserModel> call, Throwable t) {
            cancelProgressDialog();
            SharedPrefManager.getInstance(getActivity()).storeFbToken("");
        }
    };


    private void storeLoginUserData(UserModel userModel) {
        SharedPrefManager.getInstance(getActivity()).storeEmail(userModel.getEmail());
        SharedPrefManager.getInstance(getActivity()).storeUserId(userModel.getId());
        SharedPrefManager.getInstance(getActivity()).storeAuthToken(userModel.getAuthToken());
        SharedPrefManager.getInstance(getActivity()).storeUserName(userModel.getUserName());
        SharedPrefManager.getInstance(getActivity()).storePassword(password);
        MixpanelManager.registerMixpanel(getContext(), fireBaseToken, userModel);
    }


    private void storeFbLoginUserData(UserModel userModel) {
        SharedPrefManager.getInstance(getActivity()).storeEmail(userModel.getEmail());
        SharedPrefManager.getInstance(getActivity()).storeUserId(userModel.getId());
        SharedPrefManager.getInstance(getActivity()).storeAuthToken(userModel.getAuthToken());
        SharedPrefManager.getInstance(getActivity()).storeUserName(userModel.getUserName());
        MixpanelManager.registerMixpanel(getContext(), fireBaseToken, userModel);
    }


    private void trackLoginFbFirebaseEvent(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsFireBase.USER_NAME_PROPERTY, name);
        FirebaseAnalytics.getInstance(getContext()).logEvent(ConstantsFireBase.LOGIN_EVENT, bundle);
    }
}

