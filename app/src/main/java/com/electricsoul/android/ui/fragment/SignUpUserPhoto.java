package com.electricsoul.android.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.electricsoul.android.R;
import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.network.clients.RESTClient;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.base.BaseFragment;
import com.electricsoul.android.utils.FontUtil;
import com.electricsoul.android.utils.GetPhotoUtil;
import com.electricsoul.android.utils.KeyboardUtil;
import com.electricsoul.android.utils.LogUtil;
import com.electricsoul.android.utils.Regex;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yuliasokolova on 12.12.16.
 */

public class SignUpUserPhoto extends BaseFragment implements ReenterPasswordFragment.OnNextClickListener {

    public static final String TAG_SIGN_UP_PHOTO            = "sign_photo";
    private static final String TERMS_AND_CONDITIONS_URL    = "http://www.electricsoul.com/tos";
    private static final int REQUEST_PHOTO_PROFILE          = 0;

    private View rootView;

    @BindView(R.id.iv_change_profile_photo)
    LinearLayout ivChangeProfilePhoto;

    @BindView(R.id.signup_profile_photo)
    CircleImageView profilePhoto;

    @BindView(R.id.sign_up_et_password)
    EditText etPassword;

    @BindView(R.id.sign_up_et_username)
    EditText etUserName;

    @BindView(R.id.signin_layout)
    LinearLayout signInLayout;

    @BindView(R.id.ll_font_root)
    FrameLayout layoutFontRoot;

    @BindView(R.id.tv_terms_policy2)
    TextView tvTermsPolicy;

    private int mRequestedPhoto = -1;
    private GetPhotoUtil mGetPhotoUtil;
    private Bitmap pickedPhoto;
    private File userPhoto;
    private String userEmail;
    private String userFbToken;
    private String fireBaseToken;


    public static SignUpUserPhoto getInstance(String email, String fbToken) {
        SignUpUserPhoto fragment = new SignUpUserPhoto();
        fragment.userEmail = email;
        fragment.userFbToken = fbToken;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.sign_up_photo, container, false);
        ButterKnife.bind(this, rootView);
        fireBaseToken = FirebaseInstanceId.getInstance().getToken();
        etPassword.setOnEditorActionListener(keyboardDoneListener);

        initGalleryButton();
        initPhotoPicker(savedInstanceState);
        initFonts();
        return rootView;
    }


    private void initFonts() {
        Typeface typeface_black = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_BLACK);
        Typeface typeface_medium = Typeface.createFromAsset(getActivity().getAssets(), FontUtil.FONT_RALEWAY_MEDIUM);
        FontUtil.impl(layoutFontRoot, typeface_medium);
        tvTermsPolicy.setTypeface(typeface_black);
    }


    private void initPhotoPicker(Bundle savedInstanceState) {
        mGetPhotoUtil = new GetPhotoUtil(this, getActivity(), new GetPhotoUtil.PhotoUtilListener() {

            @Override
            public void onImageDecoding() {
            }


            @Override
            public void onPicObtained(GetPhotoUtil.LoadedPhoto loadedPhoto) {

                if (mRequestedPhoto == REQUEST_PHOTO_PROFILE) {
                    userPhoto = loadedPhoto.file;
                    pickedPhoto = loadedPhoto.bitmap;
                    setPhotoInView();
                }

                mRequestedPhoto = -1;
            }
        });

        if (savedInstanceState != null) {
            mGetPhotoUtil.loadInstance(savedInstanceState);
        }
    }


    private void initGalleryButton() {
        ivChangeProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRequestedPhoto = REQUEST_PHOTO_PROFILE;
                mGetPhotoUtil.takeFromGallery();
            }
        });
    }


    @OnClick(R.id.tv_terms_policy2)
    void onTermsClick() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(TERMS_AND_CONDITIONS_URL));
        startActivity(intent);
    }


    @OnClick(R.id.btn_sign_up_email)
    void onNextClick() {
        checkUserData();
    }


    private TextView.OnEditorActionListener keyboardDoneListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                checkUserData();
            }
            return false;
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        KeyboardUtil.hideKeyboard(getActivity());
        initKeyboardListener(rootView);
        setPhotoInView();
    }


    @Override
    public void onPause() {
        super.onPause();
        removeGlobalLayoutListener();
    }


    private void setPhotoInView() {
        if(pickedPhoto != null) {
            profilePhoto.setImageBitmap(pickedPhoto);
        }
    }


    private void checkUserData() {
        if (isDataValid()) {
            KeyboardUtil.hideKeyboard(getActivity());
            ReenterPasswordFragment fragment = ReenterPasswordFragment.getInstance(etPassword.getText().toString());
            fragment.setOnNextClick(this);
            ((MainActivity) getActivity()).setFragmentWithBackStack(fragment, null);
        }
    }


    private boolean isDataValid() {
        if (!isPasswordValid(etPassword.getText().toString())) {
            Toast.makeText(getActivity(), R.string.error_password_length, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (Regex.removeMultiSpace(etUserName.getText().toString()).length() < 3) {
            Toast.makeText(getActivity(), R.string.error_name_length, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }


    private boolean isPasswordValid(String password) {
        return password.length() > 7;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mGetPhotoUtil != null) {
            mGetPhotoUtil.saveInstance(outState);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mGetPhotoUtil.resolveResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GetPhotoUtil.REQ_GALLERY:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mGetPhotoUtil.sendGallery();
                } else {
                    LogUtil.logE("Permission Denied");
                }
                break;
        }
    }


    @Override
    public void onPasswordMatched() {
        showProgressDialog();
        LogUtil.logE("userFbToken - " + userFbToken);
        if (userFbToken.isEmpty()) {
            RESTClient.getInstance().requestSignUp(userEmail,
                    etUserName.getText().toString(), etPassword.getText().toString(), fireBaseToken, userPhoto, userPhoto, signUpCallback);
        } else {
            RESTClient.getInstance().requestSignUpWithFb(userEmail,
                    etUserName.getText().toString(), etPassword.getText().toString(), fireBaseToken, userFbToken, userPhoto, userPhoto, signUpCallback);
        }
    }


    Callback<JSONObject> signUpCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(Call<JSONObject> call, final Response<JSONObject> response) {
            if (response.code() == ConstantsAPI.CODE_201_CREATED) {
                cancelProgressDialog();
                final AlertDialog.Builder dialong = new AlertDialog.Builder(getActivity());
                dialong.setMessage(getString(R.string.account_was_created));
                dialong.setPositiveButton(R.string.signup_btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        trackSignupFirebaseEvent();
                        dialogInterface.dismiss();
                        ((MainActivity) getActivity()).openProfileTab();
                    }
                });
                AlertDialog alert = dialong.create();
                alert.show();
            }
            // TODO: 18.11.16 handle error response from server like (user exist, email exist, ect...)
            if (response.code() == ConstantsAPI.CODE_400_BAD_REQUEST) {
                cancelProgressDialog();
                Toast.makeText(getActivity(), R.string.error_email_or_name_exist, Toast.LENGTH_LONG).show();
            }

        }


        @Override
        public void onFailure(Call<JSONObject> call, Throwable t) {
            cancelProgressDialog();
        }
    };


    private void trackSignupFirebaseEvent() {
        FirebaseAnalytics.getInstance(getContext()).logEvent(ConstantsFireBase.REGISTER_EVENT, new Bundle());
    }
}
