package com.electricsoul.android.model;

import com.electricsoul.android.model.elasticModel.UserModelElastic;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class FollowerResult implements Serializable {

    private int count;
    private String next;
    private String previous;
    private ArrayList<Follower> results;


    public int getCount() {
        return count;
    }


    public void setCount(int count) {
        this.count = count;
    }


    public String getNext() {
        return next;
    }


    public void setNext(String next) {
        this.next = next;
    }


    public String getPrevious() {
        return previous;
    }


    public void setPrevious(String previous) {
        this.previous = previous;
    }


    public ArrayList<Follower> getResults() {
        return results == null ? new ArrayList<Follower>() : results;
    }


    public void setResults(ArrayList<Follower> results) {
        this.results = results;
    }


    public boolean isUserFollowing(String userId) {
        for (Follower follower : results) {
            if (follower.getId().equals(userId)) {
                return true;
            }
        }
        return false;
    }


    public void addFollowings(UserModel user) {
        Follower follower = new Follower();
        follower.setUsername(user.getUserName());
        follower.setId(user.getId());
        follower.setImage(user.getImage().getThumbnail());
        results.add(follower);
    }


    public void addFollowings(UserModelElastic user) {
        Follower follower = new Follower();
        follower.setUsername(user.getUserName());
        follower.setId(user.getId());
        follower.setImage(user.getUserImage());
        results.add(follower);
    }

    public void cleanFollowList(){
        results.clear();
    }


    public void addFollowingsList(List<Follower> followerList) {
        results.addAll(followerList);
    }


    public void removeFollowings(UserModel user) {
        for (int i = 0; i < results.size(); i++) {
            Follower follower = results.get(i);
            if (follower.getId().equals(user.getId())) {
                results.remove(follower);
            }
        }
    }

    public void removeFollowings(UserModelElastic user) {
        for (int i = 0; i < results.size(); i++) {
            Follower follower = results.get(i);
            if (follower.getId().equals(user.getId())) {
                results.remove(follower);
            }
        }
    }


    public class Follower implements Serializable {

        private String username;
        private String id;
        private String image;

        @SerializedName("cover_image")
        private String coverImage;


        public String getCoverImage() {
            return coverImage;
        }


        public void setCoverImage(String coverImage) {
            this.coverImage = coverImage;
        }


        public String getId() {
            return id;
        }


        public void setId(String id) {
            this.id = id;
        }


        public String getImage() {
            return image;
        }


        public void setImage(String image) {
            this.image = image;
        }


        public String getUsername() {
            return username;
        }


        public void setUsername(String username) {
            this.username = username;
        }
    }
}
