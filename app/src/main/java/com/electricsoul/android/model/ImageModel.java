package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class ImageModel implements Serializable {

    private String thumbnail = "";

    @SerializedName("full_size")
    private String fullSize = "";

    @SerializedName("thumbnail_small")
    private String thumbnailSmall = "";

    public String getFullSize() {
        return fullSize;
    }

    public void setFullSize(String fullSize) {
        this.fullSize = fullSize;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getThumbnailSmall() {
        return thumbnailSmall != null ? thumbnailSmall : "Image com.electricsoul.android.model don't have thumbnail_small image";
    }

    public void setThumbnailSmall(String thumbnailSmall) {
        this.thumbnailSmall = thumbnailSmall;
    }

    @Override
    public String toString() {
        return "ImageModel{" +
                "fullSize='" + fullSize + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", thumbnail_small='" + thumbnailSmall + '\'' +
                '}';
    }
}
