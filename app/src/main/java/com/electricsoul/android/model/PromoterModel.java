package com.electricsoul.android.model;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class PromoterModel implements Serializable {

    private String id;
    private String name;
    private ImageModel image;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public ImageModel getImage() {
        return image;
    }


    public void setImage(ImageModel image) {
        this.image = image;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public boolean hasImageThumbnail() {
        return getImage() != null && getImage().getThumbnail() != null && !getImage().getThumbnail().isEmpty();
    }


    @Override
    public String toString() {
        return "PromoterModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image=" + image +
                '}';
    }
}
