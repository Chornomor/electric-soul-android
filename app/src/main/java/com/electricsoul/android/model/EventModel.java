package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class EventModel implements Serializable {

    private String id;
    private String name;
    private String start;
    private String end;
    private PromoterModel promoter;
    private AddressModel address;
    private int attendees_count;

    private ArrayList<FanImageModel> fanImages = new ArrayList<>();

    @SerializedName("is_festival")
    private boolean isFestival;

    @SerializedName("cover_image")
    private ImageModel coverImage;


    public boolean isFestival() {
        return isFestival;
    }


    public AddressModel getAddress() {
        return address;
    }


    public void setAddress(AddressModel address) {
        this.address = address;
    }


    public ImageModel getCoverImage() {
        return coverImage;
    }


    public void setCoverImage(ImageModel coverImage) {
        this.coverImage = coverImage;
    }


    public String getEnd() {
        return end;
    }


    public void setEnd(String end) {
        this.end = end;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public PromoterModel getPromoter() {
        return promoter;
    }


    public void setPromoter(PromoterModel promoter) {
        this.promoter = promoter;
    }


    public String getStart() {
        return start;
    }


    public void setStart(String start) {
        this.start = start;
    }


    public int getAttendees_count() {
        return attendees_count;
    }


    public void setAttendees_count(int attendees_count) {
        this.attendees_count = attendees_count;
    }


    public List<FanImageModel> getFanImages() {
        return fanImages;
    }


    public void putFanImage(FanImageModel fanImage) {
        if (fanImages.isEmpty()) {
            fanImages.add(fanImage);
        } else {
            boolean notContain = true;
            for (int i = 0; i < fanImages.size(); i++) {
                FanImageModel fanImageModel = fanImages.get(i);
                if (fanImageModel.getId().equals(fanImage.getId())) {
                    notContain = false;
                }
            }
            if (notContain){
                fanImages.add(fanImage);
            }
        }
    }


    @Override
    public String toString() {
        return "EventModel{" +
                "address=" + address +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", promoter=" + promoter +
                ", coverImage=" + coverImage +
                '}';
    }
}
