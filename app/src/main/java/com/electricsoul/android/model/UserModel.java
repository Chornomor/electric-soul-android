package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 11.11.16.
 */

public class UserModel implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("email")
    private String email;

    @SerializedName("username")
    private String userName;

    @SerializedName("auth_token")
    private String authToken;

    @SerializedName("fb_id")
    private String fbId;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("follower_count")
    private int followerCount;

    @SerializedName("following_count")
    private int followingCount;

    @SerializedName("image")
    private ImageModel image;

    @SerializedName("cover_image")
    private ImageModel coverImage;


    public UserModel() {
    }


    public UserModel(FollowerResult.Follower follower) {
        userName = follower.getUsername();
        id = follower.getId();
        if (follower.getImage() != null) {
            ImageModel imageModel = new ImageModel();
            imageModel.setThumbnail(follower.getImage());
            image = imageModel;
        }
        if (follower.getCoverImage() != null) {
            ImageModel imageModel = new ImageModel();
            imageModel.setFullSize(follower.getCoverImage());
            coverImage = imageModel;
        }
    }

    public String getAuthToken() {
        return authToken;
    }


    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }


    public ImageModel getCoverImage() {
        return coverImage;
    }


    public void setCoverImage(ImageModel coverImage) {
        this.coverImage = coverImage;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getFbId() {
        return fbId;
    }


    public void setFbId(String fbId) {
        this.fbId = fbId;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public int getFollowerCount() {
        return followerCount;
    }


    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }


    public int getFollowingCount() {
        return followingCount;
    }


    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public ImageModel getImage() {
        if (image == null) {
            return new ImageModel();
        }
        return image;
    }


    public boolean hasImageThumbnail() {
        return getImage() != null && getImage().getThumbnail() != null && !getImage().getThumbnail().isEmpty();
    }


    public boolean hasCoverImageThumbnail() {
        return getCoverImage() != null && getCoverImage().getThumbnail() != null && !getCoverImage().getThumbnail().isEmpty();
    }


    public void setImage(ImageModel image) {
        this.image = image;
    }


    public String getUserName() {
        return userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }


    @Override
    public String toString() {
        return "UserModel{" +
                "authToken='" + authToken + '\'' +
                ", id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                ", fbId='" + fbId + '\'' +
                ", image=" + image +
                ", coverImage=" + coverImage +
                '}';
    }
}
