package com.electricsoul.android.model;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 15.11.16.
 */

public class ImageFanModel extends FanImageModel implements Serializable{

    String eventId;


    public String getEvent() {
        return eventId;
    }


    public void setEvent(String event) {
        this.eventId = event;
    }


    @Override
    public String toString() {
        return "ImageFanModel{" +
                "event='" + eventId + '\'' +
                '}';
    }
}
