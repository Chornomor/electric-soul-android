package com.electricsoul.android.model.Likes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class FanImageLike extends Like implements Serializable {

    @SerializedName("event")
    String eventId;

    @SerializedName("is_like")
    String fanImageIsLike;


    public String getFanImageIsLike() {
        return fanImageIsLike;
    }


    public String getEventId() {
        return eventId;
    }


    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

}
