package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class EventResultModel implements Serializable {

    private int count;
    private String next;
    private String previous;

    @SerializedName("results")
    private ArrayList<EventModel> eventModelList;


    public int getCount() {
        return count;
    }


    public void setCount(int count) {
        this.count = count;
    }


    public ArrayList<EventModel> getEventModelList() {
        if (eventModelList == null) {
            new ArrayList<>();
        }
        return eventModelList;
    }


    public ArrayList<EventModel> getEventModelListWithPhotos() {
        ArrayList<EventModel> eventModels = new ArrayList<>();
        if (eventModelList == null) {
            new ArrayList<>();
        }
        for (EventModel event : eventModelList) {
            if (!event.getFanImages().isEmpty()) {
                eventModels.add(event);
            }
        }
        return eventModels;
    }


    public void setEventModelList(ArrayList<EventModel> eventModelList) {
        this.eventModelList = eventModelList;
    }


    public String getNext() {
        return next;
    }


    public void setNext(String next) {
        this.next = next;
    }


    public String getPrevious() {
        return previous;
    }


    public void setPrevious(String previous) {
        this.previous = previous;
    }


    public void collectPhotosForEvents(FanImageResult result) {
        if (result == null) {
            return;
        }
        if (eventModelList != null) {
            for (EventModel event : eventModelList) {
                for (FanImageModel model : result.getFanImages()) {
                    if (event.getId().equals(model.getEventId())) {
                        event.putFanImage(model);
                    }
                }
            }
        }
    }


    @Override
    public String toString() {
        return "EventResultModel{" +
                "count=" + count +
                ", next='" + next + '\'' +
                ", previous='" + previous + '\'' +
                ", eventModelList=" + eventModelList +
                '}';
    }
}
