package com.electricsoul.android.model.elasticModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class AddressModelElastic implements Serializable {

    @SerializedName("pk")
    private String id = "";

    @SerializedName("fields")
    private Field field;


    public String getId() {
        return id;
    }


    public String getAddress1() {
        return field.address1;
    }


    public class Field {
        private String address1 = "";
        private String address2 = "";
        private String zipcode = "";
    }
}
