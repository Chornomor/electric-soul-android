package com.electricsoul.android.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class CommentResult implements Serializable {

    private int count;
    private String next;
    private String previous;
    private ArrayList<Comment> results = new ArrayList<>();


    public int getCount() {
        return count;
    }


    public void setCount(int count) {
        this.count = count;
    }


    public String getNext() {
        return next;
    }


    public void setNext(String next) {
        this.next = next;
    }


    public String getPrevious() {
        return previous;
    }


    public void setPrevious(String previous) {
        this.previous = previous;
    }


    public ArrayList<Comment> getResults() {
        return results;
    }


    public void setResults(ArrayList<Comment> results) {
        this.results = results;
    }
}
