package com.electricsoul.android.model.Likes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class UserImageLike extends Like implements Serializable {

    @SerializedName("user_image_pk")
    String userId;


    public String getUserId() {
        return userId;
    }


    public void setUserId(String eventId) {
        this.userId = eventId;
    }
}