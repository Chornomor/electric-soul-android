package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class FanImageModel implements Serializable {

    private String id;
    private String created;

    private ImageModel image;

    @SerializedName("fanimage_comment_count")
    private int fanImageCommentCount;

    @SerializedName("event")
    private String eventId;

    @SerializedName("image_width")
    private int imageWidth;

    @SerializedName("image_height")
    private int imageHeight;

    @SerializedName("is_owner")
    private boolean isOwner;


    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public ImageModel getImage() {
        return image;
    }


    public void setImage(ImageModel image) {
        this.image = image;
    }


    public int getFanImageCommentCount() {
        return fanImageCommentCount;
    }


    public void setFanImageCommentCount(int fanImageCommentCount) {
        this.fanImageCommentCount = fanImageCommentCount;
    }


    public String getEventId() {
        return eventId;
    }


    public void setEventId(String eventId) {
        this.eventId = eventId;
    }


    public int getImageHeight() {
        return imageHeight;
    }


    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }


    public int getImageWidth() {
        return imageWidth;
    }


    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }


    public boolean isOwner() {
        return isOwner;
    }


    public void setOwner(boolean owner) {
        isOwner = owner;
    }


    public boolean hasImageThumbnail() {
        return getImage() != null && getImage().getThumbnail() != null && !getImage().getThumbnail().isEmpty();
    }


    public boolean hasFullImage() {
        return getImage() != null && getImage().getFullSize() != null && !getImage().getFullSize().isEmpty();
    }


    @Override
    public String toString() {
        return "ImageResultModel{" +
                "created='" + created + '\'' +
                ", id='" + id + '\'' +
                ", image=" + image +
                ", imageWidth=" + imageWidth +
                ", imageHeight=" + imageHeight +
                ", isOwner=" + isOwner +
                '}';
    }
}
