package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class DetailEventModel extends EventModel {

    private String description;
    private String remark;
    private ArrayList<SearchModel> artists;
    private boolean rsvp;

    @SerializedName("fan_images_count")
    private int fanImagesCount;

    @SerializedName("fan_images")
    private ArrayList<ImageFanModel> fanImages;

    @SerializedName("ticket_url")
    private String ticketUrl;


    public ArrayList<SearchModel> getArtists() {
        return artists;
    }

    public String getArtistNames() {
        String names = "";
        String nextLine = "";
        for (SearchModel artist : artists) {
            names += nextLine + artist.getName();
            if (nextLine.isEmpty())
                nextLine = System.lineSeparator();
        }
        return names;
    }

    public void setArtists(ArrayList<SearchModel> artists) {
        this.artists = artists;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setFanImages(ArrayList<ImageFanModel> fanImages) {
        this.fanImages = fanImages;
    }


    public int getFanImagesCount() {
        return fanImagesCount;
    }

    public void setFanImagesCount(int fanImagesCount) {
        this.fanImagesCount = fanImagesCount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean isRsvp() {
        return rsvp;
    }

    public void setRsvp(boolean rsvp) {
        this.rsvp = rsvp;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }

    @Override
    public String toString() {
        return "DetailEventModel{" +
                "artists=" + artists +
                ", description='" + description + '\'' +
                ", remark='" + remark + '\'' +
                ", rsvp=" + rsvp +
                ", fanImagesCount=" + fanImagesCount +
                ", fanImages=" + fanImages +
                ", ticketUrl='" + ticketUrl + '\'' +
                '}';
    }
}
