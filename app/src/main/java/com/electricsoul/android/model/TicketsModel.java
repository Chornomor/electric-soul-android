package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexchern on 24.01.17.
 */

public class TicketsModel {

    private String id;
    private String description;
    private float price;

    @SerializedName("ticket_category")
    private String ticketCategory;

    @SerializedName("currency_code")
    private String currencyCode;

    @SerializedName("sales_end_date")
    private String salesEndDate;

    @SerializedName("event_name")
    private String eventName;

    @SerializedName("remaining_count")
    private int remainingCount;

    private int count;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public int getRemainingCount() {
        return remainingCount;
    }


    public void setRemainingCount(int remainingCount) {
        this.remainingCount = remainingCount;
    }


    public String getEventName() {
        return eventName;
    }


    public void setEventName(String eventName) {
        this.eventName = eventName;
    }


    public String getTicketCategory() {
        return ticketCategory;
    }


    public void setTicketCategory(String ticketCategory) {
        this.ticketCategory = ticketCategory;
    }


    public float getPrice() {
        return price;
    }


    public void setPrice(int price) {
        this.price = price;
    }


    public String getDescription() {
        return description;
    }


    public String getCurrencyCode() {
        return currencyCode;
    }


    public String getSalesEndDate() {
        return salesEndDate;
    }


    public int getCount() {
        return count;
    }


    public void setCount(int count) {
        this.count = count;
    }
}
