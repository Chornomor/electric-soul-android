package com.electricsoul.android.model.Likes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class EventImageLike extends Like implements Serializable {

    @SerializedName("event_image_pk")
    String eventId;

    @SerializedName("is_like")
    String eventIsLike;


    public String getEventId() {
        return eventId;
    }


    public void setEventId(String eventId) {
        this.eventId = eventId;
    }


    /** @return 0 - if i didn't like it
     *          1 - if i liked
     *          2 - user don't authorised
     */
    public String getEventIsLike() {
        return eventIsLike;
    }
}
