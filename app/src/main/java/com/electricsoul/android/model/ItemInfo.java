package com.electricsoul.android.model;

public class ItemInfo {

    private String name;
    private float priceMicros;
    private String currency;

    public ItemInfo(String name, float price, String currency) {
        this.name = name;
        this.priceMicros = price;
        this.currency = currency;
    }

    @Override
    public String toString() {
        return name;
    }


    public String getName() {
        return name;
    }


    public float getPriceMicros() {
        return priceMicros;
    }


    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
