package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by alexchern on 23.11.16.
 */

public class PromoterResult implements Serializable {
    private int count;
    private String next;
    private String previous;

    @SerializedName("results")
    private ArrayList<PromoterModel> promoters;


    public int getCount() {
        return count;
    }


    public void setCount(int count) {
        this.count = count;
    }


    public String getNext() {
        return next;
    }


    public void setNext(String next) {
        this.next = next;
    }


    public String getPrevious() {
        return previous;
    }


    public void setPrevious(String previous) {
        this.previous = previous;
    }


    public ArrayList<PromoterModel> getPromoters() {
        return promoters;
    }


    public void setPromoters(ArrayList<PromoterModel> promoters) {
        this.promoters = promoters;
    }
}
