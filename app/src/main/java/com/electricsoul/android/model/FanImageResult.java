package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by eugenetroyanskii on 15.11.16.
 */

public class FanImageResult implements Serializable {

    private int count;

    @SerializedName("results")
    private ArrayList<FanImageModel> fanImages;


    public ArrayList<FanImageModel> getFanImages() {
        return fanImages;
    }

}
