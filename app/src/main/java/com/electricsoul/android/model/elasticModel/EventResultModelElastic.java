package com.electricsoul.android.model.elasticModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class EventResultModelElastic implements Serializable {

    private Hits hits;


    public int getTotal() {
        return hits.total;
    }


    public ArrayList<EventModelElastic> getEventsModelElastic() {
        return hits.eventsModelElastic;
    }


    public class Hits implements Serializable {

        private int total;

        @SerializedName("hits")
        public ArrayList<EventModelElastic> eventsModelElastic;

    }
}
