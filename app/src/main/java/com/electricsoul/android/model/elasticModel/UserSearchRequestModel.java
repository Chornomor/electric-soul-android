package com.electricsoul.android.model.elasticModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexchern on 07.02.17.
 */

public class UserSearchRequestModel {

    private int size = 30;
    private int from = 0;

    @SerializedName("query")
    private Query query;


    public UserSearchRequestModel(int page, String searchWord) {
        from = page;
        query = new Query("*" + searchWord.toLowerCase() + "*");
    }


    private class Query {

        @SerializedName("wildcard")
        private Match match;


        private Query(String searchWord) {
            match = new Match(searchWord);
        }
    }


    private class Match {

        @SerializedName("fields.username")
        private String name;


        public Match(String name) {
            this.name = name;
        }
    }

}
