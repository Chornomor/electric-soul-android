package com.electricsoul.android.model.Likes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class PromoterImageLike extends Like implements Serializable {

    @SerializedName("promoter")
    String promoterId;


    public String getPromoterId() {
        return promoterId;
    }


    public void setPromoterId(String eventId) {
        this.promoterId = eventId;
    }
}

