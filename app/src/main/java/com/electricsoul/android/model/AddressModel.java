package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class AddressModel implements Serializable{

    private String id;
    private String address1;
    private String address2;
    private String zipcode;
    private City city;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "AddressModel{" +
                "address1='" + address1 + '\'' +
                ", id='" + id + '\'' +
                ", address2='" + address2 + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", city=" + city +
                '}';
    }

    public String getAddressForDetails() {
        StringBuilder address = new StringBuilder();
        address.append(getAddress1() + System.lineSeparator());
        address.append(getAddress2() + System.lineSeparator());
        if (getCity() != null){
            address.append(getCity().getStateAndCountry());
        }
        return new String(address);
    }

    public class City implements Serializable{

        String id;
        String name;
        String state;
        String country;

        @SerializedName("city_code")
        String cityCode;

        public String getCityCode() {
            return cityCode;
        }

        public void setCityCode(String cityCode) {
            this.cityCode = cityCode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getState() {
            return state;
        }

        public String getStateAndCountry() {
            return "(" + state + ", " + country + ")";
        }

        public void setState(String state) {
            this.state = state;
        }

        @Override
        public String toString() {
            return "City{" +
                    "cityCode='" + cityCode + '\'' +
                    ", id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", state='" + state + '\'' +
                    ", country='" + country + '\'' +
                    '}';
        }
    }
}
