package com.electricsoul.android.model.elasticModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexchern on 07.02.17.
 */

public class EventSearchRequestModel {

    private int size = 30;
    private int from = 0;

    private Query query;


    public EventSearchRequestModel(int page, String searchWord, boolean isFestival) {
        from = page;
        query = new Query("*" + searchWord.toLowerCase() + "*", isFestival);
    }


    private class Query {

        private Bool bool;


        private Query(String searchWord, boolean isFestival) {
            bool = new Bool(searchWord, isFestival);
        }
    }


    private class Bool {

        private List<Object> must = new ArrayList<>();


        private Bool(String searchWord, boolean isFestival) {

            must.add(new WildcardObject(searchWord));
            must.add(new MatchObject(isFestival));
        }
    }


    private class WildcardObject {

        private WildCard wildcard;


        private WildcardObject(String s) {
            wildcard = new WildCard(s);
        }
    }


    private class MatchObject {

        private Match wildcard;


        private MatchObject(boolean b) {

            wildcard = new Match(b);
        }
    }


    private class Match {

        @SerializedName("fields.is_festival")
        private boolean isFestival;


        private Match(boolean isFestival) {
            this.isFestival = isFestival;
        }
    }


    private class WildCard {

        @SerializedName("fields.name")
        private String name;


        private WildCard(String name) {
            this.name = name;
        }
    }
}
