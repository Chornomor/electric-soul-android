package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class SearchResultModel implements Serializable{

    public static int RESULTS_INDEX   = 0;

    @SerializedName("results")
    private ArrayList<SearchModel> searchResult;

    @SerializedName("count")
    private int conut;

    @SerializedName("previous")
    private String previous;

    @SerializedName("next")
    private String next;

    public int getConut() {
        return conut;
    }

    public void setConut(int conut) {
        this.conut = conut;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }


    public ArrayList<SearchModel> getSearchResult() {
        return searchResult;
    }


    public void setSearchResult(ArrayList<SearchModel> searchResult) {
        this.searchResult = searchResult;
    }


    public class SearchResults implements Serializable {

        private ArrayList<SearchModel> results;


        public ArrayList<SearchModel> getResults() {
            return results;
        }


        public void setResults(ArrayList<SearchModel> results) {
            this.results = results;
        }


        @Override
        public String toString() {
            return "SearchResults{" +
                    "results=" + results +
                    '}';
        }
    }
}
