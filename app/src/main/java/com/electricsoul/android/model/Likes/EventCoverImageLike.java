package com.electricsoul.android.model.Likes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class EventCoverImageLike extends Like implements Serializable {

    @SerializedName("event")
    String eventId;


    public String getEventId() {
        return eventId;
    }


    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

}
