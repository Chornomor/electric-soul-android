package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexchern on 24.02.17.
 */

public class FullWalletModel {

    @SerializedName("id")
    private String token;
    private String object;


    public String getToken() {
        return token;
    }


    public void setToken(String token) {
        this.token = token;
    }


    public String getObject() {
        return object;
    }


    public void setObject(String object) {
        this.object = object;
    }
}
