package com.electricsoul.android.model.elasticModel;

import com.electricsoul.android.model.AddressModel;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */

public class EventModelElastic implements Serializable {

    @SerializedName("_source")
    private Source source;


    public String getId() {
        return source.id;
    }


    public String getStart() {
        return source.eventModel.start;
    }


    public String getEnd() {
        return source.eventModel.end;
    }


    public String getName() {
        return source.eventModel.name;
    }


    public AddressModelElastic getAddress() {
        if (source.eventModel.address == null) {
            return new AddressModelElastic();
        }
        return source.eventModel.address;
    }


    public boolean isFestival() {
        return source.eventModel.isFestival;
    }


    public String getCoverImage() {
        if (!source.eventModel.coverImage.contains("http://") && !source.eventModel.coverImage.contains("https://")) {
            source.eventModel.coverImage = "http://" + source.eventModel.coverImage;
        }
        return source.eventModel.coverImage;
    }


    public class Source implements Serializable {

        @SerializedName("pk")
        private String id = "";

        @SerializedName("fields")
        private EventModel eventModel;


        public class EventModel implements Serializable {

            private String name = "";
            private String start = "";
            private String end = "";

            @SerializedName("is_festival")
            private boolean isFestival;

            @SerializedName("cover_image")
            private String coverImage;

            @SerializedName("address")
            private AddressModelElastic address;
        }
    }
}
