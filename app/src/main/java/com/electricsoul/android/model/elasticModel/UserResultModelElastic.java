package com.electricsoul.android.model.elasticModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by alexchern on 23.11.16.
 */

public class UserResultModelElastic implements Serializable {

    private Hits hits;


    public int getTotal() {
        return hits.total;
    }

    public ArrayList<UserModelElastic> getUsersModelElastic() {
        return hits.usersModelElastic;
    }

    public class Hits implements Serializable {

        private int total;

        @SerializedName("hits")
        public ArrayList<UserModelElastic> usersModelElastic;

    }
}
