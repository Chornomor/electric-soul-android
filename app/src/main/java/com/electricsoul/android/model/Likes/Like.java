package com.electricsoul.android.model.Likes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class Like implements Serializable {

    private int count;

    @SerializedName("user_names")
    String userNames;


    public String[] getUserNames() {
        return userNames.split(",");
    }


    public void setUserNames(String userNames) {
        this.userNames = userNames;
    }


    public int getCount() {
        return count;
    }


    public void setCount(int count) {
        this.count = count;
    }
}
