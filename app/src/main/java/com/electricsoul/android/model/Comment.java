package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 18.11.16.
 */

public class Comment implements Serializable {

    @SerializedName("event_comment_id")
    private String commentId;

    @SerializedName("event_id")
    private String eventId;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("text")
    private String comment;

    @SerializedName("comment_image")
    private String commentImage;

    @SerializedName("user_image")
    private String userImage;

    @SerializedName("user_name")
    private String userName;

    @SerializedName("like_count")
    private int likeCount;

    @SerializedName("is_like")
    private boolean isLike;


    public String getComment() {
        return comment;
    }


    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getCommentId() {
        return commentId;
    }


    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }


    public String getCommentImage() {
        return commentImage;
    }


    public void setCommentImage(String commentImage) {
        this.commentImage = commentImage;
    }


    public String getEventId() {
        return eventId;
    }


    public void setEventId(String eventId) {
        this.eventId = eventId;
    }


    public boolean isLike() {
        return isLike;
    }


    public void setLike(boolean like) {
        isLike = like;
    }


    public int getLikeCount() {
        return likeCount;
    }


    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }


    public String getUserId() {
        return userId;
    }


    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getUserImage() {
        return userImage;
    }


    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }


    public String getUserName() {
        return userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }
}
