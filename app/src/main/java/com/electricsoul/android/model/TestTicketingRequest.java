package com.electricsoul.android.model;

import java.util.ArrayList;

/**
 * Created by alexchern on 23.02.17.
 */

public class TestTicketingRequest extends TicketingRequest {

    public static final int TEST_TYPE = 1;

    private int test;

    public TestTicketingRequest(String token, String email, int test, ArrayList<TicketsModel> tickets) {
        super(token, email, tickets);
        this.test = test;
    }
}
