package com.electricsoul.android.model;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 14.11.16.
 */


public class SearchModel implements Serializable {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SearchModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}