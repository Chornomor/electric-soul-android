package com.electricsoul.android.model.elasticModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by eugenetroyanskii on 11.11.16.
 */

public class UserModelElastic implements Serializable {

    @SerializedName("_source")
    private Source source;


    public String getId() {
        return source.id;
    }


    public String getEmail() {
        return source.userModel.email;
    }


    public String getUserName() {
        return source.userModel.userName;
    }


    public String getUserImage() {
        if (!source.userModel.image.contains("http://") && !source.userModel.image.contains("https://")) {
            source.userModel.image = "http://" + source.userModel.image;
        }
        return source.userModel.image;
    }


    public class Source {

        @SerializedName("pk")
        private String id;

        @SerializedName("fields")
        private UserModel userModel;
    }


    public class UserModel {

        @SerializedName("email")
        private String email;

        @SerializedName("username")
        private String userName;

        private String image;
    }
}
