package com.electricsoul.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexchern on 23.02.17.
 */

public class TicketingRequest {

    private String token;
    private List<Object> tickets = new ArrayList<>();

    @SerializedName("sent_ticket_to")
    private String email;


    public TicketingRequest(String token, String email, ArrayList<TicketsModel> tickets) {
        this.token = token;
        this.email = email;
        for (TicketsModel ticket : tickets) {
            this.tickets.add(new Ticket(ticket));
        }
    }


    private class Ticket {

        @SerializedName("ticket_id")
        private String ticketId;

        private int count;


        public Ticket(TicketsModel ticketsModel) {
            this.count = ticketsModel.getCount();
            this.ticketId = ticketsModel.getId();
        }
    }
}
