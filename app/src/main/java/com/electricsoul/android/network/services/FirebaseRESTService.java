package com.electricsoul.android.network.services;

import com.electricsoul.android.constants.ConstantsFireBase;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by alexchorn on 20.01.17.
 */

public interface FirebaseRESTService {

    @FormUrlEncoded
    @POST(ConstantsFireBase.SHORT_LINK + ConstantsFireBase.APP_KEY)
    Call<JsonObject> getShortDynamicLick(@Field("longDynamicLink") String longDynamicLink);

}
