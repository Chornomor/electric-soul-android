package com.electricsoul.android.network.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.electricsoul.android.R;
import com.electricsoul.android.constants.ConstantsNotification;
import com.electricsoul.android.ui.activity.MainActivity;
import com.electricsoul.android.ui.activity.SplashActivity;
import com.electricsoul.android.utils.LogUtil;

/**
 * Created by eugenetroyanskii on 29.11.16.
 */

public class FireMsgService extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.google.android.c2dm.intent.RECEIVE".equals(action)) {
            this.handleNotificationIntent(context, intent);
        }

    }


    private void handleNotificationIntent(Context context, Intent intent) {
        showNotification(context, intent);
    }


    public void showNotification(Context context, Intent intent) {

        String eventId = intent.getStringExtra(ConstantsNotification.NOTIFICATION_EVENT_ID);
        String title = intent.getStringExtra(ConstantsNotification.NOTIFICATION_TITLE);

        if (title == null) {
            return;
        }

        LogUtil.logE("Notification title " + title);
        LogUtil.logE("Notification eventID " + eventId);

        if (MainActivity.isActivityVisible()) {
            intent.setClass(context, MainActivity.class);
        } else {
            intent.setClass(context, SplashActivity.class);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(ConstantsNotification.NOTIFICATION_EVENT_ID, eventId);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1410,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.event_button)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentTitle("Message")
                .setContentText(title)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager)
                        context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1410, notificationBuilder.build());
    }
}
