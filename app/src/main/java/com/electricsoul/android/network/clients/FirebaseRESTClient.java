package com.electricsoul.android.network.clients;

import com.electricsoul.android.network.NetworkManager;
import com.google.gson.JsonObject;

import retrofit2.Callback;

/**
 * Created by alexchern on 20.01.17.
 */

public class FirebaseRESTClient {

    private static FirebaseRESTClient instance;


    private FirebaseRESTClient() {
    }


    public static synchronized FirebaseRESTClient getInstance() {
        if (instance == null) {
            synchronized (RESTClient.class) {
                if (instance == null) {
                    instance = new FirebaseRESTClient();
                }
            }
        }
        return instance;
    }


    public void getShortDynamicLick(String longDynamicLink, Callback<JsonObject> callback) {
        NetworkManager.getFirebaseRESTService().getShortDynamicLick(longDynamicLink).enqueue(callback);
    }
}
