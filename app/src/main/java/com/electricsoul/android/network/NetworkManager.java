package com.electricsoul.android.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.constants.ConstantsFireBase;
import com.electricsoul.android.network.services.ElasticRESTService;
import com.electricsoul.android.network.services.FirebaseRESTService;
import com.electricsoul.android.network.services.RESTService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by eugenetroyanskii on 11.11.16.
 */


public class NetworkManager {

    private static RESTService restService;
    private static ElasticRESTService elasticRESTService;
    private static FirebaseRESTService firebaseRESTService;

    /*timeout values in seconds*/
    public static final int CONNECTION_TIMEOUT = 5;
    public static final int WRITE_TIMEOUT = 5;
    public static final int READ_TIMEOUT = 10;


    public static RESTService getRESTService() {
        if (restService == null) {
            synchronized (NetworkManager.class) {
                if (restService == null) {

                    OkHttpClient client = new OkHttpClient.Builder()
                            .addInterceptor(new RESTInterceptor())
                            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                            .build();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ConstantsAPI.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(client)
                            .build();

                    restService = retrofit.create(RESTService.class);
                }
            }
        }
        return restService;
    }


    public static FirebaseRESTService getFirebaseRESTService() {
        if (firebaseRESTService == null) {
            synchronized (NetworkManager.class) {
                if (firebaseRESTService == null) {

                    OkHttpClient client = new OkHttpClient.Builder()
                            .addInterceptor(new RESTInterceptor())
                            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                            .build();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ConstantsFireBase.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(client)
                            .build();

                    firebaseRESTService = retrofit.create(FirebaseRESTService.class);
                }
            }
        }
        return firebaseRESTService;
    }


    public static ElasticRESTService getElasticRESTService() {
        if (elasticRESTService == null) {
            synchronized (NetworkManager.class) {
                if (elasticRESTService == null) {

                    OkHttpClient client = new OkHttpClient.Builder()
                            .addInterceptor(new RESTInterceptor())
                            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                            .addInterceptor(new Authentication())
                            .build();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ConstantsAPI.BASE_ELASTIC_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(client)
                            .build();

                    elasticRESTService = retrofit.create(ElasticRESTService.class);
                }
            }
        }
        return elasticRESTService;
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private static class RESTInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Buffer buffer = new Buffer();
            if (request.body() != null)
                request.body().writeTo(buffer);

            Log.i("HTTP Request", "Request to " + request.url().toString()
                    + "\n" + request.headers().toString()
                    + "\n" + buffer.readUtf8());

            long t1 = System.nanoTime();
            Response response = chain.proceed(request);
            long t2 = System.nanoTime();

            String msg = response.body().string();
            msg = msg.replace("\r", ""); // Note: Messages with '\r' not displayed correctly in logcat

            Log.i("HTTP Response", String.format("Response from %s in %.1fms%n\n%s",
                    response.request().url().toString(), (t2 - t1) / 1e6d, msg));

            Log.i("HTTP Response", "Response code = " + response.code());

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), msg))
                    .build();
        }
    }
}
