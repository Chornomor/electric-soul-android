package com.electricsoul.android.network;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by alexchern on 07.02.17.
 */

public class Authentication implements Interceptor {

    private static String USER_NAME = "elastic";
    private static String PASSWORD = "Y4aJdl3l7IqcgOUPCFuVGLWX";
    private String authToken;


    public Authentication() {
        authToken = Credentials.basic(USER_NAME, PASSWORD);
    }


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .header("Authorization", authToken);

        Request request = builder.build();
        return chain.proceed(request);
    }
}
