package com.electricsoul.android.network.clients;

import com.electricsoul.android.model.elasticModel.EventResultModelElastic;
import com.electricsoul.android.model.elasticModel.EventSearchRequestModel;
import com.electricsoul.android.model.elasticModel.UserResultModelElastic;
import com.electricsoul.android.model.elasticModel.UserSearchRequestModel;
import com.electricsoul.android.network.NetworkManager;

import retrofit2.Callback;

/**
 * Created by alexchern on 20.01.17.
 */

public class ElasticRESTClient {

    private static ElasticRESTClient instance;


    private ElasticRESTClient() {
    }


    public static synchronized ElasticRESTClient getInstance() {
        if (instance == null) {
            synchronized (RESTClient.class) {
                if (instance == null) {
                    instance = new ElasticRESTClient();
                }
            }
        }
        return instance;
    }


    public void searchUsersByWord(UserSearchRequestModel searchModel, Callback<UserResultModelElastic> callback) {
        NetworkManager.getElasticRESTService().requestSearchUsers(searchModel).enqueue(callback);
    }


    public void searchEventsByWord(EventSearchRequestModel searchModel, Callback<EventResultModelElastic> callback) {
        NetworkManager.getElasticRESTService().requestSearchEvents(searchModel).enqueue(callback);
    }
}
