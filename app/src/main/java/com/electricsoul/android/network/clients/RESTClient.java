package com.electricsoul.android.network.clients;

import com.electricsoul.android.model.CommentResult;
import com.electricsoul.android.model.DetailEventModel;
import com.electricsoul.android.model.EventResultModel;
import com.electricsoul.android.model.FanImageResult;
import com.electricsoul.android.model.FollowerResult;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.model.Likes.EventCoverImageLike;
import com.electricsoul.android.model.Likes.EventImageLike;
import com.electricsoul.android.model.Likes.FanImageLike;
import com.electricsoul.android.model.Likes.PromoterImageLike;
import com.electricsoul.android.model.Likes.UserCoverImageLike;
import com.electricsoul.android.model.Likes.UserImageLike;
import com.electricsoul.android.model.LocationModel;
import com.electricsoul.android.model.PromoterResult;
import com.electricsoul.android.model.TestTicketingRequest;
import com.electricsoul.android.model.TicketingRequest;
import com.electricsoul.android.model.TicketsModel;
import com.electricsoul.android.model.UserResult;
import com.electricsoul.android.network.NetworkManager;
import com.electricsoul.android.model.LocationsResultModel;
import com.electricsoul.android.model.SearchResultModel;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.utils.DateUtil;
import com.facebook.AccessToken;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;

/**
 * Created by eugenetroyanskii on 11.11.16.
 */

public class RESTClient {

    private static RESTClient instance;


    private RESTClient() {
    }


    public static synchronized RESTClient getInstance() {
        if (instance == null) {
            synchronized (RESTClient.class) {
                if (instance == null) {
                    instance = new RESTClient();
                }
            }
        }
        return instance;
    }


    public void getCities(final Callback<LocationsResultModel> callback) {
        NetworkManager.getRESTService().getCities().enqueue(callback);
    }


    public void requestSignUp(String email, String userName, String password, String fireBaseToken,
                              File photo, File imageCower, final Callback<JSONObject> callback) {
        MultipartBody.Part photoBody = null;
        if (photo != null) {
            RequestBody requestBodyPhoto = RequestBody.create(MediaType.parse("image/png"), photo);
            photoBody = MultipartBody.Part.createFormData("image", photo.getName(), requestBodyPhoto);
        }

//        MultipartBody.Part coverImageBody;
//        RequestBody requestBodyCoverImg = RequestBody.create(MediaType.parse("image/png"), imageCower);
//        coverImageBody = MultipartBody.Part.createFormData("cover_image", imageCower.getName(), requestBodyCoverImg);

        RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), userName);
        RequestBody passwordBody = RequestBody.create(MediaType.parse("text/plain"), password);
        NetworkManager.getRESTService().register(emailBody, nameBody, passwordBody, fireBaseToken, photoBody).enqueue(callback);
    }


    public void requestSignUpWithFb(String email, String userName, String password, String fireBaseToken
            , String fbAuthToken, File photo, File imageCower, final Callback<JSONObject> callback) {
        MultipartBody.Part photoBody = null;
        if (photo != null) {
            RequestBody requestBodyPhoto = RequestBody.create(MediaType.parse("image/png"), photo);
            photoBody = MultipartBody.Part.createFormData("image", photo.getName(), requestBodyPhoto);
        }

//        MultipartBody.Part coverImageBody;
//        RequestBody requestBodyCoverImg = RequestBody.create(MediaType.parse("image/png"), imageCower);
//        coverImageBody = MultipartBody.Part.createFormData("cover_image", imageCower.getName(), requestBodyCoverImg);

        RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), userName);
//        RequestBody fireBase = RequestBody.create(MediaType.parse("text/plain"), fireBaseToken);
        RequestBody fbToken = RequestBody.create(MediaType.parse("text/plain"), fbAuthToken);
        RequestBody passwordBody = RequestBody.create(MediaType.parse("text/plain"), password);
        NetworkManager.getRESTService().registerWithFb(emailBody, nameBody, passwordBody, fireBaseToken, fbToken, photoBody).enqueue(callback);
    }


    public void postUpdatePassword(String authToken, String oldPassword, String newPassword, String newPasswordConfirm, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postUpdatePassword(authToken, oldPassword, newPassword, newPasswordConfirm).enqueue(callback);
    }


    public void postPasswordReset(String email, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postPasswordReset(email).enqueue(callback);
    }


    public void postResendEmailActivation(String email, Callback<String> callback) {
        NetworkManager.getRESTService().postResendEmailActivation(email).enqueue(callback);
    }


    public void requestLogin(String name, String password, String deviceId, final Callback<UserModel> callback) {
        NetworkManager.getRESTService().login(name, password, deviceId).enqueue(callback);
    }


    public void requestFacebookLogin(String fireBaseToken, Callback<UserModel> callback) {
//        RequestBody fireBase = RequestBody.create(MediaType.parse("text/plain"), fireBaseToken);
        NetworkManager.getRESTService().fbLogin(AccessToken.getCurrentAccessToken().getToken(), fireBaseToken).enqueue(callback);
    }


    public void requestFacebookLogin(String fbToken, String fireBaseToken, Callback<UserModel> callback) {
        RequestBody fireBase = RequestBody.create(MediaType.parse("text/plain"), fireBaseToken);
        NetworkManager.getRESTService().fbLogin(fbToken, fireBaseToken).enqueue(callback);
    }


    public void requestLogout(String authToken, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().logout(authToken).enqueue(callback);
    }


    public void requestDeactivate(String token, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().deactivate(token).enqueue(callback);
    }


    public void getSiteInfo(Callback<JSONObject> callback) {
        NetworkManager.getRESTService().getSiteInfo().enqueue(callback);
    }


    public void getMe(String authToken, Callback<UserModel> callback) {
        NetworkManager.getRESTService().getMe(authToken).enqueue(callback);
    }


    public void postUpdateMe(String authToken, String userName, MultipartBody.Part image, MultipartBody.Part coverImage, Callback<JsonObject> callback) {
        RequestBody userNameBody = RequestBody.create(MediaType.parse("text/plain"), userName);
        NetworkManager.getRESTService().postUpdateMe(authToken, userNameBody, image, coverImage).enqueue(callback);

    }


    public void getMyEvents(String authToken, int pageNumber, Callback<EventResultModel> callback) {
        NetworkManager.getRESTService().getMyEvents(authToken, pageNumber + "").enqueue(callback);
    }


    public void getUserEventsById(String userId, int pageNumber, Callback<EventResultModel> callback) {
        NetworkManager.getRESTService().getUserEventsById(userId, pageNumber + "", "-start").enqueue(callback);
    }


    public void getUserFanImages(String userId, String userToken, Callback<FanImageResult> callback) {
        NetworkManager.getRESTService().getUserFanImageById(userToken, userId).enqueue(callback);
    }


    public void getEventFanImageById(String userId, Callback<FanImageResult> callback) {
        NetworkManager.getRESTService().getEventFanImageById(userId).enqueue(callback);
    }


    public void getEventTicketsById(String evetntId, Callback<ArrayList<TicketsModel>> callback) {
        NetworkManager.getRESTService().getEventTicketsById(evetntId).enqueue(callback);
    }


    public void buyTicket(String authToken, TicketingRequest tickets, Callback<JsonObject> callback) {
        NetworkManager.getRESTService().postBuyTicket(authToken, tickets).enqueue(callback);
    }


    public void buyTestTicket(String authToken, TestTicketingRequest tickets, Callback<JsonObject> callback) {
        NetworkManager.getRESTService().postBuyTicket(authToken, tickets).enqueue(callback);
    }


    public void rsvpToEvent(String authToken, String eventId, boolean rsvp, Callback<Object> callback) {
        NetworkManager.getRESTService().postRSVPEventById(authToken, eventId, rsvp).enqueue(callback);
    }


    public void getUsersAttendingToEventById(String eventId, int pageNumber, Callback<UserResult> callback) {
        NetworkManager.getRESTService().getUsersAttendingToEventById(eventId, pageNumber + "").enqueue(callback);
    }


    public void getPromoterImagesById(String eventId, Callback<PromoterResult> callback) {
        NetworkManager.getRESTService().getPromoterImagesById(eventId).enqueue(callback);
    }


    public void uploadFanImageToEvent(String authToken, String eventId, File image, Callback<FanImageModel> callback) {
        MultipartBody.Part imageBody;
        RequestBody requestBodyPhoto = RequestBody.create(MediaType.parse("image/png"), image);
        imageBody = MultipartBody.Part.createFormData("image", image.getName(), requestBodyPhoto);

        NetworkManager.getRESTService().uploadFanImageToEvent(authToken, eventId, imageBody).enqueue(callback);
    }


    public void deleteFanImage(String authToken, String imageId, Callback<String> callback) {
        NetworkManager.getRESTService().deleteFanImage(authToken, imageId).enqueue(callback);
    }


    public void getCityById(String cityId, Callback<LocationModel> callback) {
        NetworkManager.getRESTService().getCityById(cityId).enqueue(callback);
    }


    public void postEventCoverImageLike(String eventImageId, String userId, String isLike, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postEventCoverImageLike(eventImageId, userId, isLike).enqueue(callback);
    }


    public void getCoverImageLikesByCoverId(String coverImageId, Callback<EventCoverImageLike> callback) {
        NetworkManager.getRESTService().getCoverImageLikesByCoverId(coverImageId).enqueue(callback);
    }


    public void postEventImageLike(String eventId, String userId, String isLike, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postEventImageLike(eventId, userId, isLike).enqueue(callback);
    }


    public void getEventLikesByEventId(String token, String eventId, Callback<EventImageLike> callback) {
        NetworkManager.getRESTService().getEventLikesByEventId(token, eventId).enqueue(callback);
    }


    public void postFanImageLike(String fanImageId, String userId, String isLike, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postFanImageLike(fanImageId, userId, isLike).enqueue(callback);
    }


    public void getFanImageLikesByFanImageId(String token, String fanImageId, Callback<FanImageLike> callback) {
        NetworkManager.getRESTService().getFanImageLikesByFanImageId(token, fanImageId).enqueue(callback);
    }


    public void postPromoterImageLike(String promoterImageId, String userId, String isLike, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postPromoterImageLike(promoterImageId, userId, isLike).enqueue(callback);
    }


    public void getPromoterImageLikesByImageId(String promoterImageId, Callback<PromoterImageLike> callback) {
        NetworkManager.getRESTService().getPromoterImageLikesByImageId(promoterImageId).enqueue(callback);
    }


    public void postUserCoverImageLike(String likedUserId, String userId, String isLike, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postUserCoverImageLike(likedUserId, userId, isLike).enqueue(callback);
    }


    public void getUserCoverImageLikesByUserId(String userId, Callback<UserCoverImageLike> callback) {
        NetworkManager.getRESTService().getUserCoverImageLikesByUserId(userId).enqueue(callback);
    }


    public void postUserImageLike(String likedUserId, String userId, String isLike, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postUserImageLike(likedUserId, userId, isLike).enqueue(callback);
    }


    public void getUserImageLikesByUserId(String userId, Callback<UserImageLike> callback) {
        NetworkManager.getRESTService().getUserImageLikesByUserId(userId).enqueue(callback);
    }


    public void postFollowUser(String userToken, String isLike, String followingUserId, String followerId, Callback<String> callback) {
        NetworkManager.getRESTService().postFollowUser(userToken, isLike, followingUserId, followerId).enqueue(callback);
    }


    public void getFollowingUsersByUserId(String userId, int page, Callback<FollowerResult> callback) {
        NetworkManager.getRESTService().getFollowingUsersByUserId(userId, String.valueOf(page)).enqueue(callback);
    }


    public void getFollowerUsersByUserId(String userId, int page, Callback<FollowerResult> callback) {
        NetworkManager.getRESTService().getFollowerUsersByUserId(userId, String.valueOf(page)).enqueue(callback);
    }


    // FIXME: 22.11.16
    public void postEventCommentWithImage(String authToken, String eventId, String commentText, MultipartBody.Part imageToComment, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postEventCommentWithImage(authToken, eventId, commentText, imageToComment).enqueue(callback);
    }


    public void postEventComment(String authToken, String eventId, String commentText, String mentionedUsers, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postEventComment(authToken, eventId, commentText, mentionedUsers).enqueue(callback);
    }


    public void getEventComments(String authToken, String eventId, Callback<CommentResult> callback) {
        NetworkManager.getRESTService().getEventComments(authToken, eventId).enqueue(callback);
    }


    public void likeEventComment(String authToken, String eventCommentId, Callback<Object> callback) {
        NetworkManager.getRESTService().likeEventComment(authToken, eventCommentId).enqueue(callback);
    }


    public void postFanImageComment(String fanImageCommentId, String commentText, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postFanImageComment(fanImageCommentId, commentText).enqueue(callback);
    }


    // FIXME: 22.11.16
    public void postFanImageCommentWithImage(String fanImageCommentId, String commentText, MultipartBody.Part imageToComment, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().postFanImageCommentWithImage(fanImageCommentId, commentText, imageToComment).enqueue(callback);
    }


    public void getFanImageComment(String eventId, Callback<CommentResult> callback) {
        NetworkManager.getRESTService().getFanImageComment(eventId).enqueue(callback);
    }


    public void likeFanImageComment(String fanImageCommentId, Callback<JSONObject> callback) {
        NetworkManager.getRESTService().likeFanImageComment(fanImageCommentId).enqueue(callback);
    }


    public void searchEventsByWord(String word, int pageCount, String isFestival, final Callback<SearchResultModel> callback) {
        NetworkManager.getRESTService().requestSearchEvents(word, pageCount + "", isFestival).enqueue(callback);
    }


    public void searchUsersByWord(String word, int pageCount, final Callback<SearchResultModel> callback) {
        NetworkManager.getRESTService().requestSearchUsers(word, pageCount + "").enqueue(callback);
    }


    public void getEventById(String eventId, final Callback<DetailEventModel> callback) {
        NetworkManager.getRESTService().getEventById(eventId).enqueue(callback);
    }


    public void getUserById(String userId, final Callback<UserModel> callback) {
        NetworkManager.getRESTService().getUserById(userId).enqueue(callback);
    }


    public void getEventsByCity(String cityId, String isFestival, int lastPageNumber, final Callback<EventResultModel> callback) {
        NetworkManager.getRESTService().getCityEvents(cityId, isFestival, lastPageNumber + "", DateUtil.getYesterdayDateInMSec()).enqueue(callback);
    }


    public void getFestivalsByCity(String cityId, String isFestival, final Callback<EventResultModel> callback) {
        NetworkManager.getRESTService().getCityFestivals(cityId, isFestival, DateUtil.getYesterdayDateInMSec()).enqueue(callback);
    }


    public void getFollowingById(String userId, int page, final Callback<FollowerResult> callback) {
        NetworkManager.getRESTService().getFollowingUsersByUserId(userId, String.valueOf(page)).enqueue(callback);
    }


    public void getFollowersById(String userId, int page, final Callback<FollowerResult> callback) {
        NetworkManager.getRESTService().getFollowerUsersByUserId(userId, String.valueOf(page)).enqueue(callback);
    }
}
