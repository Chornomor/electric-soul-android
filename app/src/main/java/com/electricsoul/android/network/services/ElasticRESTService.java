package com.electricsoul.android.network.services;

import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.model.elasticModel.EventResultModelElastic;
import com.electricsoul.android.model.elasticModel.EventSearchRequestModel;
import com.electricsoul.android.model.elasticModel.UserResultModelElastic;
import com.electricsoul.android.model.elasticModel.UserSearchRequestModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by alexchorn on 20.01.17.
 */

public interface ElasticRESTService {

    @POST(ConstantsAPI.USERS + ConstantsAPI.SEARCH_ELASTIC)
    Call<UserResultModelElastic> requestSearchUsers(@Body UserSearchRequestModel searchModel);


    @POST(ConstantsAPI.EVENTS_SLASH + ConstantsAPI.SEARCH_ELASTIC)
    Call<EventResultModelElastic> requestSearchEvents(@Body EventSearchRequestModel searchModel);
}
