package com.electricsoul.android.network.services;

import com.electricsoul.android.constants.ConstantsAPI;
import com.electricsoul.android.model.CommentResult;
import com.electricsoul.android.model.DetailEventModel;
import com.electricsoul.android.model.EventResultModel;
import com.electricsoul.android.model.FanImageResult;
import com.electricsoul.android.model.FollowerResult;
import com.electricsoul.android.model.Likes.EventCoverImageLike;
import com.electricsoul.android.model.Likes.EventImageLike;
import com.electricsoul.android.model.Likes.FanImageLike;
import com.electricsoul.android.model.Likes.PromoterImageLike;
import com.electricsoul.android.model.Likes.UserCoverImageLike;
import com.electricsoul.android.model.Likes.UserImageLike;
import com.electricsoul.android.model.LocationModel;
import com.electricsoul.android.model.LocationsResultModel;
import com.electricsoul.android.model.PromoterResult;
import com.electricsoul.android.model.SearchResultModel;
import com.electricsoul.android.model.FanImageModel;
import com.electricsoul.android.model.TestTicketingRequest;
import com.electricsoul.android.model.TicketingRequest;
import com.electricsoul.android.model.TicketsModel;
import com.electricsoul.android.model.UserModel;
import com.electricsoul.android.model.UserResult;
import com.google.gson.JsonObject;


import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by eugenetroyanskii on 11.11.16.
 */

public interface RESTService {

    @Multipart
    @POST(ConstantsAPI.AUTH_REGISTER)
    Call<JSONObject> register(@Part("email") RequestBody email,
                              @Part("username") RequestBody userName,
                              @Part("password") RequestBody password,
                              @Query("device_id") String fireBaseToken,
                              @Part MultipartBody.Part image);


    @Multipart
    @POST(ConstantsAPI.AUTH_REGISTER)
    Call<JSONObject> registerWithFb(@Part("email") RequestBody email,
                                    @Part("username") RequestBody userName,
                                    @Part("password") RequestBody password,
                                    @Query("device_id") String fireBaseToken,
                                    @Part("fb_auth_token") RequestBody fbToken,
                                    @Part MultipartBody.Part image);


    @FormUrlEncoded
    @POST(ConstantsAPI.LOGIN)
    Call<UserModel> login(@Field("username") String name,
                          @Field("password") String password,
                          @Field("device_id") String deviceId);


    @FormUrlEncoded
    @POST(ConstantsAPI.FB_LOGIN)
    Call<UserModel> fbLogin(@Field("fb_auth_token") String token,
                            @Field("device_id") String fireBaseToken);


    //// FIXME: 14.11.16
    @POST(ConstantsAPI.LOGIOUT)
    Call<JSONObject> logout(@Header("Authorization") String token);


    @POST(ConstantsAPI.ME + ConstantsAPI.DEACTIVATE)
    Call<JSONObject> deactivate(@Header("Authorization") String token);


    @GET(ConstantsAPI.SITE_INFO)
    Call<JSONObject> getSiteInfo();


    @GET(ConstantsAPI.ME)
    Call<UserModel> getMe(@Header("Authorization") String token);


    @Multipart
    @POST(ConstantsAPI.ME + ConstantsAPI.UPDATE)
    Call<JsonObject> postUpdateMe(@Header("Authorization") String token,
                                  @Part("username") RequestBody userName,
                                  @Part MultipartBody.Part image,
                                  @Part MultipartBody.Part coverImage);


    @FormUrlEncoded
    @POST(ConstantsAPI.ME + ConstantsAPI.PASSWORD_CHANGE)
    Call<JSONObject> postUpdatePassword(@Header("Authorization") String token,
                                        @Field("old_password") String oldPassword,
                                        @Field("new_password1") String newPassword,
                                        @Field("new_password2") String newPasswordConfirm);


    @FormUrlEncoded
    @POST(ConstantsAPI.PASSWORD_RESET)
    Call<JSONObject> postPasswordReset(@Field("email") String email);


    @FormUrlEncoded
    @POST(ConstantsAPI.RESEND_EMAIL_ACTIVATION)
    Call<String> postResendEmailActivation(@Field("email") String email);


    @GET(ConstantsAPI.ME + ConstantsAPI.SLASH_EVENTS)
    Call<EventResultModel> getMyEvents(@Header("Authorization") String token,
                                       @Query("page") String pageNumber);


    @GET(ConstantsAPI.USERS + "{id}" + ConstantsAPI.SLASH_EVENTS)
    Call<EventResultModel> getUserEventsById(@Path("id") String userId,
                                             @Query("page") String pageCount,
                                             @Query("order") String order);


    @GET(ConstantsAPI.USERS + "{id}" + ConstantsAPI.FANIMAGES)
    Call<FanImageResult> getUserFanImageById(@Header("Authorization") String token,
                                             @Path("id") String userId);


    @GET(ConstantsAPI.EVENTS_SLASH + "{id}" + ConstantsAPI.SLASH_FAN_IMAGES)
    Call<FanImageResult> getEventFanImageById(@Path("id") String eventId);


    @GET(ConstantsAPI.EVENTS_SLASH + "{id}" + ConstantsAPI.SLASH_TICKETS)
    Call<ArrayList<TicketsModel>> getEventTicketsById(@Path("id") String eventId);


    @POST(ConstantsAPI.TICKETS_SLASH + ConstantsAPI.BUY)
    Call<JsonObject> postBuyTicket(@Header("Authorization") String authToken
                                    , @Body TicketingRequest tickets);

    @POST(ConstantsAPI.TICKETS_SLASH + ConstantsAPI.BUY)
    Call<JsonObject> postBuyTicket(@Header("Authorization") String authToken
                                    , @Body TestTicketingRequest tickets);


    @GET(ConstantsAPI.EVENTS_SLASH + "{id}" + ConstantsAPI.SLASH_FAN_IMAGES)
    Call<FanImageModel> getEventFanImageForUserById(@Path("id") String eventId,
                                                    @Query("user") String userId);


    @FormUrlEncoded
    @POST(ConstantsAPI.EVENTS_SLASH + "{id}" + ConstantsAPI.RSVP)
    Call<Object> postRSVPEventById(@Header("Authorization") String token,
                                   @Path("id") String eventId,
                                   @Field("rsvp") boolean isRsvp);


    @GET(ConstantsAPI.EVENTS_SLASH + "{id}" + ConstantsAPI.EVENT_ATTENDEES)
    Call<UserResult> getUsersAttendingToEventById(@Path("id") String eventId,
                                                  @Query("page") String pageCount);


    @GET(ConstantsAPI.EVENTS_SLASH + "{id}" + ConstantsAPI.PROMOTER_IMAGES)
    Call<PromoterResult> getPromoterImagesById(@Path("id") String eventId);


    @Multipart
    @POST(ConstantsAPI.EVENTS_SLASH + "{id}" + ConstantsAPI.UPLOAD)
    Call<FanImageModel> uploadFanImageToEvent(@Header("Authorization") String token,
                                              @Path("id") String eventId,
                                              @Part MultipartBody.Part image);


    @DELETE(ConstantsAPI.FAN_IMAGES_SLASH + "{id}")
    Call<String> deleteFanImage(@Header("Authorization") String token,
                                @Path("id") String imageId);


    @GET(ConstantsAPI.CITY + "{id}")
    Call<LocationModel> getCityById(@Path("id") String cityId);


    /*
    * @param isLike must be
    * {@value 1} if it's like
    * or
    * {@value 0} if it's dislike
    * */
    @FormUrlEncoded
    @POST(ConstantsAPI.EVENT_COVER_IMAGE_LIKE + "001" + ConstantsAPI.LIKE)
    Call<JSONObject> postEventCoverImageLike(@Field("event") String eventImageId,
                                             @Field("user_id") String userId,
                                             @Field("is_like") String isLike);


    @GET(ConstantsAPI.EVENT_COVER_IMAGE_LIKE + "{id}" + ConstantsAPI.GET_LIKES)
    Call<EventCoverImageLike> getCoverImageLikesByCoverId(@Path("id") String eventId);


    /*
    * @param isLike must be
    * {@value 1} if it's like
    * or
    * {@value 0} if it's dislike
    * */
    @FormUrlEncoded
    @POST(ConstantsAPI.EVENT_LIKES + "001" + ConstantsAPI.SLASH_LIKE)
    Call<JSONObject> postEventImageLike(@Field("event_image_pk") String eventId,
                                        @Field("user_id") String userId,
                                        @Field("is_like") String isLike);


    @GET(ConstantsAPI.EVENT_LIKES + "{id}" + ConstantsAPI.GET_PROMOTER_LIKES)
    Call<EventImageLike> getEventLikesByEventId(@Header("Authorization") String token,
                                                @Path("id") String eventId);


    /*
    * @param isLike must be
    * {@value 1} if it's like
    * or
    * {@value 0} if it's dislike
    * */
    @FormUrlEncoded
    @POST(ConstantsAPI.FAN_LIKES + "001" + ConstantsAPI.SLASH_LIKE)
    Call<JSONObject> postFanImageLike(@Field("fan_image_pk") String fanImageId,
                                      @Field("user_id") String userId,
                                      @Field("is_like") String isLike);


    @GET(ConstantsAPI.FAN_LIKES + "{id}" + ConstantsAPI.GET_LIKES)
    Call<FanImageLike> getFanImageLikesByFanImageId(@Header("Authorization") String token,
                                                    @Path("id") String fanImageId);


    /*
    * @param isLike must be
    * {@value 1} if it's like
    * or
    * {@value 0} if it's dislike
    * */
    @FormUrlEncoded
    @POST(ConstantsAPI.PROMOTER_LIKES + "001" + ConstantsAPI.SLASH_LIKE)
    Call<JSONObject> postPromoterImageLike(@Field("promoter") String promoterImageId,
                                           @Field("user_id") String userId,
                                           @Field("is_like") String isLike);


    @GET(ConstantsAPI.PROMOTER_LIKES + "{id}" + ConstantsAPI.GET_PROMOTER_LIKES)
    Call<PromoterImageLike> getPromoterImageLikesByImageId(@Path("id") String promoterImageId);


    /*
    * @param isLike must be
    * {@value 1} if it's like
    * or
    * {@value 0} if it's dislike
    * */
    @FormUrlEncoded
    @POST(ConstantsAPI.USER_COVER_IMAGE_LIKES + "001" + ConstantsAPI.SLASH_LIKE)
    Call<JSONObject> postUserCoverImageLike(@Field("is_like") String isLike,
                                            @Field("user_pk") String likedUserId,
                                            @Field("user_id") String userId);


    @GET(ConstantsAPI.USER_COVER_IMAGE_LIKES + "{id}" + ConstantsAPI.GET_LIKES)
    Call<UserCoverImageLike> getUserCoverImageLikesByUserId(@Path("id") String userId);


    /*
    * @param isLike must be
    * {@value 1} if it's like
    * or
    * {@value 0} if it's dislike
    * */
    @FormUrlEncoded
    @POST(ConstantsAPI.USER_LIKES + "001" + ConstantsAPI.SLASH_LIKE)
    Call<JSONObject> postUserImageLike(@Field("user_image_pk") String likedUserId,
                                       @Field("user_id") String userId,
                                       @Field("is_like") String isLike);


    @GET(ConstantsAPI.USER_LIKES + "{id}" + ConstantsAPI.GET_LIKES)
    Call<UserImageLike> getUserImageLikesByUserId(@Path("id") String userId);


    /*
    * @param isFollow must be
    * {@value 1} if it's follow
    * or
    * {@value 0} if it's unfollow
    * */
    @FormUrlEncoded
    @POST(ConstantsAPI.FOLLOW_USER + "001" + ConstantsAPI.FOLLOW)
    Call<String> postFollowUser(@Header("Authorization") String token,
                                @Field("is_follow") String isFollow,
                                @Field("user_pk") String followingUserId,
                                @Field("user_id") String followerId);


    @GET(ConstantsAPI.FOLLOW_USER + "{id}" + ConstantsAPI.GET_FOLLOWING_NAMES)
    Call<FollowerResult> getFollowingUsersByUserId(@Path("id") String userId,
                                                   @Query("page") String pageCount);


    @GET(ConstantsAPI.FOLLOW_USER + "{id}" + ConstantsAPI.GET_FOLLOWER_NAMES)
    Call<FollowerResult> getFollowerUsersByUserId(@Path("id") String userId,
                                                  @Query("page") String pageCount);


    @FormUrlEncoded
    @POST(ConstantsAPI.EVENT_COMMENT + "{id}" + ConstantsAPI.SEND)
    Call<JSONObject> postEventCommentWithImage(@Header("Authorization") String token,
                                               @Path("id") String eventId,
                                               @Field("text") String commentText,
                                               @Part("image") MultipartBody.Part imageToComment);


    @FormUrlEncoded
    @POST(ConstantsAPI.EVENT_COMMENT + "{id}" + ConstantsAPI.SEND)
    Call<JSONObject> postEventComment(@Header("Authorization") String token,
                                      @Path("id") String eventId,
                                      @Field("text") String commentText,
                                      @Field("mentioned_users") String mentionedUsers);


    @GET(ConstantsAPI.GET_EVENT_COMMENT)
    Call<CommentResult> getEventComments(@Header("Authorization") String token,
                                         @Query("event_id") String eventId);


    @POST(ConstantsAPI.EVENT_COMMENT_LIKE + "{id}" + ConstantsAPI.SLASH_LIKE)
    Call<Object> likeEventComment(@Header("Authorization") String token,
                                  @Path("id") String eventCommentId);


    @POST(ConstantsAPI.FAN_IMAGE_COMMENT_SHASH + "{id}" + ConstantsAPI.SEND)
    Call<JSONObject> postFanImageComment(@Path("id") String fanImageCommentId,
                                         @Query("text") String commentText);


    @POST(ConstantsAPI.FAN_IMAGE_COMMENT_SHASH + "{id}" + ConstantsAPI.SEND)
    Call<JSONObject> postFanImageCommentWithImage(@Path("id") String fanImageCommentId,
                                                  @Query("text") String commentText,
                                                  @Part("image") MultipartBody.Part imageToComment);


    @GET(ConstantsAPI.FAN_IMAGE_COMMENT)
    Call<CommentResult> getFanImageComment(@Query("id") String eventId);


    @POST(ConstantsAPI.FAN_IMAGE_COMMENT_SHASH + "{id}" + ConstantsAPI.SLASH_LIKE)
    Call<JSONObject> likeFanImageComment(@Path("id") String fanImageCommentId);


    @GET(ConstantsAPI.CITIES)
    Call<LocationsResultModel> getCities();


    @GET(ConstantsAPI.SEARCH + "word" + ConstantsAPI.GET_USER)
    Call<SearchResultModel> requestSearchUsers(@Query("search") String word,
                                               @Query("page") String pageCount);


    @GET(ConstantsAPI.SEARCH + "word" + ConstantsAPI.GET_EVENT)
    Call<SearchResultModel> requestSearchEvents(@Query("search") String word,
                                                @Query("page") String pageCount,
                                                @Query("is_festival") String isFestival);


    @GET(ConstantsAPI.USERS + "{id}")
    Call<UserModel> getUserById(@Path("id") String id);


    @GET(ConstantsAPI.EVENTS_SLASH + "{id}")
    Call<DetailEventModel> getEventById(@Path("id") String eventId);


    @GET(ConstantsAPI.EVENTS)
    Call<EventResultModel> getCityEvents(@Query("city") String cityId,
                                         @Query("is_festival") String isFestival,
                                         @Query("page") String pageCount,
                                         @Query("end_after") String endAfter);


    @GET(ConstantsAPI.EVENTS)
    Call<EventResultModel> getCityFestivals(@Query("city") String cityId,
                                            @Query("is_festival") String isFestival,
                                            @Query("end_after") String endAfter);
}
