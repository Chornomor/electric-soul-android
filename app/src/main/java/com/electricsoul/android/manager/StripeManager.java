package com.electricsoul.android.manager;

import android.content.DialogInterface;
import android.view.View;
import android.widget.LinearLayout;

import com.electricsoul.android.R;
import com.electricsoul.android.ui.base.BaseActivity;
import com.electricsoul.android.ui.dialog.DialogManager;
import com.electricsoul.android.utils.LogUtil;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

/**
 * Created by alexchern on 07.03.17.
 */

public class StripeManager {

    private BaseActivity mActivity;
    private CardInputWidget mCardInputWidget;
    private String mPublishableKey;
    private StripeCallback stripeCallback;


    public StripeManager(BaseActivity activity, CardInputWidget mCardInputWidget) {
        this.mCardInputWidget = mCardInputWidget;
        this.mActivity = activity;
        mPublishableKey = mActivity.getString(R.string.stripe_test_key);
    }


    public void attachButton(LinearLayout button, StripeCallback callback) {
        stripeCallback = callback;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmPurchase();
            }
        });
    }


    private void confirmPurchase() {
        final Card cardToSave = mCardInputWidget.getCard();
        if (cardToSave == null) {
            DialogManager.showStripeErrorDialog(mActivity, mActivity.getString(R.string.invalid_card));
            return;
        } else {
            DialogManager.showSrtipePayConfirmDialog(mActivity, cardToSave, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getPoken(cardToSave);
                }
            });
        }
    }


    private void getPoken(Card cardToSave) {
        mActivity.showProgressDialog();
        new Stripe(mActivity.getApplicationContext()).createToken(
                cardToSave,
                mPublishableKey,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        stripeCallback.onSuccess(token.getId());
                        mActivity.cancelProgressDialog();
                    }


                    public void onError(Exception error) {
                        DialogManager.showStripeErrorDialog(mActivity, error.getLocalizedMessage());
                        mActivity.cancelProgressDialog();
                    }
                });
    }


    public interface StripeCallback {

        void onSuccess(String token);
    }
}
