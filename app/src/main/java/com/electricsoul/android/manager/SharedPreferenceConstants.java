package com.electricsoul.android.manager;

/**
 * Created by eugenetroyanskii on 17.11.16.
 */

public interface SharedPreferenceConstants {

    String PASSWORD = "password";
    String USER_ID = "user_id";
    String EMAIL = "email";
    String AUTH_TOKEN = "auth_token";
    String FB_TOKEN = "fb_token";
    String USER_NAME = "user_name";
    String PICKED_CITY_ID = "picked_city_id";
    String PICKED_CITY_CODE = "picked_city_code";

}
