package com.electricsoul.android.manager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by eugenetroyanskii on 17.11.16.
 */

public class SharedPrefManager {

    public static final String APP_PREFERENCES = "app_shared_pref";
    public static final String TOKEN = "Token ";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static SharedPrefManager instance = null;

    private SharedPrefManager() {
    }

    public static SharedPrefManager getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPrefManager();
            initSharedPreference(context);
        }
        return instance;
    }

    private static void initSharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void storePassword(String userPassword) {
        editor.putString(SharedPreferenceConstants.PASSWORD, userPassword).apply();
    }

    public String getStoredPassword() {
        return sharedPreferences.getString(SharedPreferenceConstants.PASSWORD, "");
    }

    public void storeEmail(String userPassword) {
        editor.putString(SharedPreferenceConstants.EMAIL, userPassword).apply();
    }

    public String getStoredLogin() {
        return sharedPreferences.getString(SharedPreferenceConstants.EMAIL, "");
    }

    public void storeUserName(String userName) {
        editor.putString(SharedPreferenceConstants.USER_NAME, userName).apply();
    }

    public String getStoredUserName() {
        return sharedPreferences.getString(SharedPreferenceConstants.USER_NAME, "");
    }

    public void storeUserId(String userId) {
        editor.putString(SharedPreferenceConstants.USER_ID, userId).apply();
    }

    public String getStoredUserId() {
        return sharedPreferences.getString(SharedPreferenceConstants.USER_ID, "");
    }

    public void storeAuthToken(String authToken) {
        editor.putString(SharedPreferenceConstants.AUTH_TOKEN, authToken).apply();
    }

    public String getStoredAuthToken() {
        return TOKEN + sharedPreferences.getString(SharedPreferenceConstants.AUTH_TOKEN, "");
    }

    public void storeFbToken(String fbToken) {
        editor.putString(SharedPreferenceConstants.FB_TOKEN, fbToken).apply();
    }

    public String getStoredFbToken() {
        return sharedPreferences.getString(SharedPreferenceConstants.FB_TOKEN, "");
    }

    public void storePickedCityId(String city) {
        editor.putString(SharedPreferenceConstants.PICKED_CITY_ID, city).apply();
    }

    public String getPickedCityId() {
        return sharedPreferences.getString(SharedPreferenceConstants.PICKED_CITY_ID, "");
    }

    public void storePickedCityCode(String cityCode) {
        editor.putString(SharedPreferenceConstants.PICKED_CITY_CODE, cityCode).apply();
    }

    public String getPickedCityCode() {
        return sharedPreferences.getString(SharedPreferenceConstants.PICKED_CITY_CODE, "");
    }

    public void clearStoredData() {
        storePassword("");
        storeEmail("");
        storeUserName("");
        storeUserId("");
        storeAuthToken("");
        storeFbToken("");
        storeEmail("");
    }
}
