package com.electricsoul.android.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexchern on 25.11.16.
 */

public class FacebookManager {

    private static FacebookManager instance = null;

    private Context context;

    private static final int ERROR_CODE = -1;
    private static final int GET_ME_TRIAL_NUMBER = 4;

    // PERMISSION
    private static final String PUBLIC_PROFILE = "public_profile";
    private static final String USER_FRIENDS = "user_friends";
    private static final String EMAIL = "email";
    private static final String USER_BIRTHDAY = "user_birthday";
    private static final String USER_LIKES = "user_likes";
    private static final String USER_PHOTOS = "user_photos";


    public enum LoginError {
        NOT_ALL_PERMISSIONS_GRANTED, CANCELED_BY_USER, FACEBOOK_ERROR
    }


    private CallbackManager mCallbackManager;
    private LoginListener mLoginListener;
    private Set<String> mNeededPermissions;


    public interface LoginListener {

        void onSuccess();


        void onError(LoginError error);
    }


    public static FacebookManager getInstance(Context context) {
        if (instance == null) {
            instance = new FacebookManager();
        }
        instance.context = context;
        return instance;
    }


    private FacebookManager() {
        mNeededPermissions = new HashSet<>(
                Arrays.asList(EMAIL));
//        mNeededPermissions = new HashSet<>(
//                Arrays.asList(PUBLIC_PROFILE, USER_FRIENDS, EMAIL, USER_BIRTHDAY, USER_LIKES, USER_PHOTOS));
        initSDK();
        initComponentsToLogin();
    }


    private void initSDK() {
        FacebookSdk.sdkInitialize(context);
        AppEventsLogger.activateApp(context);
    }


    private void initComponentsToLogin() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookLoginCallback());
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void login(Activity activity, LoginListener loginListener) {
        mLoginListener = loginListener;
        LoginManager.getInstance().logInWithReadPermissions(activity, mNeededPermissions);
    }


    public void logout() {
        LoginManager.getInstance().logOut();
    }


    private class FacebookLoginCallback implements FacebookCallback<LoginResult> {

        @Override
        public void onSuccess(LoginResult loginResult) {
            if (mLoginListener == null) return;
            if (loginResult.getRecentlyGrantedPermissions() != null && loginResult.getRecentlyGrantedPermissions().containsAll(mNeededPermissions)) {
                mLoginListener.onSuccess();
            } else {
                mLoginListener.onError(LoginError.NOT_ALL_PERMISSIONS_GRANTED);
            }
            logout();
        }


        @Override
        public void onCancel() {
            if (mLoginListener != null) {
                mLoginListener.onError(LoginError.CANCELED_BY_USER);
            }
        }


        @Override
        public void onError(FacebookException e) {
            mLoginListener.onError(LoginError.FACEBOOK_ERROR);
        }
    }

}
