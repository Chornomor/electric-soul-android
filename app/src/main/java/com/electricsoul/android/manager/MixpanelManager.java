package com.electricsoul.android.manager;

import android.content.Context;

import com.electricsoul.android.model.UserModel;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alexchern on 06.01.17.
 */

public class MixpanelManager {

    private static final String MIXPANEL_API_TOKEN = "cfba418b8ded80006a7e5e759bdfdd06";
    private static final String FCM_SENDER_TOKEN = "524813578943";

    private static MixpanelAPI mixpanel;


    public static void registerMixpanel(Context context, String userDeviceId, UserModel user) {
        mixpanel = MixpanelAPI.getInstance(context, MIXPANEL_API_TOKEN);
        MixpanelAPI.People people = mixpanel.getPeople();
        people.set(getUserData(user));
        people.identify(userDeviceId);
        people.initPushHandling(FCM_SENDER_TOKEN);
    }

    private static JSONObject getUserData(UserModel user){
        JSONObject property = new JSONObject();
        try {
            property.put("$username", user.getUserName());
            property.put("$email", user.getEmail());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return property;
    }
}
