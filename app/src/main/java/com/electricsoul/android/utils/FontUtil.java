package com.electricsoul.android.utils;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by yuliasokolova on 16.11.16.
 */


public class FontUtil {
    public static String FONT_KHULA_BOLD = "fonts/Khula-Bold.ttf";
    public static String FONT_KHULA_SEMI_BOLD = "fonts/khula_semibold.ttf";
    public static String FONT_KHULA_EXO_BOLD = "fonts/Exo-Bold.ttf";
    public static String FONT_KHULA_EXTRA_BOLD = "fonts/Khula-ExtraBold.ttf";
    public static String FONT_KHULA_EXTRA_BLACK = "fonts/Exo-Black.ttf";
    public static String FONT_RALEWAY_SEMI_BOLD = "fonts/Raleway-SemiBold.ttf";
    public static String FONT_RALEWAY_EXTRA_BOLD = "fonts/Raleway-ExtraBold.ttf";
    public static String FONT_RALEWAY_MEDIUM = "fonts/Raleway-Medium.ttf";
    public static String FONT_RALEWAY_BLACK = "fonts/Raleway-Black.ttf";


    public static void impl(ViewGroup view, Typeface typface) {
        for (int i = 0; i < view.getChildCount(); i++) {
            View v = view.getChildAt(i);
            if (v instanceof TextView)
                ((TextView) v).setTypeface(typface);
            else if (v instanceof ViewGroup)
                impl((ViewGroup) v, typface);
        }
    }
}