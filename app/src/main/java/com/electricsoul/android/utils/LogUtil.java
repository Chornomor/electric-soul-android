package com.electricsoul.android.utils;

import android.util.Log;

/**
 * Created by alexchern on 14.11.16.
 */

public class LogUtil {
    private static final String LOG_TAG_DEBUG = "DEBUG";


    public static void logE(String log) {
    Log.e(LOG_TAG_DEBUG, log);
}

    public static void logD(String log) {
        Log.d(LOG_TAG_DEBUG, log);
    }

    public static void logD(String tag, String log) {
        Log.d(tag, log);
    }
}
