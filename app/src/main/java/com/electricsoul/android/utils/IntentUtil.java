package com.electricsoul.android.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by yuliasokolova on 21.11.16.
 */

public class IntentUtil {


    public static void callSendIntent(Context context, String subject, String body, String title, String url){
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setType("message/rfc822");
        i.setData(Uri.parse("mailto:" + url));
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, body);
        try {
            context.startActivity(Intent.createChooser(i, title));
        } catch (android.content.ActivityNotFoundException ex) {
            if (context instanceof AppCompatActivity &&
                    ((AppCompatActivity) context).findViewById(android.R.id.content) != null) {
                Snackbar.make(((AppCompatActivity)context).findViewById(android.R.id.content),
                        "There are no email clients installed.", Snackbar.LENGTH_SHORT).show();
            }
        }
    }


    public static void callShareIntent(Context context, String subject, String body, String title) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(sharingIntent, title));
    }
}
