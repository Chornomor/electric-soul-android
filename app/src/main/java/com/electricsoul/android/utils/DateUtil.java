package com.electricsoul.android.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by alexchern on 16.11.16.
 */

public class DateUtil {

    private static final int SEC_IN_DAY = 86400;

    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String TICKET_END_DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    private static final String DATE_FORMAT = "MMM d";
    private static final String MONTH_FORMAT = "MMM";
    private static final String DAY_FORMAT = "d";
    private static final String FULL_DATE_FORMAT = "EEE, MMM d HH:mm";
    private static final String TIME_FORMAT = "HH:mm";
    private static final String COMPLETELY_FULL_DATE = "EEE, dd MMM yyyy";
    private static final String FULL_DATE_END_TICKITING = "dd MMM yyyy";

    public static final String DATE_PHOTO_NAME_FORMAT = "yy.MM.dd.mm.ss";

    public static String convertDate(String dateInString) {
        dateInString = removeMiliSec(dateInString);
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            date = format.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        return dateFormat.format(date);
    }

    public static String convertDateEndTicketing(String dateInString) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat(TICKET_END_DATE_FORMAT_PATTERN, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            date = format.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(FULL_DATE_END_TICKITING, Locale.getDefault());
        return dateFormat.format(date);
    }

    private static String removeMiliSec(String dateInString) {
        return dateInString.replace(dateInString.substring(dateInString.indexOf("."), dateInString.indexOf("Z") - 1), "");
    }


    public static String convertEventFullDate(String sStartDate, String sEndDate) {
        Date startDate = new Date();
        Date endDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            startDate = format.parse(sStartDate);
            endDate = format.parse(sEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat monthFormat = new SimpleDateFormat(MONTH_FORMAT, Locale.getDefault());
        SimpleDateFormat dayFormat = new SimpleDateFormat(DAY_FORMAT, Locale.getDefault());
        String fullDate = monthFormat.format(startDate);
        String startMonth = monthFormat.format(startDate);
        String endMonth = monthFormat.format(endDate);
        String startDay = dayFormat.format(startDate);
        String endDay = dayFormat.format(endDate);
        if (startMonth.equals(endMonth)) {
            if (startDay.equals(endDay)) {
                fullDate += " " + startDay;
            } else {
                fullDate += " " + startDay + " - " + endDay;
            }
        } else {
            fullDate += " " + startDay + " - " + endMonth + " " + endDay;
        }
        return fullDate;
    }


    public static String convertEventCompletelyFullDate(String sStartDate) {
        Date startDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            startDate = format.parse(sStartDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat monthFormat = new SimpleDateFormat(COMPLETELY_FULL_DATE, Locale.getDefault());
        String fullDate = monthFormat.format(startDate);
        return fullDate;
    }


    public static String convertFullDate(String sStartDate, String sEndDate) {
        Date startDate = new Date();
        Date endDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            startDate = format.parse(sStartDate);
            endDate = format.parse(sEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat fullDateFormat = new SimpleDateFormat(FULL_DATE_FORMAT, Locale.getDefault());
        SimpleDateFormat dayFormat = new SimpleDateFormat(DAY_FORMAT, Locale.getDefault());
        String fullDate = fullDateFormat.format(startDate);
        String startDay = dayFormat.format(startDate);
        String endDay = dayFormat.format(endDate);
        if (startDay.equals(endDay)) {
            fullDate += " - " + convertTime(sEndDate);
        } else {
            fullDate += " - " + convertFullDate(sEndDate);
        }
        return fullDate;
    }


    private static String convertFullDate(String dateInString) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            date = format.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(FULL_DATE_FORMAT, Locale.getDefault());
        return dateFormat.format(date);
    }


    public static String convertTime(String dateInString) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            date = format.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
        return dateFormat.format(date);
    }


    public static boolean isEventStarted(String dateInString) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            date = format.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long eventTime = date.getTime();
        long myTime = new Date().getTime();

        return (eventTime - myTime) < 0;
    }


    public static String getCurrentTime(String format) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(new Date());
    }


    public static String getYesterdayDateInMSec() {
        return (new Date().getTime()/1000 - SEC_IN_DAY) + "";
    }
}
