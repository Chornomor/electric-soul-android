package com.electricsoul.android.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.electricsoul.android.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.electricsoul.android.utils.GetPhotoUtil.REQ_GALLERY;
import static com.facebook.FacebookSdk.getApplicationContext;

public class PhotoUtil {
    public static final int REQUEST_IMAGE_GALLERY = 100;
    public static final int MAX_IMAGE_DIMENSION = 512;
    Uri fileContainer;
    Activity activity;


    public PhotoUtil(Activity activity) {
        this.activity = activity;
    }


    public boolean resolveResult(Activity a, int requestCode, int resultCode, Intent data, OnBitmapObtained listener) {
        if (resultCode != Activity.RESULT_OK)
            return false;
        if (requestCode != REQUEST_IMAGE_GALLERY)
            return false;
        new ConvertURItoBitmap(a, data, requestCode, listener).execute();
        return true;
    }


    public class ConvertURItoBitmap extends AsyncTask<Void, Void, Bitmap> {
        Activity activity;
        Intent data;
        int requestCode;
        OnBitmapObtained listener;
        Uri imageUri;


        public ConvertURItoBitmap(Activity activity, Intent data, int requestCode, OnBitmapObtained listener) {
            this.activity = activity;
            this.data = data;
            this.requestCode = requestCode;
            this.listener = listener;
        }


        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap bitmap = null;
            if (requestCode == REQUEST_IMAGE_GALLERY) {
                imageUri = data.getData();
                fileContainer = imageUri;
                try {
                    bitmap = BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(fileContainer));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return bitmap;
        }


        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            listener.onBitmapObtained(bitmap);
        }
    }


    public interface OnBitmapObtained {
        void onBitmapObtained(Bitmap bmp);
    }


    //save image
    public static void imageDownload(Fragment fragment, Activity activity, String url) {
        mContext = activity;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Marshmallow+
            int permission = activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permission == PackageManager.PERMISSION_GRANTED) {
                saveImage(url);
            } else {
                if (fragment != null) {
                    fragment.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQ_GALLERY);
                } else {
                    activity.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQ_GALLERY);
                }
            }
        } else {
            saveImage(url);
        }
    }


    private static void saveImage(String url) {
        showDownloadNotification();
        Picasso.with(mContext)
                .load(url)
                .into(getTarget());
    }


    //target to save
    private static Target getTarget() {
        Target target = new Target() {

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                storeImage(bitmap);
            }


            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }


            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }


    private static NotificationManager mNotifyManager;
    private static NotificationCompat.Builder mBuilder;
    private static final int id = 1;
    private static Context mContext;
    private static String ulr;


    private static void storeImage(final Bitmap image) {
        final File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            LogUtil.logE("Error creating media file, check storage permissions: ");
            return;
        }
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            FileOutputStream fos = new FileOutputStream(pictureFile);
                            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
                            fos.close();
                            downloadComplete();
                        } catch (FileNotFoundException e) {
                            LogUtil.logE("File not found: " + e.getMessage());
                            downloadFailed();
                        } catch (IOException e) {
                            LogUtil.logE("Error accessing file: " + e.getMessage());
                            downloadFailed();
                        }
                    }
                }
        ).start();
    }


    private static void showDownloadNotification() {
        mNotifyManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setContentTitle("Picture Download")
                .setContentText("Download in progress")
                .setSmallIcon(android.R.drawable.stat_sys_download);
        mBuilder.setProgress(0, 0, true);
        mNotifyManager.notify(id, mBuilder.build());
    }


    private static void downloadComplete() {
        if (mBuilder == null && mNotifyManager == null) {
            showDownloadNotification();
        } else {
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            File file = new File(ulr); // set your audio path
            intent.setDataAndType(Uri.fromFile(file), "image/*");

            PendingIntent pIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
            mBuilder.setContentText("Download complete")
                    .setProgress(0, 0, false)
                    .setSmallIcon(android.R.drawable.stat_sys_download_done)
                    .setContentIntent(pIntent);
            mNotifyManager.notify(id, mBuilder.build());
        }
    }


    private static void downloadFailed() {
        if (mBuilder == null && mNotifyManager == null) {
            showDownloadNotification();
        } else {
            mBuilder.setContentText("Download failed")
                    .setProgress(0, 0, false)
                    .setSmallIcon(android.R.drawable.stat_sys_download_done);
            mNotifyManager.notify(id, mBuilder.build());
        }
    }


    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS) + "/");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "IM_" + timeStamp + ".jpg";
        ulr = mediaStorageDir.getPath() + File.separator + mImageName;
        mediaFile = new File(ulr);
        return mediaFile;
    }

}