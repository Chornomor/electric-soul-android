package com.electricsoul.android.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;

import com.electricsoul.android.R;

/**
 * Created by alexchern on 20.01.17.
 */

public class ClipboardUtil {

    private static final String CLIPBOARD_TAG = "CLIPBOARD";


    public static void saveToClipboard(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(CLIPBOARD_TAG, text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, context.getString(R.string.events_details_copied), Toast.LENGTH_SHORT).show();
    }
}
