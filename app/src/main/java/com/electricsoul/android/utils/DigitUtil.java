package com.electricsoul.android.utils;

/**
 * Created by alexchern on 09.02.17.
 */

public class DigitUtil {

    public static String floatFormat(double d) {
        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return String.format("%.2f", d).replace(",", ".");
    }
}
