package com.electricsoul.android.utils;

import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by eugenetroyanskii on 21.11.16.
 */

public class Regex {

    public static boolean isValidEmail(String text) {
//        String valid = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
//        Pattern p = Pattern.compile(valid);
//        Matcher m = p.matcher(text);
        return Patterns.EMAIL_ADDRESS.matcher(text).matches();
    }

    public static String removeMultiSpace(String text){
        if(text.contains("  ")) {
            text = text.trim().replaceAll(" +", " ");
        }
        return text;
    }

    public static boolean isEmpty(String text) {
        return text.trim().isEmpty();
    }
}