package com.electricsoul.android.utils;

import android.app.Activity;
import android.content.Context;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by eugenetroyanskii on 29.11.16.
 */

public class AnimateView {

    private static final int ANIMATE_TIME_DURATION      = 300;
    private static final int ANIMATION_DELAY            = 5000;
;

    private static final float ALPHA_VISIBLE            = 1f;
    private static final float ALPHA_INVISIBLE          = 0f;

    private static final float MIN_SCALE                = 0.7f;
    private static final float DEFAULT_SCALE            = 1f;

    private Context mContext;
    private Timer timer;
    private LinearLayout animatingLayout;

    public AnimateView(Context mContext, LinearLayout animateLayout) {
        this.mContext = mContext;
        this.animatingLayout = animateLayout;
    }


    public void showNoConnection() {
        animateVisibility(ALPHA_VISIBLE, animatingLayout);
        timer = new Timer();
        AnimateTimerTask timerTask = new AnimateTimerTask();
        timer.schedule(timerTask, ANIMATION_DELAY);
    }


    public void likeAnimation() {
        animateVisibility(ALPHA_VISIBLE, animatingLayout);
        Animation scale = new ScaleAnimation(MIN_SCALE, DEFAULT_SCALE, MIN_SCALE, DEFAULT_SCALE, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setDuration(ANIMATE_TIME_DURATION);
        animatingLayout.startAnimation(scale);
        timer = new Timer();
        AnimateTimerTask timerTask = new AnimateTimerTask();
        timer.schedule(timerTask, ANIMATE_TIME_DURATION);
    }


    private void animateVisibility(float alpha, LinearLayout layout) {
        layout.animate()
                .alpha(alpha)
                .setDuration(ANIMATE_TIME_DURATION)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }


    private class AnimateTimerTask extends TimerTask {

        @Override
        public void run() {
            if (mContext != null) {
                ((Activity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        animateVisibility(ALPHA_INVISIBLE, animatingLayout);

                        if (timer != null) {
                            timer.cancel();
                            timer = null;
                        }
                    }
                });
            }
        }
    }
}
