package com.electricsoul.android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;

/**
 * Created by denisshovhenia on 24.11.16.
 */
public class GetPhotoUtil {

    public static final int REQ_CAMERA = 0;
    public static final int REQ_GALLERY = 1;
    public static final int DEFAULT_MAX_SIZE = 400;
    private Activity activity;
    private Fragment fragment;
    private PhotoUtilListener listener;
    private Uri photoContainer;
    private int maxSize = -1;

    public GetPhotoUtil(Activity activity, PhotoUtilListener listener) {
        this.activity = activity;
        this.listener = listener;
    }

    public GetPhotoUtil(Fragment f, Activity activity, PhotoUtilListener listener) {
        this.fragment = f;
        this.activity = activity;
        this.listener = listener;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getMaxSize() {
        if (maxSize == -1)
            return DEFAULT_MAX_SIZE;
        else
            return maxSize;
    }

    public void takeFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Marshmallow+
            int permission1 = activity.checkSelfPermission(android.Manifest.permission.CAMERA);
            int permission2 = activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permission1 == PackageManager.PERMISSION_GRANTED &&
                    permission2 == PackageManager.PERMISSION_GRANTED) {
                sendPhotoIntent();
            } else {
                if (fragment != null) {
                    fragment.requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQ_CAMERA);
                } else {
                    activity.requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQ_CAMERA);
                }
            }
        } else {
            sendPhotoIntent();
        }
    }

    public void sendPhotoIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        photoContainer = makeContainer();
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoContainer);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            if (fragment != null) {
                fragment.startActivityForResult(takePictureIntent, REQ_CAMERA);
            } else {
                activity.startActivityForResult(takePictureIntent, REQ_CAMERA);
            }
        }
    }

    private Uri makeContainer() {
        File file = new File(Environment.getExternalStorageDirectory(),
                DateUtil.getCurrentTime(DateUtil.DATE_PHOTO_NAME_FORMAT) + ".jpg");
        return Uri.fromFile(file);
    }

    public void takeFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Marshmallow+
            int permission = activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permission == PackageManager.PERMISSION_GRANTED) {
                sendGallery();
            } else {
                if (fragment != null) {
                    fragment.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQ_GALLERY);
                } else {
                    activity.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQ_GALLERY);
                }
            }
        } else {
            sendGallery();
        }
    }

    public void sendGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (fragment != null)
            fragment.startActivityForResult(i, REQ_GALLERY);
        else
            activity.startActivityForResult(i, REQ_GALLERY);
    }

    public boolean resolveResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != REQ_CAMERA && requestCode != REQ_GALLERY) {
            return false;
        }
        if (resultCode != Activity.RESULT_OK) {
            return true;
        }
        if (listener != null)
            listener.onImageDecoding();
        Uri uri = null;
        if (requestCode == REQ_CAMERA) {
            uri = photoContainer;
        } else {
            LogUtil.logE("data.getData() = " + data.getData().getPath());

            uri = data.getData();
        }
        new ImageResolver(getMaxSize()).execute(uri);
        return true;
    }

    private class ImageResolver extends AsyncTask<Uri, Void, LoadedPhoto> {
        int maxSize;

        public ImageResolver(int maxSize) {
            this.maxSize = maxSize;
        }

        @Override
        protected LoadedPhoto doInBackground(Uri... params) {
            if (params == null || params.length == 0) {
                Log.e("ERROR", "URI = NULL!");
                return null;
            }
            Uri uri = params[0];
            LoadedPhoto photo = new LoadedPhoto();
            try {
                photo.bitmap = getScaledBitmap(activity, uri, maxSize);
                photo.uri = uri;

                photo.file = new File(activity.getCacheDir(), DateUtil.getCurrentTime(DateUtil.DATE_PHOTO_NAME_FORMAT) + ".png");
                try {
                    OutputStream os = new FileOutputStream(photo.file);
                    photo.bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                return photo;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(LoadedPhoto photo) {
            super.onPostExecute(photo);
            if (listener != null && photo != null) {
                listener.onPicObtained(photo);
            }
        }
    }

    private Bitmap getScaledBitmap(Context context, Uri uri, int maxSize) throws IOException {
        InputStream is;
        boolean isLocal = !uri.getScheme().equals("http") && !uri.getScheme().equals("https");

        if (isLocal)
            is = context.getContentResolver().openInputStream(uri);
        else
            is = new URL(uri.toString()).openConnection().getInputStream();
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();
        int rotatedWidth, rotatedHeight;
        int orientation = 0;
        if (isLocal)
            orientation = getOrientation(context, uri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        if (isLocal)
            is = context.getContentResolver().openInputStream(uri);
        else
            is = new URL(uri.toString()).openConnection().getInputStream();
        if (rotatedWidth > maxSize || rotatedHeight > maxSize) {
            float widthRatio = ((float) rotatedWidth) / ((float) maxSize);
            float heightRatio = ((float) rotatedHeight) / ((float) maxSize);
            float maxRatio = Math.max(widthRatio, heightRatio);
            maxRatio = Math.round(maxRatio + 0.5f);
            if ((maxRatio % 2) > 0)
                maxRatio++;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        } else {
        }
        String type = context.getContentResolver().getType(uri);
        if (!isLocal) {
            type = uri.toString().endsWith(".jpg") ? "image/jpg" : "image/png";
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (type != null && type.equals("image/png")) {
            Log.d("DEBUG", "compressed to png");
            srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        } else if (type == null || type.equals("image/jpg") || type.equals("image/jpeg")) {
            Log.d("DEBUG", "compressed to jpg");
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] bMapArray = baos.toByteArray();
        baos.close();
        Bitmap b = BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
        Log.d("DEBUG", "bitmap size " + b.getByteCount());
        return b;
    }

    private int getOrientation(Context context, Uri photoUri) throws IOException {
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor == null || cursor.getCount() != 1) {
            if (cursor == null) {
                File file = new File(URI.create(photoUri.toString()));
                ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotate = 0;
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;

                }
                return rotate;
            }
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static class LoadedPhoto {
        public Uri uri;
        public Bitmap bitmap;
        public File file;
    }

    public interface PhotoUtilListener {
        void onImageDecoding();

        void onPicObtained(LoadedPhoto loadedPhoto);
    }

    public void saveInstance(Bundle save) {
        save.putParcelable("photoContainer", photoContainer);
    }

    public void loadInstance(Bundle load) {
        photoContainer = load.getParcelable("photoContainer");
    }

}