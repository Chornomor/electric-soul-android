package com.electricsoul.android.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by yuliasokolova on 14.11.16.
 */

public class KeyboardUtil {
    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public static boolean isKeyboardVisible(View view, Context context){
        int heightDiff = view.getRootView().getHeight() - view.getHeight();
        // if more than 200 dp, it's probably a keyboard
        return heightDiff > dpToPx(context, 200);
    }

    private static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public static void initFocusAndShowKeyboard(final EditText et, final Context context) {
        et.requestFocus();
        et.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
            }
        }, 300);
    }
}
