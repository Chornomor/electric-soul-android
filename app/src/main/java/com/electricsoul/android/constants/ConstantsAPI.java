package com.electricsoul.android.constants;

import com.electricsoul.android.BuildConfig;

/**
 * Created by eugenetroyanskii on 11.11.16.
 */

public interface ConstantsAPI {

    int CODE_429_SERVER_UPDATING    = 429;
    int CODE_400_BAD_REQUEST        = 400;
    int CODE_401_NOT_AUTHENTICATE   = 401;
    int CODE_403_VEREFIY_ACCOUNT    = 403;
    int CODE_404_NO_MATCHES         = 404;
    int CODE_200_SUCCESS            = 200;
    int CODE_201_CREATED            = 201;
    int CODE_204_NO_CONTENT         = 204;

    String BASE_URL = BuildConfig.BASE_URL;
    String BASE_ELASTIC_URL = BuildConfig.BASE_ELASTIC_URL;

    String AUTH_REGISTER            = "auth/register";
    String FB_LOGIN                 = "auth/fb_login";
    String LOGIN                    = "auth/login";
    String LOGIOUT                  = "auth/logout";
    String CITIES                   = "cities";
    String CITY                     = "city";
    String SEARCH                   = "search/";
    String USERS                    = "users/";
    String EVENTS_SLASH             = "events/";
    String SLASH_EVENTS             = "/events";
    String EVENTS                   = "events";
    String GET_USER                 = "/get_user";
    String GET_EVENT                = "/get_event";
    String SITE_INFO                = "site_info";
    String ME                       = "me";
    String UPDATE                   = "/update";
    String PASSWORD_CHANGE          = "/password_change";
    String PASSWORD_RESET           = "auth/password_reset";
    String DEACTIVATE               = "/deactivate";
    String FANIMAGES                = "/fanimages";
    String SLASH_FAN_IMAGES         = "/fan_images";
    String FAN_IMAGES_SLASH         = "fan_images/";
    String RSVP                     = "/rsvp";
    String SLASH_TICKETS            = "/tickets";
    String TICKETS_SLASH            = "tickets/";
    String BUY                      = "buy";
    String EVENT_ATTENDEES          = "/event_attendees";
    String PROMOTER_IMAGES          = "/promoter_images";
    String UPLOAD                   = "/upload";
    String EVENT_COVER_IMAGE_LIKE   = "event_cover_image_likes/";
    String LIKE                     = "like";
    String SLASH_LIKE               = "/like";
    String GET_LIKES                = "/get_likes";
    String GET_PROMOTER_LIKES       = "/get_likes";
    String EVENT_LIKES              = "event_likes/";
    String FAN_LIKES                = "fan_likes/";
    String PROMOTER_LIKES           = "promoter_likes/";
    String USER_LIKES               = "user_likes/";
    String USER_COVER_IMAGE_LIKES   = "user_cover_image_likes/";
    String FOLLOW_USER              = "follow_user/";
    String FOLLOW                   = "/follow";
    String GET_FOLLOWING_NAMES      = "/get_following_name";
    String GET_FOLLOWER_NAMES       = "/get_follower_name";
    String EVENT_COMMENT            = "event_comment/";
    String GET_EVENT_COMMENT        = "get_event_comment";
    String SEND                     = "/send";
    String EVENT_COMMENT_LIKE       = "event_comment_likes/";
    String FAN_IMAGE_COMMENT_SHASH  = "fan_image_comment/";
    String FAN_IMAGE_COMMENT        = "fan_image_comment";
    String RESEND_EMAIL_ACTIVATION = "auth/resend_email";

    // Elastic API
    String SEARCH_ELASTIC = "_search";
}
