package com.electricsoul.android.constants;

/**
 * Created by alexchern on 31.01.17.
 */

public interface ConstantsNotification {

    String NOTIFICATION_EVENT_ID = "eventID";
    String NOTIFICATION_TITLE = "title";
}
