package com.electricsoul.android.constants;

import com.google.android.gms.wallet.WalletConstants;

/**
 * Created by alexchern on 24.01.17.
 */

public interface ConstantsAndroidPay {

    int REQUEST_CODE_MASKED_WALLET = 1001;
    int REQUEST_CODE_RESOLVE_LOAD_FULL_WALLET = 1004;

    int ERROR_CODE_INVALID_TOKEN = 1;

    // Environment to use when creating an instance of Wallet.WalletOptions
    int WALLET_ENVIRONMENT = WalletConstants.ENVIRONMENT_PRODUCTION;

    String MERCHANT_NAME = "Electric Soul";

    String PAY_RESULT = "PAY_RESULT";
    int PAY_RESULT_SUCCESS = 1;

    // Intent extra keys
    String EXTRA_ITEM_ID = "EXTRA_ITEM_ID";
    String EXTRA_MASKED_WALLET = "EXTRA_MASKED_WALLET";

    String CURRENCY_CODE_USD = "USD";

    String DESCRIPTION_LINE_ITEM_SHIPPING = "Shipping";
    String DESCRIPTION_LINE_ITEM_TAX = "Tax";
}
