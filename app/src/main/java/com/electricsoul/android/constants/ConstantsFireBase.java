package com.electricsoul.android.constants;

/**
 * Created by yuliasokolova on 07.12.16.
 */

public interface ConstantsFireBase {

    String BASE_URL = "https://firebasedynamiclinks.googleapis.com/v1/";

    // Firebase Requests
    String SHORT_LINK           = "shortLinks";
    String APP_KEY              = "?key=AIzaSyD7GyNxjFx7Oco6OU316Oa-HA3VibFKJDQ";


    // FirebaseAnalytics
    // Events
    String LOGIN_EVENT           = "Login";
    String REGISTER_EVENT        = "Register";
    String TICKET_PURCHASE_EVENT = "Ticket_Purchase";
    String GEOLOCATION_EVENT     = "Geolocation";
    String UPLOAD_PHOTO_EVENT    = "Upload_photo";
    String RSVPS_EVENT           = "RSVP";
    String EVENT_COMMENT_EVENT   = "Event_Comment";
    String EVENT_CHECKIN_EVENT   = "Check_in";
    String EVENT_CARD_EVENT      = "Event_card";
    String FOLLOW_USER_EVENT     = "Follow_User";
    String UNFOLLOW_USER_EVENT   = "Unfollow_User";

    // Params
    String CITY_NAME_PROPERTY    = "City_Name";
    String RSVP_STATE_PROPERTY   = "Going";
    String USER_NAME_PROPERTY    = "username";
    String EMAIL_PROPERTY        = "email";
    String EVENT_NAME_PROPERTY   = "EventName";
    String COMMENT_TEXT_PROPERTY = "Comment_text";

}
